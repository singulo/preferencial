<?php

function carregar_na_filial()
{
    $CI = get_instance();

    if( ! isset($_SESSION['filial']['bairros_imoveis']))
        $_SESSION['filial']['bairros_imoveis'] = $CI->imovel_model->bairros_imoveis();
}