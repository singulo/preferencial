<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>

<div class="quem-somos">
    <div class="container">
        <div class="col-md-7">
            <h1>A realização dos teus sonhos <strong><em>é o que nos realiza.</em></strong></h1>
            <br>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra dui vitae justo maximus, quis porttitor eros auctor. Aenean facilisis sit amet elit ac aliquam. Sed eleifend posuere viverra. Quisque sed velit orci. Maecenas at nibh ut arcu tristique rutrum. In facilisis nunc id blandit cursus. Aenean in tortor lectus. Maecenas id quam tempor, tempus libero vel, vehicula mauris. Nullam ante mauris, lobortis quis diam at, lacinia vestibulum tortor. Ut et justo ac tortor tempus malesuada. Cras in pharetra neque. Pellentesque in justo nulla. Vivamus tempus risus eu congue vehicula.</p>
            <br>
            <h1>Somos <strong>especialistas</strong></h1>
            <div class="row">
                <div class="col-md-4">
                    <img src="http://placehold.it/350x250" class="img-responsive">
                    <h3>COMERCIAIS E COOPORATIVOS</h3>
                    <p>Portifólio exclusivos e equipe especializada em imóveis comerciais e coorporativos que vão ao encontro de suas principais necessidades.</p>
                </div>
                <div class="col-md-4">
                    <img src="http://placehold.it/350x250" class="img-responsive">
                    <h3>PRONTO PARA MORAR</h3>
                    <p>Apartamentos, coberturas horizontais, duplex, lofys, flats e casas em condomínios luxuosas nos bairros mais nobres de Porto Alegre.</p>
                </div>
                <div class="col-md-4">
                    <img src="http://placehold.it/350x250" class="img-responsive">
                    <h3>EM CONSTRUÇÃO</h3>
                    <p>Projetos luxuosos, modernos e inovadores nos melhores bairros de Porto Alegre para você morar ou que representam alto potencial de rentabilidade e valorização.</p>
                </div>
            </div>
        </div>
        <div class="col-md-5 diretor">
            <img src="<?= base_url('assets/images/quem-somos-diretor.jpg'); ?>" alt="Qualitá Diretor Executivo" class="img-responsive">
            <p>Nam turpis eros, ullamcorper sed turpis sit amet, tincidunt euismod enim. Aliquam erat volutpat. Donec ligula justo, consequat sit amet risus eget, commodo egestas lacus.</p>
            <h3><b><em>Thiago Trindade - Diretor executivo</em></b></h3>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-7">
                    <h1>Conheça a nossa equipe</h1>
                    <div class="row">
                        <? foreach($corretores as $corretor) : ?>
                            <? if($corretor->exibir_site) : ?>
                                <div class="col-md-6">
                                    <!--                                <img src="http://placehold.it/350x250" class="img-responsive">-->
                                    <? $nome = explode(' ', $corretor->nome); ?>
                                    <h3><em><?= $nome[0]; ?> <strong><? if(isset($nome[1])) echo $nome[1]; ?></strong></em></h3>
                                    <small>Telefone: <?= $corretor->telefone; ?>
                                        <br>
                                        <em><?= $corretor->email; ?></em>
                                    </small>
                                    <br>
                                    <hr>
                                </div>
                            <? endif; ?>
                        <? endforeach; ?>
                    </div>
                </div>

                <div class="col-md-5">
                    <h1>Seja bem-vindo a nossa casa</h1>
                    <img src="http://placehold.it/630x350" class="img-responsive">
                </div>
            </div>
        </div>

    </div>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<style>
    .quem-somos
    {
        padding-top: 30px;
    }

    .quem-somos h1,
    .quem-somos p
    {
        color: #616161;
    }

    .quem-somos h1
    {
        font-size: 23px;
    }

    .quem-somos h3
    {
        font-size: 17px;
    }

    .quem-somos .diretor img
    {
        margin-bottom: 15px;
    }

    .quem-somos hr
    {
        width: 20%;
        border-top: 3px solid #0a2444;
        float: left;
    }
</style>
