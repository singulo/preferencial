<div class="rodape col-xs-12" style="background-image: url('<?= base_url('assets/images/bg-rodape.png'); ?>');">
    <div class="container">
        <div class="info-imobiliaria">
            <div class="col-md-12">
                <img src="<?= base_url('assets/images/logo-rodape.png'); ?>" class="center-block" style="height: 90px; margin-top: -125px;">
                <p class="text-center rodape-creci"><?= $_SESSION['filial']['creci']; ?></p>
            </div>
            <div class="col-xs-12 col-md-4">
                <h3>CONTATO</h3>
                <p> <b><?= $_SESSION['filial']['endereco']; ?></b>
                    <br> <?= $_SESSION['filial']['cidade']; ?> | <?= strtoupper($_SESSION['filial']['estado']); ?>
                    <br>
                    <br> <b><?= $_SESSION['filial']['email_padrao']; ?></b>
                    <br> <?= $_SESSION['filial']['telefone_1']; ?> / <?= $_SESSION['filial']['telefone_2']; ?></p>
            </div>
            <div class="col-xs-12 col-md-4 text-center rodape-tipos">
                <? foreach(SiteFilial::imovelTipos() as $tipo) : ?>
                    <p><a href="<?= base_url('imovel/pesquisar?tipo%5B%5D=' . $tipo->id); ?>"><?= $tipo->tipo; ?></a></p>
                <? endforeach; ?>
            </div>
            <div class="col-xs-12 col-md-3 pull-right">
                <a href="https://www.youtube.com/user/CorretorEronVargas/videos" target="_blank" style="color:#fff;">
                    <img src="<?= base_url('assets/images/logo-youtube.png'); ?>" style="height: 60px; margin-top: -8px;" class="center-block">
                    <p class="text-center">Assista a vídeos dos <br><strong>Empreendimentos</strong></p>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="direitos-simples col-xs-12">
    <div class="container">
        <div class="text-right">
            <a href="http://www.simplesimob.com.br/">
                <img src="<?= base_url('assets/images/simplesimob-logo.png'); ?>" class="img-responsive pull-right" alt="Simples Imob" style="padding-left: 10px; margin-top: -4px;">
            </a>
            <p>Copyright <?= date('Y'); ?> | Todos os direitos reservados, desenvolvido por Singulo Comunicação | Site integrado ao sistema Simples Imob </p>
        </div>
    </div>
</div>