<div class="banner-container">
    <? foreach(SiteFilial::banners() as $banner) : ?>
        <div class="item" style="background-image: url(<?= eh_mobile() ? $GLOBALS[FILIAL_BANNER_CAMINHO_EXTERNO] . $banner->arquivo_pequeno : $GLOBALS[FILIAL_BANNER_CAMINHO_EXTERNO] . $banner->arquivo_medio ; ?>);" data-src-xlg="<?= $GLOBALS[FILIAL_BANNER_CAMINHO_EXTERNO] . $banner->arquivo_extra_grande; ?>" data-src-lg="<?= $GLOBALS[FILIAL_BANNER_CAMINHO_EXTERNO] . $banner->arquivo_grande; ?>" data-src-md="<?= $GLOBALS[FILIAL_BANNER_CAMINHO_EXTERNO] . $banner->arquivo_medio; ?>" data-src-sm="<?= $GLOBALS[FILIAL_BANNER_CAMINHO_EXTERNO] . $banner->arquivo_pequeno; ?>">
            <div class="container">

                <? if($banner->link != '') : ?>
                    <div class="banner-imovel-container">
                        <div class="row">
                            <div class="col-md-9">
                                <? if($banner->titulo != '') :?>
                                    <h2 class="banner-imovel-titulo"><?= $banner->titulo; ?></h2>
                                    <p class="banner-imovel-texto"><em><?= $banner->subtitulo; ?></em></p>
                                <?else :?>
                                    <h2 class="banner-imovel-titulo">Ver detalhes</h2>
                                <?endif; ?>
                            </div>
                            <div class="col-md-3">
                                <a href="<?= $banner->link; ?>" <?= $banner->nova_aba == 1 ? 'target="_blank"' : ''; ?> title="Veja mais detalhes">
                                    <span class="glyphicon glyphicon-menu-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                <?endif; ?>
            </div>
        </div>
    <? endforeach; ?>
</div>