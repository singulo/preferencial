<div class="mapa-contato">
    <div class="col-md-6">
        <div class="row">
            <?= montar_div_mapa('map-canvas', SiteFilial::dadosImobiliaria()->latitude_longitude, ['ao-carregar-pagina-chamar' => 'GmapScript.funcs.iniciar(\'map-canvas\');'], 'mapa'); ?>
        </div>
    </div>
    <div class="col-md-6 contato">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <em>
                        <h1>Fale com a gente</h1>
                        <p>Para falar conosco, por favor, envie o formulário abaixo que um dos nossos corretores entrará em contato.</p>
                    </em>
                </div>
            </div>
        </div>

        <form id="form-contato" onsubmit="return false;">
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <input type="text" name="nome" class="form-control" placeholder="Qual é o seu nome?">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Informe seu email">
                </div>
                <div class="form-group">
                    <input type="text" name="telefone" class="form-control telefone" placeholder="Número para contato?">
                </div>
                <div class="form-group">
                    <input type="text" name="assunto" class="form-control" placeholder="Qual é o assunto?">
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <textarea name="mensagem" class="form-control" rows="8" placeholder="Digite aqui o motivo do seu contato..."></textarea>
                </div>

                <button type="button" class="btn btn-danger pull-right btn-enviar" data-loading-text="Aguarde..." onclick="ClienteScript.funcs.enviarContatoNormal();">ENVIAR</button>
            </div>
        </form>
    </div>
</div>