<div class="container" style="margin-top: 120px;">
    <h1><?= $titulo; ?></h1>
    <p><em><?= $texto; ?></em></p>
    <div class="condominios row">
        <? $this->load->library('simples/CondominioTipos'); ?>
        <? foreach($condominios as $condominio) : ?>
            <div class="col-md-4">
                <div class="condominio">
                    <a href="<?= base_url('condominio?id=' . $condominio->id); ?>">
                        <span class="finalidade"><?= CondominioTipos::toString($condominio->tipo) ?></span>
                        <img class="img-responsive center-block" src="<?= json_decode($condominio->fotos_destaque)[0]->arquivo_grande; ?>" alt="<?= 'Condomínio Fechado ' . $condominio->nome; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                        <h3><?= $condominio->nome; ?></h3>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>