<? require_once MODULESPATH . 'simples/libraries/CondominioTipos.php'; ?>
<? require_once MODULESPATH . 'simples/helpers/condominio_detalhe_helper.php'; ?>

<div class="hidden-xs hidden-sm">
    <div id="imagens-principais-carousel">
        <? foreach($condominio->fotos_destaque as $foto) : ?>
            <div class="item">
                <img src="<?= $foto->arquivo_grande; ?>">
            </div>
        <? endforeach; ?>
    </div>
    <div class="owl-carousel-navigation">
        <a class="prev"><img src="<?= base_url('assets/images/arrow-prev.png'); ?>"></a>
        <a class="next"><img src="<?= base_url('assets/images/arrow-next.png'); ?>"></a>
    </div>
</div>
<div class="visible-xs visible-sm">
    <? if($condominio->fotos_destaque[0]) : ?>
        <img class="img-responsive" src="<?= $condominio->fotos_destaque[0]->arquivo_grande; ?>">
    <? endif; ?>
</div>

<div class="lancamento-cabecalho">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <span><?= CondominioTipos::toString($condominio->tipo); ?></span>
                <h3><?= $condominio->nome; ?> - <?= $condominio->cidade; ?></h3>
            </div>
            <? if(count($condominio->unidades) > 1) : ?>
                <div class="col-md-3 text-right">
                    <span>Área</span>
                    <h3><? exibir_valor_min_max($condominio->unidades, 'area_util', 'de %sm² a %sm²'); ?></h3>
                </div>
                <div class="col-md-1 text-right">
                    <span>Domitórios</span>
                    <h3><? exibir_valor_min_max($condominio->unidades, 'dormitorios'); ?></h3>
                </div>
            <? elseif(count($condominio->unidades) == 1) : ?>
                <div class="col-md-3 text-right">
                    <span>Área</span>
                    <h3><?= obter_valor_min($condominio->unidades, 'area_util') . 'm²'; ?></h3>
                </div>
                <div class="col-md-1 text-right">
                    <span>Domitórios</span>
                    <h3><?= obter_valor_min($condominio->unidades, 'dormitorios'); ?></h3>
                </div>
            <? else : ?>
                <div class="col-md-2 text-right">
                    <span>Área</span>
                    <h3>Nenhum</h3>
                </div>
                <div class="col-md-1 text-right">
                    <span>Domitórios</span>
                    <h3>Nenhum</h3>
                </div>
            <? endif; ?>
            <? if(count($condominio->unidades) > 1) : ?>
                <div class="col-md-2 text-right">
                    <span>A partir de</span>
                    <h3><?= formatar_valor(obter_valor_min($condominio->unidades, 'valor'), '<small>R$</small>'); ?></h3>
                </div>
            <? else : ?>
                <div class="col-md-2 text-right">
                    <span>A partir de</span>
                    <h3>Não informado</h3>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-5 imovel-descricao">
        <h4 class="pull-left">DESCRIÇÃO DO CONDOMÍNIO</h4> <small class="pull-right">CÓDIGO: <?= $condominio->id; ?></small>
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-pills text-center">
                    <li>
                        <span><?= $condominio->area_total; ?>m²</span><br><small> ÁREA TOTAL</small>
                    </li>
                    <? if(strlen($condominio->previsao_entrega) > 0) : ?>
                        <li>
                            <span><?= $condominio->previsao_entrega; ?></span><br><small> Previsão de entrega</small>
                        </li>
                    <? endif; ?>
                </ul>

                <p><?= $condominio->descricao; ?></p>

                <div class="imovel-complementos">
                    <? if($condominio->complementos) : ?>
                        <h4>AQUI VOCÊ VAI ENCONTRAR</h4>
                        <? foreach($condominio->complementos as $complemento) : ?>
                            <span><?= $complemento->complemento; ?></span>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 imovel-midias">
        <section>
            <div class="tabs tabs-style-linetriangle">
                <nav>
                    <ul>
                        <li>
                            <a href="#section-fotos">
                                <span>
                                    <img src="<?= base_url('assets/images/icon-photo.png'); ?>"><br> <small>Fotos</small>
                                </span>
                            </a>
                        </li>
                        <? if(count($condominio->midias->fotos->planta) > 0) : ?>
                            <li>
                                <a href="#section-plantas">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-metragem.png'); ?>"><br> <small>Plantas (<?= count($condominio->fotos['plantas']);?>)</small>
                                    </span>
                                </a>
                            </li>
                        <? endif; ?>
                        <? if(count($condominio->videos) > 0) : ?>
                            <li>
                                <a href="#section-videos">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-youtube.png'); ?>"><br> <small>Vídeos (<?= count($condominio->videos);?>)</small>
                                    </span>
                                </a>
                            </li>
                        <? endif; ?>
                        <? if(count($condominio->unidades) > 0 ) : ?>
                            <li>
                                <a href="#section-unidades">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-metragem.png'); ?>"><br> <small>Unidades</small>
                                    </span>
                                </a>
                            </li>
                        <? endif; ?>
                    </ul>
                </nav>
                <div class="content-wrap">
                    <section id="section-fotos" class="condominio-fotos">
                        <? if(count($condominio->midias->fotos->normais) > 0) : ?>
                            <? foreach($condominio->midias->fotos->normais as $foto) : ?>
                                <div class="col-md-3">
                                    <a rel="galleria-fotos" href="<?= $foto->arquivo_grande; ?>" title="<?= $foto->legenda; ?>">
                                        <img class="img-responsive" src="<?= $foto->arquivo_grande; ?>" >
                                    </a>
                                </div>
                            <? endforeach; ?>
                        <? else : ?>
                            <div class="col-md-12">
                                <p class="text-center">Sem fotos</p>
                            </div>
                        <? endif; ?>
                    </section>
                    <section id="section-plantas">
                        <? foreach($condominio->midias->fotos->planta as $foto) : ?>
                            <div class="col-md-3">
                                <a rel="galleria-fotos" href="<?= $foto->arquivo_grande; ?>" title="<?= $foto->legenda; ?>">
                                    <img class="img-responsive" src="<?= $foto->arquivo_grande; ?>" >
                                </a>
                            </div>
                        <? endforeach; ?>
                    </section>
                    <section id="section-videos">
                        <? foreach($condominio->videos as $video) : ?>
                            <div class="col-md-4">
                                <iframe width="100%" height="150" src="https://www.youtube.com/embed/<?= $video->id_youtube; ?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                        <? endforeach; ?>
                    </section>
                    <? if(count($condominio->unidades) > 0 ) : ?>
                        <section id="section-unidades">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Área útil</th>
                                    <th>Dormitórios</th>
                                    <th>Suítes</th>
                                    <th>Vagas</th>
                                    <th>Valor</th>
                                </tr>
                                <tbody>
                                <? foreach($condominio->unidades as $unidade) : ?>
                                    <tr>
                                        <td><?= $unidade->area_util; ?>m²</td>
                                        <td><?= $unidade->dormitorios; ?></td>
                                        <td><?= $unidade->suites; ?></td>
                                        <td><?= $unidade->vagas; ?></td>
                                        <td><?= format_valor($unidade->valor, '<small>R$</small>'); ?></td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                                </thead>
                            </table>
                        </section>
                    <? endif; ?>
                </div><!-- /content -->
            </div><!-- /tabs -->
        </section>
    </div>
</div>

<div class="condominios-similares">
    <div class="container">

        <div class="col-xs-12">
            <h2><em>Imóveis em <strong><?= $condominio->nome; ?></strong></em><br><hr></h2>
        </div>

        <? foreach($condominio->imoveis as $imovel) : ?>
            <div class="imovel col-md-3">
                <a href="<?= imovel_url($imovel); ?>">
                    <span class="finalidade"><?= Finalidades::toString($imovel->finalidade); ?></span>
                    <img src="<?= json_decode($imovel->fotos_destaque)[0]->arquivo_grande; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="tipo"><?= SiteFilial::imovelTipos()[$imovel->id_tipo]->tipo; ?></h3>
                        </div>
                        <div class="col-md-6">
                            <p class="descricao"><?= $imovel->descricao_abreviada; ?></p>
                        </div>
                        <div class="col-md-12">
                            <p class="valor"><i class="glyphicon glyphicon-triangle-right"></i><?= formatar_valor($imovel->valor_site, 'R$ '); ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <? endforeach; ?>
    </div>
</div>