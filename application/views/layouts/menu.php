<div class="menu-superior hidden-xs hidden-sm">
    <div class="container">
        <div class="col-md-5 pull-right">
            <a href="tel:+55 51 3502 4888"><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> <small>+55 51</small> 3502 4888</a>
            <a href="tel:+55 51 9564 9908"><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> <small>+55 51</small> 9564 9908</a>
            <? if($this->session->has_userdata('usuario')) : ?>
                <a href="#modal-cliente-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a>
            <? else : ?>
                <a href="#modal-login-cadastro" data-toggle="modal"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> login/cadastro</a>
            <?endif; ?>
        </div>
    </div>
</div>
<div class="menu-imovel hidden-xs hidden-sm">
    <div class="container">
        <form id="menu-filtro" class="col-md-10" action="<?= base_url('imovel/pesquisar'); ?>">
            <div class="col-md-2 input">
                <select name="cidade" class="selectpicker" data-show-subtext="true"  data-live-search="true" title="Cidade" data-width="100%">
                    <? if(SiteFilial::configs()->filtro_cidade_e_bairros): ?>
                        <? foreach(SiteFilial::cidadesEBairros() as $cidade => $bairros) : ?>
                            <optgroup label="<?= $cidade; ?>">
                                <? foreach($bairros as $bairro): ?>
                                    <? $valor = remover_caracteres_especiais($cidade.'_'.$bairro); ?>
                                    <option value="<?= $valor; ?>"><?= $bairro; ?></option>
                                <? endforeach; ?>
                            </optgroup>
                        <? endforeach; ?>
                    <? else: ?>
                        <? foreach(SiteFilial::cidadesEBairros() as $cidade => $bairros) : ?>
                            <option value="<?= $cidade; ?>"><?= $cidade; ?></option>
                        <? endforeach; ?>
                    <? endif; ?>
                </select>
            </div>
            <input type="hidden" name="preco_min">
            <input type="hidden" name="preco_max">
            <div class="col-md-2 input">
                <select name="id_tipo" class="selectpicker" data-live-search="true" title="Tipo" data-width="100%" multiple>
                    <? foreach(SiteFilial::imovelTipos() as $tipo): ?>
                        <option value="<?= $tipo->id; ?>"><?= $tipo->tipo;?></option>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="col-md-2 input">
                <select name="valores[]" class="selectpicker" data-live-search="false" title="Valor min/max" data-width="100%" multiple data-max-options="2">
                    <option value="0">0+</option>
                    <option value="150000">150.000</option>
                    <option value="200000">200.000</option>
                    <option value="300000">300.000</option>
                    <option value="400000">400.000</option>
                    <option value="500000">500.000</option>
                    <option value="600000">600.000</option>
                    <option value="700000">700.000</option>
                    <option value="800000">800.000</option>
                    <option value="900000">900.000</option>
                    <option value="1000000">1.000.000</option>
                </select>
            </div>
            <div class="col-md-2 input">
                <input name="id" type="text" class="form-control" placeholder="Código">
            </div>
            <div class="col-md-2 text-right btn-buscar-filtro">
                <a href="#" class="glyphicon glyphicon-search" onclick="$('#menu-filtro').submit();"></a>
            </div>
        </form>

        <div class="row menu-items">
            <div class="col-md-3">
                <a href="<?= base_url('home'); ?>"><img src="<?= base_url('assets/images/logo-menu.png'); ?>"></a>
            </div>
            <div class="col-md-7 pull-right">
                <ul>
<!--                    <li><a href="--><?//= base_url('quem-somos'); ?><!--">QUEM SOMOS</a></li>-->
                    <li><a href="<?= base_url('imovel/pesquisar'); ?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/todos_fechados'); ?>">CONDOMÍNIOS</a></li>
                    <li><a href="<?= base_url('condominio/lancamentos'); ?>">LANÇAMENTOS</a></li>
                    <li><a href="<?= base_url('contato'); ?>">VENDA SEU IMÓVEL</a></li>
                    <li><a href="<?= base_url('contato'); ?>">CONTATO</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="visible-sm visible-xs menu-mobile">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url('home'); ?>"><img src="<?= base_url('assets/images/logo.png') ?>" alt="logo-preferencial" class="logo-preferencial"></a>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?= base_url('imovel/pesquisar'); ?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/todos_fechados'); ?>">CONDOMÍNIOS</a></li>
                    <li><a href="<?= base_url('condominio/lancamentos'); ?>">LANÇAMENTOS</a></li>
                    <li><a href="<?= base_url('contato'); ?>">VENDA SEU IMÓVEL</a></li>
                    <li><a href="<?= base_url('contato'); ?>">CONTATO</a></li>
                    <li><a href="<?=base_url('imovel/pesquisar')?>">PESQUISAR</a></li>
                    <li>
                        <? if($this->session->has_userdata('usuario')) : ?>
                            <a href="#modal-dados" data-toggle="modal"><?= $this->session->userdata('usuario')->nome; ?></a>
                        <? else : ?>
                            <a href="#modal-login" data-toggle="modal"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> login/cadastro</a>
                        <? endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>


<? $this->load->view('cliente/login'); ?>
<? $this->load->view('cliente/cadastrar'); ?>
<? $this->load->view('cliente/dados'); ?>