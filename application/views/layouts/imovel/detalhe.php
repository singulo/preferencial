<div class="hidden-xs hidden-sm">
    <div id="imagens-principais-carousel">
        <? foreach(json_decode($imovel->fotos_destaque) as $foto) : ?>
            <div class="item">
                <img src="<?= $foto->arquivo_grande; ?>"  height="580px">
            </div>
        <? endforeach; ?>
    </div>
    <div class="hidden-xs hidden-md owl-carousel-navigation">
        <a class="prev"><img src="<?= base_url('assets/images/arrow-prev.png'); ?>"></a>
        <a class="next"><img src="<?= base_url('assets/images/arrow-next.png'); ?>"></a>
    </div>
</div>

<div class="visible-xs visible-sm">
    <? if($imovel->fotos_destaque) : ?>
        <img class="img-responsive" src="<?= json_decode($imovel->fotos_destaque)[0]->arquivo_grande; ?>">
    <? endif; ?>
</div>

<div class="imovel-cabecalho">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <? if(isset($imovel->id_condominio) && $imovel->id_condominio > 0) : ?>
                    <span><?= $imovel->condominio_nome; ?></span>
                <? endif; ?>
                <h1><?= $imovel->tipo_extenso; ?> - <?= $imovel->cidade; ?></h1>
            </div>
            <div class="hidden-xs hidden-sm">
                <div class="col-xs-3 col-md-2 text-right">
                    <span>Área</span>
                    <h1><?= $imovel->area_total; ?>m²</h1>
                </div>
                <div class="col-xs-2 col-md-1 text-right">
                    <span>Banheiros</span>
                    <h1><?= $imovel->banheiros; ?></h1>
                </div>
                <div class="col-xs-2 col-md-1 text-right">
                    <span>Domitórios</span>
                    <h1><?= $imovel->dormitorios; ?></h1>
                </div>
                <div class="col-xs-3 text-right">
                    <span>A partir de</span>
                    <h1><?= formatar_valor($imovel->valor_site, 'R$'); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-7 imovel-midias">

        <? if(!$this->session->has_userdata('usuario')) : ?>
            <div class="aviso-para-ver-fotos text-left">
                <h4>Este imóvel contem <strong><?= $total_midias_disponiveis; ?> fotos</strong> e outras <strong>mídias</strong></h4>
                <h5>Tenha acesso as mídias de <strong>todos imóveis</strong>, apenas faça seu cadastro.</h5>
                <button class="btn btn-danger" data-toggle="modal" data-target="#modal-login-cadastro">Entre/Cadastre-se</button>
            </div>
        <? else : ?>
            <section>

                <h4>MÍDIAS DO IMÓVEL</h4>

                <div class="tabs tabs-style-linetriangle">
                    <nav>
                        <ul>
                            <li>
                                <a href="#section-fotos">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-photo.png'); ?>"><br> <small>Fotos  (<?= count($imovel->midias->fotos->normais);?>)</small>
                                    </span>
                                </a>
                            </li>
                            <? if(count($imovel->midias->fotos->planta) > 0) : ?>
                                <li>
                                    <a href="#section-plantas">
                                        <span>
                                            <img src="<?= base_url('assets/images/icon-metragem.png'); ?>"><br> <small>Plantas (<?= count($imovel->midias->fotos->planta);?>)</small>
                                        </span>
                                    </a>
                                </li>
                            <? endif; ?>
                            <? if(count($imovel->midias->videos) > 0) : ?>
                                <li>
                                    <a href="#section-videos">
                                        <span>
                                            <img src="<?= base_url('assets/images/icon-youtube.png'); ?>"><br> <small>Vídeos (<?= count($imovel->midias->videos);?>)</small>
                                        </span>
                                    </a>
                                </li>
                            <? endif; ?>
                        </ul>
                    </nav>
                    <div class="content-wrap">
                        <section id="section-fotos">
                            <? foreach($imovel->midias->fotos->normais as $foto) : ?>
                                <div class="col-md-3">
                                    <a rel="galleria-fotos" href="<?= $foto->arquivo_grande; ?>" title="<?= $foto->legenda; ?>">
                                        <img class="img-responsive" src="<?= $foto->arquivo_grande; ?>" >
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </section>
                        <section id="section-plantas">
                            <? foreach($imovel->fotos['plantas'] as $foto) : ?>
                                <div class="col-md-3">
                                    <a rel="galleria-fotos" href="<?= $foto->arquivo_grande; ?>" title="<?= $foto->legenda; ?>">
                                        <img class="img-responsive" src="<?= (isset($foto->arquivo_pequeno) ? $foto->arquivo_pequeno : $foto->arquivo_grande)?>" >
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </section>
                        <section id="section-videos">
                            <? foreach($imovel->videos as $video) : ?>
                                <div class="col-md-4">
                                    <iframe width="100%" height="150" src="https://www.youtube.com/embed/<?= $video->id_youtube; ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <? endforeach; ?>
                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        <? endif; ?>
    </div>
    <div class="col-md-5 imovel-descricao">
        <h4 class="pull-left">DESCRIÇÃO IMÓVEL</h4> <small class="pull-right">CÓDIGO: <?= $imovel->id; ?></small>
        <div class="col-xs-12">
            <div class="row">
                <ul class="nav nav-pills text-center">
                    <li>
                        <span><?= $imovel->area_total; ?>m²</span><br><small> ÁREA TOTAL</small>
                    </li>
                    <li>
                        <span><?= $imovel->dormitorios; ?></span><br><small> DOMITÓRIOS</small>
                    </li>
                    <li>
                        <span><?= $imovel->suites; ?></span><br><small> SUÍTES</small>
                    </li>
                    <li>
                        <span><?= $imovel->banheiros; ?></span><br><small> BANHEIROS</small>
                    </li>
                    <li>
                        <span><?= $imovel->garagem; ?></span><br><small> VAGAS</small>
                    </li>
                </ul>

                <p><?= $imovel->descricao; ?></p>

                <div class="col-xs-12 imovel-valor">
                    <div class="row">
                        <div class="col-md-7">
                            <h1><?= formatar_valor($imovel->valor_site, '<small>R$</small>'); ?></h1>
                        </div>
                        <div class="col-md-5">
                            <button type="button" class="btn btn-danger btn-interesse" data-target="#modal-contato-interesse" data-toggle="modal"><span class="glyphicon glyphicon-plus"></span> INFORMAÇÕES</button>
                        </div>
                    </div>
                </div>

                <div class="imovel-complementos">
                    <? if($imovel->complementos) : ?>
                        <h4>AQUI VOCÊ VAI ENCONTRAR</h4>
                        <? foreach($imovel->complementos as $complemento) : ?>
                            <span><?= $complemento->complemento; ?></span>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="imoveis-similares">
    <div class="container">

        <div class="col-xs-12">
            <h2><em>Imóveis <strong>similares</strong></em><br><hr></h2>
        </div>

        <? foreach($imoveis_similares as $imovel) : ?>
            <div class="imovel col-xs-12 col-md-3">
                <a href="<?= imovel_url($imovel); ?>">
                    <span class="finalidade"><?= Finalidades::toString($imovel->finalidade); ?></span>
                    <img src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="tipo"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></h3>
                        </div>
                        <div class="col-md-6">
                            <p class="descricao"><?= ellipsize($imovel->descricao, 100); ?></p>
                        </div>
                        <div class="col-md-12">
                            <p class="valor"><i class="glyphicon glyphicon-triangle-right"></i><?= formatar_valor($imovel, 'R$ '); ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <? endforeach; ?>
    </div>
</div>