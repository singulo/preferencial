<? if( ! isset($filtro)) :
    $filtro = obter_objeto_padrao_filtro();
endif;
$GLOBALS['variaveisJS']['Filtro'] = $filtro;  ?>
<div class="container">
    <div class="imoveis col-xs-12">

        <div class="row">
            <div class="col-md-3">
                <h3 class="text-left" style="margin-top: 100px"><em id="msg-resultados-encontrados"><strong>Busque</strong> <br> <small>ofertas em nosso site.</small></em></h3>
            </div>
            <div class="col-md-9">
                <hr>

                <h5><em><strong>Busca avançada</strong></em></h5>
                <div class="row">
                    <div class="col-md-10">
                        <form id="filtro-imovel" onsubmit="return false;">
                            <div class="row">
                                <div class="col-md-3">
                                    <select name="cidade" class="selectpicker" data-show-subtext="true"  data-live-search="false" title="Cidade" multiple data-width="100%">
                                        <? if(SiteFilial::configs()->filtro_cidade_e_bairros): ?>
                                            <? foreach(SiteFilial::cidadesEBairros() as $cidade => $bairros) : ?>
                                                <optgroup label="<?= $cidade; ?>">
                                                    <? foreach($bairros as $bairro): ?>
                                                        <? $valor = remover_caracteres_especiais($cidade.'_'.$bairro); ?>
                                                        <option value="<?= $valor; ?>" <? if(select_value($valor, $filtro->cidade)) echo 'selected'; ?> ><?= $bairro; ?></option>
                                                    <? endforeach; ?>
                                                </optgroup>
                                            <? endforeach; ?>
                                        <? else: ?>
                                            <? foreach(SiteFilial::cidadesEBairros() as $cidade => $bairros) : ?>
                                                <option value="<?= $cidade; ?>"><?= $cidade; ?></option>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="id_condominio" class="selectpicker" data-live-search="true" title="Condomínio" data-width="100%" multiple>
                                        <? foreach ($_SESSION['filial']['condominios'] as $condominio) : ?>
                                            <option value="<?= $condominio->id; ?>" data-subtext="<?= $condominio->cidade; ?>" <? if(isset($filtro->id_condominio) && select_value($condominio->id, $filtro->id_condominio)) echo 'selected'; ?>><?= $condominio->nome;?></option>
                                        <?endforeach;?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="id_tipo" class="selectpicker" data-live-search="true" title="Tipo" data-width="100%" multiple>
                                        <? foreach(SiteFilial::imovelTipos() as $tipo): ?>
                                            <option value="<?= $tipo->id; ?>" <? if(isset($filtro->id_tipo) && select_value($tipo->id, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="dormitorios" class="selectpicker" data-live-search="false" title="Dorm." data-width="100%">
                                        <option title="Dorm." value="">0+</option>
                                        <? for($i = 1; $i <= $this->config->item('pesquisa')['dormitorios']; $i++): ?>
                                            <option value="<?= $i; ?>" <? if(select_value($i, $filtro->dormitorios)) echo 'selected'; ?> ><?= $i . ($i == $this->config->item('pesquisa')['dormitorios'] ? '+' : ''); ?></option>
                                        <? endfor; ?>
                                    </select>
                                </div>
                                <!--<div class="col-md-3">
                                    <select name="finalidade" class="selectpicker" data-live-search="false" title="Finalidade" data-width="100%" multiple>
                                        <?/* $this->load->library('simples/Finalidades'); */?>
                                        <?/* foreach(Finalidades::getConstants() as $finalidade => $valor) : */?>
                                            <option value="<?/*= $valor; */?>"><?/*= $finalidade; */?></option>
                                        <?/* endforeach; */?>
                                    </select>
                                </div>-->

                                <div class="col-md-3">
                                    <select name="preco_min" class="selectpicker" data-live-search="false" title="Valor mínimo" data-width="100%">
                                        <option value="">0+</option>
                                        <option value="50000">50.000</option>
                                        <option value="100000">100.000</option>
                                        <option value="150000">150.000</option>
                                        <option value="200000">200.000</option>
                                        <option value="300000">300.000</option>
                                        <option value="400000">400.000</option>
                                        <option value="500000">500.000</option>
                                        <option value="600000">600.000</option>
                                        <option value="700000">700.000</option>
                                        <option value="800000">800.000</option>
                                        <option value="900000">900.000</option>
                                        <option value="1000000">1.000.000</option>
                                        <option value="1500000">1.500.000</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="preco_max" class="selectpicker" data-live-search="false" title="Valor maxímo" data-width="100%">
                                        <option value="100000">100.000</option>
                                        <option value="150000">150.000</option>
                                        <option value="200000">200.000</option>
                                        <option value="300000">300.000</option>
                                        <option value="400000">400.000</option>
                                        <option value="500000">500.000</option>
                                        <option value="600000">600.000</option>
                                        <option value="700000">700.000</option>
                                        <option value="800000">800.000</option>
                                        <option value="900000">900.000</option>
                                        <option value="1000000">1.000.000</option>
                                        <option value="1500000">1.500.000</option>
                                        <option value="2000000">2.000.000</option>
                                        <option value="2500000">2.500.000</option>
                                        <option value="3000000">3.000.000+</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input name="id" type="text" class="form-control" placeholder="Código">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2">
                        <buttton class="btn btn-danger btn-procurar" type="button" onclick="PesquisaScript.funcs.pesquisar(0);"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><br>Procurar</buttton>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <hr>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-3">
                    <div class="row" style="display: flex; align-items: center;">
                        <label for="ordenar_por" class="pull-left" style="margin-right: auto;">Ordenar: </label>
                        <select id="ordenar_por" class="form-control pull-right" style="width: 200px">
                            <option value="valor ASC, id_tipo ASC">Menor ao maior</option>
                            <option value="valor DESC, id_tipo">Maior ao menor</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row resultado-pesquisa">
            <div class="imoveis-resultado-pesquisa">
                <div class="col-md-4 previa-imovel" id="imovel-visualizacao" style="display: none;">
                    <div class="imovel">
                        <a href="#" class="imovel-url">
                            <span class="finalidade">Venda</span>
                            <div class="col-md-12 img-imovel"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="tipo-imovel"></h3>
                                </div>
                                <div class="col-md-6">
                                    <p class="descricao"></p>
                                </div>
                                <div class="col-md-12">
                                    <p class="valor"><i class="glyphicon glyphicon-triangle-right"></i><span class="imovel-valor"></span></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-12">
        <div class="center-block" style="display: table;">
            <div id="pagination" class="pagination-holder clearfix">
                <div id="light-pagination" class="pagination light-theme simple-pagination">
                </div>
            </div>
        </div>
    </div>
</div>