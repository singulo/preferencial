    </body>

    <!--  MODERNIZR  -->
    <script type="text/jscript" src="<?= base_url('assets/js/modernizr.js'); ?>" ></script>

    <script type="text/jscript" src="<?= base_url('assets/plugins/bootstrap-3.3.6/js/bootstrap.min.js'); ?>" ></script>

    <!--   MULTISELECT  -->
    <script type="text/jscript" src="<?= base_url('assets/plugins/bootstrap-select/bootstrap-select.min.js'); ?>" ></script>

    <!--   VALUE SLIDER  -->
    <script type="text/jscript" src="<?= base_url('assets/plugins/bootstrap-slider/bootstrap-slider.js'); ?>" ></script>

    <!--  JSONIFY  -->
    <script type="text/jscript" src="<?= base_url('assets/simples/js/jsonify.js'); ?>"></script>

    <!--FANCYBOX-->
    <script src="<?= base_url('assets/plugins/fancybox/jquery.fancybox.pack.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/fancybox/jquery.mousewheel-3.0.6.pack.js'); ?>"></script>

    <!-- OWL CAROUSEL -->
    <script src="<?= base_url('assets/plugins/owl-carousel/owl.carousel.js'); ?>"></script>

    <!--  AUTONUMERIC  -->
    <script type="text/jscript" src="<?= base_url('assets/js/autoNumeric.min.js'); ?>"></script>

    <!-- MASK INPUT -->
    <script type="text/jscript" src="<?= base_url('assets/js/jquery.maskedinput.js'); ?>"></script>

    <!-- ALERTIFY -->
    <script src="<?= base_url('assets/plugins/alertify/js/alertify.js'); ?>"></script>

    <!-- PAGINATION -->
    <script src="<?= base_url('assets/plugins/simplePagination/jquery.simplePagination.js'); ?>"></script>

    <!-- VALIDATION -->
    <script src="<?= base_url('assets/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>

    <script src="<?=base_url_filial('modules/simples/assets/js/cliente.js', false)?>"></script>
    <script src="<?=base_url('modules/simples/assets/js/imovel.js')?>"></script>
    <script src="<?=base_url_filial('modules/simples/assets/js/simples.js',false)?>"></script>
    <script src="<?=base_url_filial('modules/simples/assets/js/contato.js',false)?>"></script>

    <!--  IMOVEL  -->
    <script type="text/jscript" src="<?= base_url('assets/simples/js/imovel.js'); ?>"></script>
    <!--  FILTRO  -->
    <script type="text/jscript" src="<?= base_url('assets/js/filtro.js'); ?>"></script>

    <!--  CUSTOM  -->
    <script type="text/jscript" src="<?= base_url('assets/js/custom.js'); ?>" ></script>

    <script>
        // JS start | Multiselect -->
        $('.selectpicker').selectpicker({
            style: 'btn-multselect',
            //size: 44
        });

        var bairros_cidades = <?= json_encode($_SESSION['filial']['bairros_imoveis']); ?>

        var cidades_bairros = [];

        $.each(bairros_cidades, function(index, bairro){
            if(cidades_bairros[bairro.cidade] == undefined || cidades_bairros[bairro.cidade].length == 0)
            {
                cidades_bairros[bairro.cidade] = [];
            }

            cidades_bairros[bairro.cidade].push(bairro.bairro);
        });
    </script>


<!--Google Analytics-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-88020286-1', 'auto');
        ga('send', 'pageview');

    </script>


</html>