<div class="container msg-bem-vindo">
    <div class="col-md-8">
        <p>A Preferencial Imóveis busca oferecer ótimas oportunidades de mercado para quem procura o imóvel dos sonhos e para investidores, tendo como objetivo principal, atender nossos clientes proporcionando além dos nossos conhecimentos no mercado imobiliário um trabalho com coerência, ética e confiabilidade.</p>
    </div>
    <div class="col-md-4">
        <h3>Seja bem-vindo</h3>
        <p>A seguir veja alguns dos nossos destaques.</p>
    </div>
</div>

<!-- DESTAQUES -->
<div class="imoveis-destaques">
    <div class="container">
        <div class="owl-destaques">
            <? foreach($destaques as $imovel) : ?>
                <div class="item imovel">
                    <a href="<?= imovel_url($imovel); ?>">
                        <? $finalidade = Finalidades::toString($imovel->finalidade);?>
                        <span class="finalidade"><?= $finalidade; ?></span>
                        <img src="<?= json_decode($imovel->fotos_destaque)[0]->arquivo_grande; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="tipo"><?= $imovel->tipo_extenso; ?></h3>
                            </div>
                            <div class="col-md-6">
                                <p class="descricao"><?= $imovel->descricao_abreviada; ?></p>
                            </div>
                            <div class="col-md-12">
                                <p class="valor"><i class="glyphicon glyphicon-triangle-right"></i><?= formatar_valor($imovel->valor_site, 'R$ '); ?></p>
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

<? if(! is_null($condominio_destaque)) : ?>
    <? $this->load->helper('text'); ?>
    <div class="col-xs-12 imovel-destaque-maior">
        <div class="row descricao">
            <div class="col-md-6 hidden-xs hidden-sm">
                <div class="col-md-7 pull-right">
                    <h2><?= $condominio_destaque->nome; ?></h2>
                    <div class="col-xs-12">
                        <hr>
                    </div>
                    <p><?= character_limiter($condominio_destaque->descricao, 370); ?></p>
                </div>
            </div>
            <div class="foto">
                <div class="col-md-6 hidden-xs hidden-sm">
                    <div class="row">
                        <a href="<?= base_url('condominio?id=' . $condominio_destaque->id); ?>" alt="<?= 'Condomínio Fechado ' . $condominio_destaque->nome; ?>">
                            <div style="height: 500px; background-image: url(<?= json_decode($condominio_destaque->fotos_destaque)[0]; ?>), url(<?= $GLOBALS[CONFIG_IMG_CONDOMINIO_SEM_FOTO]; ?>); background-position: center; background-repeat: no-repeat;background-size: contain;"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>


