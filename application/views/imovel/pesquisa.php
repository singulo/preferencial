<!doctype html>
<html lang="pt-BR">
<head>
    <? if(isset($imovel)): ?><!-- IMOVEL -->
    <?= html_titulo_elemento_imovel($imovel); ?>
    <?= html_meta_tags_facebook_imovel($imovel); ?>
    <?= html_meta_tags_imovel($imovel); ?>
    <? elseif(isset($condominio)): ?><!-- CONDOMINIO -->
<?= html_titulo_elemento_condominio($condominio); ?>
<?= html_meta_tags_facebook_condominio($condominio); ?>
<?= html_meta_tags_condominio($condominio); ?>
    <? else: ?><!-- IMOBILIARIA/FILIAL -->
<?= html_titulo_elemento_filial(); ?>
<?= html_meta_tags_facebook_filial(); ?>
<?= html_meta_tags_filial(); ?>
<? endif; ?>

    <link rel="shortcut icon" type="image/png" href="<?= $GLOBALS[CONFIG_IMOBILIARIA_FAVICON]; ?>"/>

    <? $this->load->view('templates/styles', array('appendStyles' => array(
        base_url('assets/pages/templates/menu/menu.css'),
        base_url('assets/pages/templates/rodape/rodape.css'),
        base_url('assets/pages/imovel/pesquisa.css')
    ))); ?>
</head>
<body>

<? $this->load->view('simples/templates/script-facebook'); ?>
<? $this->load->view('simples/templates/script-google'); ?>
<? $this->load->view('simples/templates/fab-whatsapp'); ?>
<? $this->load->view('simples/templates/carregar-modais-ambiente'); ?>

<? $this->load->view('layouts/menu'); ?>

<? $this->load->view('layouts/imovel/pesquisa'); ?>

<? $this->load->view('layouts/rodape'); ?>

</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' => array())); ?>
</footer>
</html>


