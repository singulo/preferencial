<!-- Modal -->
<div class="modal fade" id="modal-mais-info-login" tabindex="-1" role="dialog" aria-labelledby="maisInfoModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Quer mais informações?</h4>
            </div>
            <div class="modal-body">
                <p class="text-center">Efetue o <strong>login</strong> ou <strong>cadastre-se</strong> <br>para ver <strong>todas as fotos</strong> deste e outros imóveis!</p>
                <button type="button" data-target="#modal-login" data-toggle="modal" data-dismiss="modal" class="btn btn-danger center-block" style="margin-top: 15px">Entrar/Cadastrar</button>
            </div>
        </div>
    </div>
</div>
