<!-- Modal -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Entre com suas crendenciais</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form-login" onsubmit="cliente_login(); return false;">
                        <div class="form-group col-xs-12">
                            <input class="form-control" type="email" name="email" placeholder="Email">
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group col-xs-12">
                    <button type="button" onclick="cliente_login()" data-loading-text="Aguarde..." class="btn btn-danger btn-login" autocomplete="off">Verificar</button>
                </div>
            </div>
        </div>
    </div>
</div>
