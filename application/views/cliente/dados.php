<!-- Modal -->

<? require_once MODULESPATH . 'simples/helpers/endereco_helper.php'; ?>

<?  $estados = obter_estados_brasil();?>
<div class="modal fade" id="modal-dados" tabindex="-1" role="dialog" aria-labelledby="dadosModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seus dados</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <? if ($this->session->has_userdata('usuario')): ?>
                        <form id="form-dados" onsubmit="cliente_salvar_dados(); return false;">
                            <div class="form-group col-xs-12">
                                <input class="form-control" type="text" name="nome" placeholder="Nome" value="<?= $this->session->userdata('usuario')->nome; ?>">
                            </div>
                            <div class="form-group col-xs-6">
                                <input class="form-control" type="email" name="email" placeholder="Email" disabled value="<?= $this->session->userdata('usuario')->email; ?>">
                            </div>
                            <div class="form-group col-xs-6">
                                <input class="form-control telefone" type="text" name="telefone_1" placeholder="Telefone" value="<?= $this->session->userdata('usuario')->telefone_1; ?>">
                            </div>
                            <div class="form-group col-xs-6">
                                <select name="uf" class="selectpicker"  onchange="obter_cidades($('#form-dados'));" data-live-search="true" title="Estado" data-width="100%">
                                    <? foreach ($estados as $uf => $estado) : ?>
                                        <option value="<?= strtoupper($uf); ?>" <? if(strtoupper($this->session->userdata('usuario')->uf) == strtoupper($uf)) : echo  "selected"; endif;?>><?= $estado; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <select name="cidade" class="selectpicker" data-cidade-default="<?= strtoupper($this->session->userdata('usuario')->cidade); ?>" data-live-search="true" disabled data-width="100%" title="Cidade"></select>
                            </div>
                        </form>
                    <?endif; ?>

                    <hr>

                    <div class="col-xs-12 corretor">
                        <div class="text-center"><h5>Seu corretor</h5></div>
                        <div class="col-md-5">
                            <img class="img-responsive img-circle corretor-foto" src="<?= $_SESSION['filial']['fotos_corretores'] . $this->session->userdata('usuario')->corretor->id_corretor . '.jpg' ?>" alt="Corretor Itrend" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';" />
                        </div>
                        <div class="col-md-7">
                            <p><h3 class="corretor-nome"><?= $this->session->userdata('usuario')->corretor->nome; ?></h3></p>
                            <p class="corretor-email"><?= $this->session->userdata('usuario')->corretor->email; ?></p>
                            <p><small class="corretor-creci">CRECI: <?= $this->session->userdata('usuario')->corretor->creci; ?></small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group col-xs-12">
                    <button type="button" onclick="cliente_salvar_dados()" data-loading-text="Aguarde..." class="btn btn-danger btn-salvar" autocomplete="off">Salvar</button>
                    <button type="button" onclick="cliente_logout();" class="btn btn-default">Sair</button>
                </div>
            </div>
        </div>
    </div>
</div>
