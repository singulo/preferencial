<div class="condominios-apresentacao-rodape col-xs-12">
    <div class="row">
        <div class="col-md-6 texto text-right">
            <h2>Condomínios Fechados</h2>
            <p>Os imóveis mais luxuosos do litoral norte em<br>condomínios com total infraestrutura e segurança <br> pra sua família.</p>
            <h4 style="font-weight: 700; margin-top: 30px;"><em>Encontre o melhor aqui:</em></h4>
            <div class="row">
                <form action="<?= base_url('imovel/pesquisar'); ?>" method="GET">
                    <div class="col-md-11">
                        <select name="id_condominio[]" class="selectpicker pull-right select-condominios" data-live-search="true" title="CONDOMÍNIOS" multiple>
                            <? foreach($_SESSION['filial']['condominios'] as $condominio) : ?>
                                <option value="<?= $condominio->id; ?>"><?= $condominio->nome; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-default">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6 condominios">
            <div class="col-md-8">
                <div class="owl-condominios-apresentacao-rodape">
                    <? foreach(array_chunk($_SESSION['filial']['condominios'], 2) as $condominios) : ?>
<!--                        --><?// if($condominios[0]->fechado || (isset($condominios[1]) && $condominios[1]->fechado)) : ?>
                            <div class="item">
                                <a href="<?= base_url('condominio?id=' . $condominios[0]->id); ?>">
                                    <div class="imagem" style="background-image: url(<?= json_decode($condominios[0]->fotos_destaque)[0]->arquivo_pequeno; ?>), url(<?= $GLOBALS[CONFIG_IMG_CONDOMINIO_SEM_FOTO]; ?>)"></div>
                                </a>
                                <? if(isset($condominios[1])) : ?>
                                    <a href="<?= base_url('condominio?id=' . $condominios[1]->id); ?>">
                                        <div class="imagem hidden-xs hidden-sm" style="background-image: url(<?= json_decode($condominios[1]->fotos_destaque)[0]->arquivo_pequeno; ?>), url(<?= $GLOBALS[CONFIG_IMG_CONDOMINIO_SEM_FOTO]; ?>)"></div>
                                    </a>
                                <? endif; ?>
                            </div>
<!--                        --><?// endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>