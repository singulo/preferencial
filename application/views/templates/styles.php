<!-- SimplesPlugins -->
<link href="<?= base_url('assets/plugins/simples-plugins/css/simples-plugins-v1.0.css');?>" rel="stylesheet">
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<!-- FontAwesome -->
<link href="<?= base_url('assets/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
<!-- SIMPLES -->
<link href="<?= base_url('modules/simples/assets/css/simples.css');?>" rel="stylesheet">
<? if(isset($GLOBALS[FILIAL_CSS_CAMINHO_TERCEIROS])): ?>
    <? if( ! is_array($GLOBALS[FILIAL_CSS_CAMINHO_TERCEIROS])): ?>
        <? $GLOBALS[FILIAL_CSS_CAMINHO_TERCEIROS] = [$GLOBALS[FILIAL_CSS_CAMINHO_TERCEIROS]]; ?>
    <? endif; ?>
    <? foreach($GLOBALS[FILIAL_CSS_CAMINHO_TERCEIROS] as $arquivo): ?>
        <? $appendStyles[] = $arquivo; ?>
    <? endforeach ?>
<? endif; ?>
<? if(isset($appendStyles)) : ?>
    <? foreach($appendStyles as $style): ?>
        <link rel="stylesheet" href="<?= $style; ?>">
    <? endforeach; ?>
<? endif; ?>
<?
    $GLOBALS['variaveisJS'] = [
        'FilialSessao'                      => $GLOBALS[FILIAL_CHAVE],
        'BaseUrl'                           => base_url(),
        'FilialFotosImoveis'                => $GLOBALS[FILIAL_FOTOS_IMOVEIS_CAMINHO_EXTERNO],
        'FilialFotosCondominios'            => $GLOBALS[FILIAL_FOTOS_CONDOMINIOS_CAMINHO_EXTERNO],
        'ImovelTipos'                       => SiteFilial::imovelTipos(),
        'CondominioStatus'                  =>array_flip(CondominioStatus::getConstants()),
        'Finalidades'                       => array_flip(Finalidades::getConstants()),
        'FilialFotosCorretores'             => $GLOBALS[FILIAL_FOTOS_CORRETORES_CAMINHO_EXTERNO],
        'ImgImovelSemFoto'                  => $GLOBALS[CONFIG_IMG_IMOVEL_SEM_FOTO],
        'ImgCondominioSemFoto'              => $GLOBALS[CONFIG_IMG_CONDOMINIO_SEM_FOTO],
        'ImgUsuarioSistemaSemFoto'          => $GLOBALS[CONFIG_IMG_USUARIO_SISTEMA_SEM_FOTO],
        'ImgMapaMarcador'                   => $GLOBALS[CONFIG_IMOBILIARIA_MARCADOR_MAPA],
        'GifCarregando'                     => $GLOBALS[CONFIG_IMOBILIARIA_GIF_CARREGANDO],
        'ImovelUsarCodigoReferenciaComoId'  => SiteFilial::configs()->usar_codigo_referencia_imovel,
        'RotaChamada'                       => $this->router->fetch_class() . '/' . $this->router->fetch_method(),
        'FacebookAPIHabilitada'             => facebook_api_habilitada(),
        'GooglePeopleAPIHabilitada'         => google_people_api_habilitada(),
        'SimplesLink'                       => SISTEMA_SIMPLES_LINK
    ];
    if(isset($GLOBALS[FILIAL_ARQUIVOS_CONFIG_INTERACOES_EXTERNO])) :
        $GLOBALS['variaveisJS']['SiteConfigInteracoesJsonExterno'] = $GLOBALS[FILIAL_ARQUIVOS_CONFIG_INTERACOES_EXTERNO];
    endif;
    if($this->session->has_userdata('usuario')) :
        $GLOBALS['variaveisJS']['Usuario'] = $this->session->userdata('usuario');
    endif;
    if(isset($imovel)) :
        $GLOBALS['variaveisJS']['Imovel'] = $imovel;
    endif;
    if(isset($condominio)) :
        $GLOBALS['variaveisJS']['Condominio'] = $condominio;
    endif;
?>