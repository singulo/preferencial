<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Imobiliária referênciada de qualidade, inovadora, que atrai investidores e desenvolve ótimas oportunidades de mercado litoral norte. Xangri-lá, Capão da Canoas, Atlântida.">
    <meta name="keywords" content="empresa, sobre, institucional, imobiliária, litoral norte, xangri-la, atlantida">
    <meta name="classification" content="imoveis, imobiliaria">
    <meta name="robots" content="index,follow">
    <meta name="author" content="Simples Imob">
    <meta name="reply-to" content="<?= $_SESSION['filial']['email_padrao']; ?>">
    <meta name="copyright" content="www.simplesimob.com.br">
    <meta http-equiv="content-language" content="pt-br">
    <meta http-equiv="content-language" content="pt">
    <title><?= $_SESSION['filial']['nome']; ?></title>
    <link rel="icon" href="<?= base_url('assets/images/favicon.ico'); ?>" type="image/x-icon">

    <!-- CSS Bootstrap | Arquivos padrões -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/bootstrap-3.3.6/css/bootstrap.min.css'); ?>">

    <!--  BOOTSTRAP MULTSELECT -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/bootstrap-select/bootstrap-select.min.css'); ?>">

    <!--  BOOTSTRAP VALOR SLIDER  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/bootstrap-slider/bootstrap-slider.css'); ?>">

    <!--  ALERTIFY  -->
    <link href="<?= base_url('assets/plugins/alertify/css/alertify.css'); ?>" rel="stylesheet" type="text/css"/>

    <!-- PAGINATION -->
    <link href="<?= base_url('assets/plugins/simplePagination/simplePagination.css'); ?>" rel="stylesheet" type="text/css"/>

    <!-- FANCYBOX -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/fancybox/jquery.fancybox.css'); ?>">

    <!-- OWL CAROUSEL -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/owl-carousel/assets/owl.carousel.css'); ?>">
<!--    <link rel="stylesheet" href="--><?//= base_url('assets/plugins/owl-carousel/assets/owl.theme.css'); ?><!--">-->

    <!--  CUSTOM  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/custom.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css'); ?>">

    <!--    JS -->
    <script type="text/jscript" src="<?= base_url('assets/js/jquery-2.2.0.min.js'); ?>" ></script>

    <!--  Font Google  -->
    <link href='https://fonts.googleapis.com/css?family=Istok+Web:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="apple-touch-icon" href="<?= base_url('assets/images/icones-mobile/Icon-Small-50@2x.png'); ?>">

    <? if(isset($appendTags)) : ?>
        <? foreach($appendTags as $tag) : ?>
            <?= $tag; ?>
        <? endforeach; ?>
    <? endif; ?>

</head>
<body id="page-wrap">

    <? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>
    <input id="base_url" type="hidden" value="<?= base_url()?>">
    <input id="filial_fotos_imoveis" type="hidden" value="<?= $_SESSION['filial']['fotos_imoveis']; ?>">
    <input id="filial_sessao" type="hidden" value="<? if(isset($_SESSION['filial']['chave'])) {echo  $_SESSION['filial']['chave'];}?>">
    <input id="usuario" type="hidden" value='<?= json_encode($this->session->userdata('usuario')); ?>'>

    <? $this->load->library('simples/Finalidades'); ?>

    <script>
        var imoveis_tipos = <?= json_encode($_SESSION['filial']['tipos_imoveis']); ?>;
        var finalidades = <?= json_encode(array_flip(Finalidades::getConstants())); ?>
    </script>
