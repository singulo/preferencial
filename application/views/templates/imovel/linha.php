<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/imovel/linha/linha.css'); ?>">

<!-- IMOVEIS_LINHA_TITULO -->
<div class="imoveis-linha col-xs-12">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="<?= base_url('assets/images/arrow-up.png'); ?>" class="img-responsive">
                <?= $imoveis_linha_titulo; ?>
                <hr>

                <div class="pull-right">
                    <button type="button" class="btn btn-default prev" aria-label="Left Align">
                        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default next" aria-label="Left Align">
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
        </div>
        <div class="row owl-imoveis-linha">
            <? foreach($imoveis_linha as $imovel) : ?>
                <div class="imovel item">
                    <figure>
                        <img src="http://placehold.it/350x250" class="img-responsive">
                        <figcaption>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4><?= $imovel['tipo']; ?><br><small>R$<?= $imovel['valor']; ?></small></h4>
                                </div>
                                <div class="col-md-6">
                                    <p><span class="glyphicon glyphicon-map-marker"></span> <?= $imovel['cidade']; ?></p>
                                    <p><span class="glyphicon glyphicon-bed"></span> <?= $imovel['dormitorios']; ?> dormitórios</p>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

<script type="text/jscript" src="<?= base_url('assets/pages/templates/imovel/linha/linha.js'); ?>"></script>