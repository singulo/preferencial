<?
    if(isset($addVariaveisSistema)) :
        foreach ($addVariaveisSistema as $index => $variavel):
            $GLOBALS['variaveisJS'][$index] = $variavel;
        endforeach;
    endif;

    if( ! isset($GLOBALS['variaveisJS']['Filtro']))
        $GLOBALS['variaveisJS']['Filtro'] = new stdClass();

    $GLOBALS['variaveisJS']['Filtro']->config = obter_objeto_config_filtro();

?>
<script>
    var VariaveisSistema = <?= json_encode($GLOBALS['variaveisJS']); ?>
</script>
<!-- Jquery -->
<script src="<?=base_url('assets/plugins/jquery/jquery-2.2.0.min.js');?>"></script>
<!-- SimplesPlugins -->
<script src="<?=base_url('assets/plugins/simples-plugins/js/simples-plugins-v1.0.js');?>"></script>
<!-- SimplesModuloPlugins -->
<script src="<?=base_url('modules/simples/assets/plugins/simples-modulo-plugins-v1.0.js');?>"></script>
<!--Google maps-->
<script src="https://maps.googleapis.com/maps/api/js<?= (ENVIRONMENT == 'production') ? '?key=' . $GLOBALS[FILIAL_GOOGLE_MAPS_KEY] : ''; ?>"></script>
<!-- SIMPLES -->
<script src="<?=base_url('modules/simples/assets/js/simples.js');?>"></script>
<script src="<?=base_url('modules/simples/assets/js/cliente.js');?>"></script>
<script src="<?=base_url('modules/simples/assets/js/filtro-slider.js');?>"></script>
<script src="<?=base_url('modules/simples/assets/js/filtro-slider-mobile.js');?>"></script>
<script src="<?=base_url('modules/simples/assets/js/pesquisa.js');?>"></script>
<script src="<?=base_url('modules/simples/assets/js/gmap.js');?>"></script>

<? if(isset($GLOBALS[FILIAL_JS_CAMINHO_TERCEIROS])):
    if( ! is_array($GLOBALS[FILIAL_JS_CAMINHO_TERCEIROS])):
        $GLOBALS[FILIAL_JS_CAMINHO_TERCEIROS] = [$GLOBALS[FILIAL_JS_CAMINHO_TERCEIROS]];
    endif;
    if( ! isset($appendScripts)):
        $appendScripts = [];
    endif;
    foreach($GLOBALS[FILIAL_JS_CAMINHO_TERCEIROS] as $arquivo):
        $appendScripts[] = $arquivo;
    endforeach;
endif; ?>
<? if (isset($appendScripts)):
    foreach ($appendScripts as $script): ?>
        <script src="<?= $script; ?>"></script>
    <? endforeach;
endif; ?>