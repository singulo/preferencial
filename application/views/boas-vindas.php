<? $this->load->view('templates/header'); ?>

<div class="container msg-boas-vindas">

    <div class="col-md-9 center-block" style="float: none">

        <img src="<?= base_url('assets/images/logo.png'); ?>" alt="Logo Preferencial" class="img-responsive center-block" >

        <h3 class="text-center " >Nestas férias, vai ter muita diversão com a família na praia!</h3>

        <form method="get" action="<?= base_url('imovel/pesquisar'); ?>">
            <div>
                <div class="col-md-4">
                    <select name="cidade" class="selectpicker" data-live-search="false" title="CIDADE" data-width="100%">
                        <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                            <option value="<?= $cidade->cidade; ?>"><?= $cidade->cidade; ?></option>
                        <? endforeach; ?>
                    </select>
                </div>

                <div class="col-md-4">
                    <select name="bairro[]" class="selectpicker" data-live-search="false" title="BAIRRO" data-width="100%" multiple>
                        <option value="" disabled >--Selecione uma cidade--</option>
                    </select>
                </div>

                <div class="col-md-4">
                    <select name="tipo" class="selectpicker" data-live-search="false" title="TIPO DE IMÓVEL" data-width="100%" multiple>
                        <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                            <option value="<?= $tipo->id; ?>"><?= $tipo->tipo; ?></option>
                        <? endforeach; ?>
                    </select>
                </div>

                <!--
                <div class="col-md-4">
                    <select name="finalidade" class="selectpicker" data-live-search="false" title="FINALIDADE" data-width="100%" multiple>
                        <?/* foreach(array_flip(Finalidades::getConstants()) as $id => $finalidade) :*/?>
                        <option value="<?/*= $id;*/?>"><?/*= $finalidade; */?></option>
                        <?/* endforeach; */?>
                    </select>
                </div>
                -->
            </div>
        </div>

        <div class="col-xs-12">
            <button class="center-block btn-enviar" type="submit" style="margin-top: 50px">ENTRAR</button>
        </div>
    </form>
</div>

<? $this->load->view('templates/footer'); ?>

<style>

html
{
    height: 100%;
}

body
{
    background: url("<?= base_url('assets/images/bg-boas-vindas.jpg'); ?>");
    background-size: auto;
    background-position: center;
    background-repeat: no-repeat;
}

.msg-boas-vindas
{
    padding-top: 5%;
}

.bootstrap-select .dropdown-toggle
{
    background-color: transparent;
    color: white;
    margin-top: 30px;
}

select option
{
    color: black;
}

.btn-enviar
{
    font-size: 30px;
}

h2,
.btn-enviar,
h3,
select
{
    background-color: transparent;
    border: transparent;
    color: white;
    font-weight: 800;
    font-style: italic;
}

h2 a
{
    margin-top: 30px;
}

</style>

<script>

    var bairros_cidades = <?= json_encode($_SESSION['filial']['bairros_imoveis']); ?>

    var cidades_bairros = [];

    $.each(bairros_cidades, function(index, bairro){
        if(cidades_bairros[bairro.cidade] == undefined || cidades_bairros[bairro.cidade].length == 0)
        {
            cidades_bairros[bairro.cidade] = [];
        }

        cidades_bairros[bairro.cidade].push(bairro.bairro);
    });

    $('form select[name="cidade"]').on('change', function(){

        $('form select[name="bairro[]"]').html('');

        $.each(cidades_bairros[$('form select[name="cidade"]').val()], function(index, bairro){
            $('form select[name="bairro[]"]').append('<option value="'+ bairro + '">' + bairro + '</option>');
        });

        $('form select[name="bairro[]"]').selectpicker('refresh');
    });
</script>