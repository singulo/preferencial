<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Corretores_Model $corretores_model
 */
class Quemsomos extends Base_Controller {

	public function index()
	{
		$this->load->model('simples/corretores_model');
		$data['corretores'] = $this->corretores_model->listar_todos();

		$this->load->view('quem-somos', $data);;
	}
}
