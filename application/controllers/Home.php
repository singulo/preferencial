<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Imovel_Model $imovel_model
 * @property Condominio_Model $condominio_model
 */
class Home extends Base_Controller {

	public function index()
	{
		$this->load->model('simples/imovel_model');
		$this->load->model('simples/condominio_model');

		$data['condominio_destaque'] = $this->condominio_model->destaques(1);

		if(! is_null($data['condominio_destaque']) && count($data['condominio_destaque']) > 0)
			$data['condominio_destaque'] = $data['condominio_destaque'][0];
		else
			$data['condominio_destaque'] = null;

		$data['destaques'] = $this->imovel_model->destaques(10);

		$this->load->view('home', $data);
	}
}
