<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "../modules/simples/core/Base_Controller.php";

class BoasVindas extends Base_Controller
{
	public function index()
	{
		$this->load->view('boas-vindas');
	}
}
