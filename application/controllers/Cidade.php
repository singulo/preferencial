<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "../modules/simples/controllers/Base_cidade_controller.php";

class Cidade extends Base_Cidade_Controller
{
    public function obter_cidade_pela_uf()
    {
        $data = parent::obter_cidade_pela_uf();
        $data['status'] = true;

        echo json_encode($data);
    }
}