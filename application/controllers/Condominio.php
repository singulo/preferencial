<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require MODULESPATH . "simples/controllers/Base_condominio_controller.php";

class Condominio extends Base_Condominio_Controller
{
    public function index()
    {
        $data = parent::index();

        if(isset($data['condominio']))
        {
            $this->load->view('condominio/detalhes', $data);
        }
        else
            die('Condomínio não encontrado');
    }

    public function todos_fechados()
    {
        $data = parent::todos_fechados();
        $data['titulo'] = 'Condomínios Fechados';
        $data['texto'] = '';//'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vestibulum faucibus est a lacinia. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis blandit consequat nunc, a convallis justo cursus et. Aliquam augue nisi, mattis ut condimentum vel, ullamcorper rutrum nulla.';

        $this->load->view('condominio/lista', $data);
    }

    public function lancamentos()
    {
        $data = parent::todos_lancamentos();
        $data['titulo'] = 'Lançamentos';
        $data['texto'] = '';//'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vestibulum faucibus est a lacinia. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis blandit consequat nunc, a convallis justo cursus et. Aliquam augue nisi, mattis ut condimentum vel, ullamcorper rutrum nulla.';

        $this->load->view('condominio/lista', $data);
    }
}
