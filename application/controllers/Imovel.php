<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require MODULESPATH . "simples/controllers/Base_imovel_controller.php";

class Imovel extends Base_Imovel_Controller
{
    protected $quantidade_fotos_principais = 3;
    protected $quantidade_sugestoes = 4;

    public function __construct()
    {
        parent::__construct();

        $this->filtro_parametros[] = array('param' => 'bairro', 'default_value' => '');
    }

    public function index($imovel = NULL)
    {
        $data = parent::index($imovel);

        if( ! is_null($data['imovel']))
            $this->load->view('imovel/detalhes', $data);
        else
            die('Imóvel não encontrado ou não existe mais. Voltar a <a href="' . base_url() . ' ">página inicial</a>.');
    }

    public function pesquisar()
    {
        $data = parent::pesquisar();

        if(isset($_GET['valores']))
        {
            $data['filtro']->preco_min = $_GET['valores'][0];

            if(isset($_GET['valores'][1]))
                $data['filtro']->preco_max = $_GET['valores'][1];
        }

        $data['complementos'] = $this->imovel_model->todos_complementos();

        $this->load->view('imovel/pesquisa', $data);
    }

    public function buscar()
    {
        if( ! is_null($this->input->post('ordenacao')))
            $this->pesquisa_order_by = $this->input->post('ordenacao');

        if(isset($_POST['filtro']['preco_min']) && $_POST['filtro']['preco_min'] > 0)
            $_POST['filtro']['preco_min'] = (float)$_POST['filtro']['preco_min'];
        if(isset($_POST['filtro']['preco_max']) && $_POST['filtro']['preco_max'] > 0)
            $_POST['filtro']['preco_max'] = (float)$_POST['filtro']['preco_max'];

        $data = parent::buscar();
        $data['status'] = true;

        echo json_encode($data);
    }
}
