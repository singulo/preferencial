<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Imovel_Model $imovel_model
 * @property Link_Personalizado_Model $link_personalizado_model
 * @property Imovel $imovel
 */
class Pagina extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('simples/imovel_model');
        $this->load->model('simples/link_personalizado_model');
    }

    public function nao_encontrada()
    {
        $uri_string = uri_string();

        $imovel = $this->imovel_model->pelo_seo_link($uri_string);

        $this->load->library('../controllers/imovel');

        if(! is_null($imovel))
            $this->imovel->index($imovel);
        else
        {
            $link = $this->link_personalizado_model->pelo_link_personalizado($uri_string);

            if( ! is_null($link))
            {
                if( ! is_null($link->id_imovel))
                {
                    $_GET['id'] = $link->id_imovel;

                    $this->imovel->index();
                }
                else
                    redirect($link->link_original, 'location');
            }
            else
                show_404();
        }
    }
}
