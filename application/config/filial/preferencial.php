<?php
$GLOBALS[FILIAL_ID_SIMPLESIMOB]                                       = 3;
$GLOBALS[FILIAL_NOME]                                                 = 'Preferencial Imóveis';
$GLOBALS[FILIAL_CHAVE]                                                = 'preferencial';
$GLOBALS[FILIAL_BANNER_CAMINHO_EXTERNO]                               = 'https://www.simplesimob.com.br/testes/imobiliarias/modelo/arquivos/banners/';
$GLOBALS[FILIAL_FOTOS_CORRETORES_CAMINHO_EXTERNO]                     = 'https://www.simplesimob.com.br/testes/imobiliarias/modelo/arquivos/corretores/';
$GLOBALS[FILIAL_FOTOS_CORRETORES_ASSINATURA_CAMINHO_EXTERNO]          = 'https://www.simplesimob.com.br/testes/imobiliarias/modelo/arquivos/corretores/assinaturas/';
$GLOBALS[FILIAL_FOTOS_CORRETORES_ASSINATURA_CAMINHO_INTERNO]          = RAIZPROJETO . 'arquivos/corretores/assinaturas/';
$GLOBALS[FILIAL_FOTOS_CONDOMINIOS_CAMINHO_EXTERNO]                    = 'https://www.simplesimob.com.br/testes/imobiliarias/modelo/arquivos/condominios/fotos/';
$GLOBALS[FILIAL_FOTOS_CONDOMINIOS_CAMINHO_INTERNO]                    = RAIZPROJETO . 'arquivos/condominios/fotos/';
$GLOBALS[FILIAL_FOTOS_IMOVEIS_CAMINHO_EXTERNO]                        = 'https://www.simplesimob.com.br/testes/imobiliarias/modelo/arquivos/imoveis/fotos/';
$GLOBALS[FILIAL_FOTOS_IMOVEIS_CAMINHO_INTERNO]                        = RAIZPROJETO . 'arquivos/imoveis/fotos/';
$GLOBALS[FILIAL_SITE_FOTO_CLIENTE_INTERNO]                            = RAIZPROJETO . 'arquivos/clientes/';
$GLOBALS[FILIAL_LINK]                                                 = base_url('?filial=' . $GLOBALS[FILIAL_CHAVE]);
$GLOBALS[FILIAL_ARQUIVOS_CONFIG_INTERACOES_EXTERNO]                   = base_url('arquivos/site/configuracao/interacoes/interacoes_usuario.json');
$GLOBALS[FILIAL_FACEBOOK_ID_API]                                      = ENVIRONMENT == 'production' ? '249660415689806' : '174020516794864';
$GLOBALS[FILIAL_GOOGLE_CLIENT_ID]                                     = '773007968448-bntks8mm5fsr10ispep79bboejn5hd26.apps.googleusercontent.com';
$GLOBALS[FILIAL_GOOGLE_MAPS_KEY]                                      = 'AIzaSyCIN1JY_p-QMDCkL4Dm9Qo2HqByRzeDYYg';

$GLOBALS[FILIAL_SITE_BANNER_ARQUIVO_INTERNO]                          = RAIZPROJETO . 'arquivos/preferencial/site/banners.json';
$GLOBALS[FILIAL_SITE_IMOVEIS_TIPOS_ARQUIVO_INTERNO]                   = RAIZPROJETO . 'arquivos/preferencial/site/imovel_tipos.json';
$GLOBALS[FILIAL_SITE_DADOS_IMOBILIARIA_ARQUIVO_INTERNO]               = RAIZPROJETO . 'arquivos/preferencial/site/dados_imobiliaria.json';
$GLOBALS[FILIAL_SITE_IMOVEIS_CIDADES_E_BAIRROS_ARQUIVO_INTERNO]       = RAIZPROJETO . 'arquivos/preferencial/site/imoveis_cidades_e_bairros.json';
$GLOBALS[FILIAL_SITE_CONDOMINIOS_CIDADES_E_BAIRROS_ARQUIVO_INTERNO]   = RAIZPROJETO . 'arquivos/preferencial/site/condominios_cidades_e_bairros.json';
$GLOBALS[FILIAL_SITE_CONFIGS_GERAIS_ARQUIVO_INTERNO]                  = RAIZPROJETO . 'arquivos/preferencial/site/configs_gerais.json';

$GLOBALS[FILIAL_CSS_CAMINHO_TERCEIROS]                                = array();

if(ENVIRONMENT == 'production')
    $GLOBALS[FILIAL_CSS_CAMINHO_TERCEIROS][]                          = base_url('assets/filial/css/preferencial.css');

$GLOBALS[FILIAL_JS_CAMINHO_TERCEIROS]                                 = array(base_url('modules/simples/assets/js/facebook-registros.js'), base_url('modules/simples/assets/js/google-registros.js?v=1'));
