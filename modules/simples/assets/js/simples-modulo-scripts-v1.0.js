$.simplesImobModal = {
    ui: {
        modal: {
            id: '#simples-modal-iframe',
            iframeOpcoes: {
                html: {
                    modal: '<div id="simples-modal-iframe" class="modal fade" tabindex="-1" role="dialog"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body no-padding"><div class="col-xs-12"><div class="row iframe-container"><iframe src="" frameborder="0" width="100%" height="0" scrolling="no"></iframe></div></div></div></div></div></div>'
                },
                eventos: {
                    mostrar: function (e) {
                    },      //show.bs.modal
                    mostrando: function (e) {
                    },    //shown.bs.modal
                    esconder: function (e) {
                    },     //hide.bs.modal
                    escondido: function (e) {
                    },    //hidden.bs.modal
                    linkCarregado: function (iframe, titulo) { //Iframe termina de carregar o link

                        if (titulo == '' && iframe.contentWindow.document.querySelector('title') != null)
                            titulo = iframe.contentWindow.document.querySelector('title').text;

                        $(iframe).closest('.modal-dialog').find('h4.modal-title').fadeOut(200, function () {
                            $(this).html(titulo).fadeIn(200);
                        });

                        setTimeout(function () {
                            $(iframe).height(270);
                        }, 500);
                    },
                    clickFinalizar: function () {
                        $($.simplesImob.servicos.ajudante.ui.modal.id).modal('hide');
                    },
                    resultadoAcao: function (dados) {
                    },
                },
                css: {
                    largura: '', // modal-sm | modal-lg | modal-xlg
                },
                titulo: '', // Se vazio é tentado utilizar o texto na tag title do link no iframe
                tituloCarregando: '<div class="carregando"><img src="' + (typeof VariaveisSistema !== 'undefined' && typeof VariaveisSistema.GifCarregando !== 'undefined' ? VariaveisSistema.GifCarregando : '') + '" style="width: 25px; margin-right: 10px; margin-top: -4px;"/> um momento...</div>'
            },
            abrirLinkEmIframe: function (link, opcoes) {
                opcoes = $.simplesImobModal.objeto.extensaoProfunda($.simplesImobModal.ui.modal.iframeOpcoes, opcoes);

                var $modal = $(opcoes.html.modal);
                var uid = Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);
                $modal.attr('data-uid', uid);
                $modal.find('.modal-dialog').addClass(opcoes.css.largura);
                $modal.find('h4.modal-title').html(opcoes.tituloCarregando);

                var $iframe = $($modal.find('iframe'));
                $iframe.attr('src', link);
                $iframe.attr('onload', '$.simplesImobModal.ui.modal.iframeOpcoes.eventos.linkCarregado(this, \'' + opcoes.titulo + '\');');

                document.addEventListener(SimplesImobComponentesEventos.modalIframe.fechar + '_' + uid, function (e) {
                    opcoes.eventos.clickFinalizar(e, e.detail);
                }, {once: true});

                var funcAcaoResultado = function (e) {
                    opcoes.eventos.resultadoAcao(e.detail.dados);

                    if (e.detail.fecharModal) {
                        document.removeEventListener(SimplesImobComponentesEventos.modalIframe.acaoResultado + '_' + uid, funcAcaoResultado);

                        opcoes.eventos.clickFinalizar();
                    }

                };

                document.addEventListener(SimplesImobComponentesEventos.modalIframe.acaoResultado + '_' + uid, funcAcaoResultado, false);

                $modal.on('show.bs.modal', opcoes.eventos.mostrar);
                $modal.on('shown.bs.modal', opcoes.eventos.mostrando);
                $modal.on('hide.bs.modal', opcoes.eventos.esconder);
                $modal.on('hidden.bs.modal', function (e) {
                    opcoes.eventos.escondido(e);
                    e.target.remove();
                });

                $('body').append($modal);

                $modal.modal('show');
            },
            naoPermitirFechar: function (modalId) {
                $(modalId).data('prevent-default', true);
                $(modalId).on('hide.bs.modal', function (e) {
                    if ($(e.target).data('prevent-default'))
                        e.preventDefault();
                });
            },
            permitirFechar: function (modalId) {
                $(modalId).data('prevent-default', false);
            }
        }
    },
    url: {

    },
    objeto: {
        extensaoProfunda: function(objeto, sobrecarga){
            return $.extend( true, {}, objeto, sobrecarga || {});
        }
    },
};

var SimplesImobComponentesEventos = {
    modalIframe: {
        fechar:                 'simples-modal-pai-iframe-fechar',
            acaoResultado:          'simples-modal-pai-iframe-acao-resultado',
    },
    disparar: function(evento, dados, uid){
        if(dados == undefined)
            dados = null;

        parent.document.dispatchEvent(new CustomEvent(evento, { detail: { dados: dados, uid: uid } }));
    }
};