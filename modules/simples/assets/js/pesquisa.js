var PesquisaScript = {
    vars: {
        maxResultado: 12,
        imovelVisualizacao: $('.previa-imovel').clone(),
        tempoDeslocamento: 800,
        msg: {
            nadaEncontrado: 'Nada foi encontrado.'
        }
    },
    events: {
        nadaEncontrado: function(){

        }
    },
    funcs: {
        documentReady: function(){
            if(VariaveisSistema.RotaChamada == 'imovel/pesquisar')
            {
                var pag = $.simplesImob.servicos.ajudante.url.obterParametro('pagina');
                PesquisaScript.funcs.pesquisar(pag);
            }

            $('#ordenar_por').on('change', function(){
                PesquisaScript.funcs.pesquisar(0);
            });

            $('.form-filtro input[name=id]').on('keypress', function(e){
                if(e.keyCode === 13)
                    PesquisaScript.funcs.pesquisar(0);
            });

            String.prototype.capitalize = function() {
                return this.charAt(0).toUpperCase() + this.slice(1);
            }

        },
        pesquisar: function(pagina, forcarTotal)
        {
            if(VariaveisSistema.RotaChamada != 'imovel/pesquisar')
            {
                if($('#filtro-imovel').length)
                    $('#filtro-imovel').submit();
                else if($('#filtro-form-imovel-mobile').length)
                    $('#filtro-form-imovel-mobile').submit();
            }
            else
            {
                var msg_encontrados = '';
                if(pagina > 0)
                    msg_encontrados = $('#msg-resultados-encontrados').html();

                $('#msg-resultados-encontrados').html('Buscando <strong>imóveis</strong>, aguarde ...');
                $('.imoveis-resultado-pesquisa').children().fadeOut(400, function(){
                    $(this).remove();
                });

                VariaveisSistema.Filtro = {};
                VariaveisSistema.Filtro.pagina = pagina;
                VariaveisSistema.Filtro.limite = PesquisaScript.vars.maxResultado;
                VariaveisSistema.Filtro.ordenacao = $('#ordenar_por').val();
                VariaveisSistema.Filtro.forcarTotal = forcarTotal || true;

                if($.simplesImob.servicos.ajudante.acessoMobile())
                    VariaveisSistema.Filtro.params = $('#filtro-form-imovel-mobile').jsonify();
                else
                    VariaveisSistema.Filtro.params = $('#filtro-imovel').jsonify();

                PesquisaScript.funcs._aoIniciarPesquisa();

                $.simplesImob.servicos.imovel.requisitar.pesquisar(
                    VariaveisSistema.Filtro,
                    {
                        sucesso: function (dados) {
                            PesquisaScript.funcs._aoPesquisarComSucessoExibirResultadoDaPesquisa(dados);
                        },
                        completo: function(){
                            PesquisaScript.funcs._aoCompletarPesquisa();
                        }
                    }
                );
            }
        },
        _aoIniciarPesquisa: function(){
            SimplesScript.ui.bloquear($('.resultado-pesquisa .imoveis-resultado-pesquisa'));
        },
        _aoCompletarPesquisa: function(){
            SimplesScript.ui.desbloquear($('.resultado-pesquisa .imoveis-resultado-pesquisa'));
        },
        _aoPesquisarComSucessoExibirResultadoDaPesquisa: function(dados){
            $.each(dados.imoveis, function (key, imovel) {
                $('.resultado-pesquisa .imoveis-resultado-pesquisa').append(PesquisaScript.funcs.montarImovel(imovel));
            });

            if (dados.total != undefined && dados.total > 0)
            {
                $('#msg-resultados-encontrados').html('<strong>Encontramos ' + dados.total + '</strong> <br> <small>ofertas em nosso site.</small>');
                PesquisaScript.funcs.atualizarPaginacao(parseInt(VariaveisSistema.Filtro.pagina) + 1, dados.total);
            }
            else if(dados.total != undefined)
            {
                $('#msg-resultados-encontrados').html(PesquisaScript.vars.msg.nadaEncontrado);
                PesquisaScript.funcs.atualizarPaginacao(parseInt(VariaveisSistema.Filtro.pagina) + 1, 0);
                PesquisaScript.events.nadaEncontrado();
            }
            else
                $('#msg-resultados-encontrados').html(msg_encontrados);

            PesquisaScript.funcs._aoCompletarMontagemDeResultadosDePesquisa();

            SimplesScript.funcs.elemento.posicionar.em($('.resultado-pesquisa .imoveis-resultado-pesquisa').offset().top - 100, PesquisaScript.vars.tempoDeslocamento);
        },
        _aoCompletarMontagemDeResultadosDePesquisa: function(){

        },
        atualizarPaginacao: function(paginaAtual, items)
        {
            $('#pagination').pagination({
                items: items,
                itemsOnPage: PesquisaScript.vars.maxResultado,
                cssStyle: 'light-theme',
                nextText: '',
                prevText: '',
                currentPage: paginaAtual,
                onPageClick : function(pageNumber){
                    PesquisaScript.funcs.pesquisar((pageNumber-1), false);
                }
            });
        },
        montarImovel: function(imovel)
        {
            novaDiv = PesquisaScript.vars.imovelVisualizacao.clone();

            console.log(imovel);

            foto_destaque = JSON.parse(imovel.fotos_destaque)[0].arquivo_grande;

            if(foto_destaque.match(/.(jpg|jpeg|png|gif)$/i))
                novaDiv.find('.img-imovel').attr('style', 'background-image: url(' + foto_destaque + '), url(' + VariaveisSistema.ImgImovelSemFoto + ');');
            else
                novaDiv.find('.img-imovel').attr('style', 'background-image: url(' + VariaveisSistema.ImgImovelSemFoto + ');');

            novaDiv.find('.imovel-url').attr('href', $.simplesImob.servicos.imovel.montarURL(imovel));


            if(VariaveisSistema.ImovelTipos[imovel.id_tipo] != undefined)
                novaDiv.find('.imovel-tipo').text(VariaveisSistema.ImovelTipos[imovel.id_tipo].tipo);
            else
                novaDiv.find('.imovel-tipo').text('Desconhecido');

            var detalhes = [];
            if(imovel.dormitorios > 0 )
                detalhes.push(imovel.dormitorios + ' dorm.');
            if(imovel.area_total > 0 )
                detalhes.push(imovel.area_total + 'm²');
            if(imovel.garagem > 0)
                detalhes.push(imovel.garagem + ' vagas');
            if(imovel.suites > 0)
                detalhes.push(imovel.suites + ' suites');

            if(detalhes.length > 0)
                novaDiv.find('.detalhes').text(detalhes.join(' | '));
            else
                novaDiv.find('.detalhes').html('&#8291;');

            if(imovel.adjetivo == '')
                novaDiv.find('.finalidade').text(VariaveisSistema.Finalidades[imovel.finalidade]);
            else
                novaDiv.find('.finalidade').text(imovel.adjetivo);


            novaDiv.find('.endereco').text(imovel.cidade.capitalize() + ', ' + imovel.bairro.capitalize());

            if(imovel.id_situacao_agenciadora != undefined && VariaveisSistema.CondominioStatus != undefined)
                novaDiv.find('.situacao').html('<b>' + VariaveisSistema.CondominioStatus[imovel.id_situacao_agenciadora].capitalize().replace('_', ' ') + '</b>');

            novaDiv.find('.ribbon').addClass(VariaveisSistema.Finalidades[imovel.finalidade].toLowerCase());
            if(VariaveisSistema.ImovelUsarCodigoReferenciaComoId == true && imovel.codigo_referencia != null && imovel.codigo_referencia.length > 0)
                novaDiv.find('.codigo').text('c.ref: ' + imovel.codigo_referencia);
            else
                novaDiv.find('.codigo').text('cód: ' + imovel.id);

            novaDiv.find('.valor-imovel').html(SimplesScript.funcs.formatarValor(imovel.valor_site, 'R$'));

            novaDiv.show();

            return novaDiv;
        }
    }
};
$(document).ready(function(){
    PesquisaScript.funcs.documentReady();
});