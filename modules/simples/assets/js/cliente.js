var ClienteScript = {
    vars: {
        usuarioSaudado: false,
        modais:{
            loginCadastro: '#modal-login-cadastro',
            contatoInteresse: '#modal-contato-interesse',
        },
        forms: {
            login: '#modal-login-cadastro #form-login',
            cadastrar: '#modal-login-cadastro #form-cadastrar',
            dados: '#modal-cliente-dados #form-cliente-dados',
        },
        contato: {
            interesse: {
                modal: '#modal-contato-interesse',
                form: '#modal-contato-interesse #form-interesse',
                btnEnviar: '#modal-contato-interesse #form-interesse .btn-enviar'
            },
            normal: {
                form: '#form-contato'
            }
        }
    },
    funcs: {
        documentReady: function(){
            $.simplesImob.ambiente.on(
                $.simplesImob.ambienteEventosNome.CLIENTE_LOGOU,
                function(e, usuario){
                    ClienteScript.funcs.acoesDepoisLoginConfirmado(usuario);
                }
            );

            $.simplesImob.ambiente.on(
                $.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU_E_LOGOU,
                function(e, usuario){
                    ClienteScript.funcs.acoesDepoisLoginConfirmado(usuario);
                }
            );
        },
        saudar: function(usuario, aguardarMs){
            setTimeout(function(){
                alertify.success('Olá, ' + usuario.nome);
            }, aguardarMs || 500);
        },
        acoesDepoisLoginConfirmado: function(usuario) {
            if($(ClienteScript.vars.modais.loginCadastro).is(':visible'))
                $(ClienteScript.vars.modais.loginCadastro).modal('hide');

            ClienteScript.funcs.preecherFormInteresse(usuario);

            if( ! ClienteScript.vars.usuarioSaudado)
            {
                ClienteScript.funcs.saudar(usuario);
                ClienteScript.vars.usuarioSaudado = true;
            }

            $.each($('[ao-cliente-logar]'), function(index, elem){
                try{
                    new Function('usuario', $(elem).attr('ao-cliente-logar')).call(this, usuario)
                }
                catch (erro){
                    console.log(erro);
                }
            });
            $('[ao-cliente-logar]:not([cliente-logado-executar-novamente="true"])').removeAttr('ao-cliente-logar');
        },
        login: function(){
            var form = $(ClienteScript.vars.forms.login);

            if(form.valid()){
                var btn = form.find('.btn-login');
                btn.button('loading');
                ClienteScript.funcs.logar(
                    form.find('input[name=email]').val(),
                    function(){
                        btn.button('reset');
                    }
                );
            }
        },
        logar: function(email, chamadaRetornoCompleto){
            $.simplesImob.servicos.usuario.requisitar.tentarLogarEmailSeNecessarioCadastrar(
                email,
                { metodo_cadastro: $.simplesImob.servicos.usuario.vars.metodosDeCadastro.tipos.Normal },
                chamadaRetornoCompleto
            );
        },
        logout: function(){
            alertify
                .okBtn("Sim")
                    .cancelBtn("Não")
                        .confirm("Deseja realmente sair?",
                            function () {
                                document.location.href = VariaveisSistema.BaseUrl + 'cliente/logout';
                            });
        },
        preecherFormInteresse: function(usuario){
            var form = $(ClienteScript.vars.contato.interesse.form);
            form.find('input[name=nome]').val(usuario.nome);
            form.find('input[name=email]').val(usuario.email);
            form.find('input[name=telefone]').val(usuario.telefone_1);
            form.find('textarea[name=obs]').focus();
        },
        cadastrar: function(){
            if( ! $(ClienteScript.vars.forms.cadastrar).valid())
                return;

            var $btn = $(ClienteScript.vars.forms.cadastrar + ' .btn-success').button('loading');

            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url: VariaveisSistema.BaseUrl + 'cliente/novo',
                    dados: $(ClienteScript.vars.forms.cadastrar).jsonify(),
                    chamadaRetorno: {
                        sucesso: function(dados){
                            //TALVEZ ELIMINAR FUNCIONALIDADES acaoDepois*
                            $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU_E_LOGOU, dados.usuario);
                        },
                        falha: function(dados){
                            if(dados.msg != undefined)
                               alertify.log(dados.msg);
                            else
                               alertify.error('Ocorreu uma falha ao registrar seus dados.');
                        },
                        erro: function(){
                            alertify.error('Ocorreu um erro ao registrar seus dados.');
                        },
                        completo: function(){
                            $btn.button('reset');
                        }
                    }
                })
            );
        },
        salvarDados: function(){
            if( ! $(ClienteScript.vars.forms.dados).valid())
                return;

            var $btn = $('.btn-salvar').button('loading');

            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url: VariaveisSistema.BaseUrl + 'cliente/atualizar',
                    dados: $(ClienteScript.vars.forms.dados).jsonify(),
                    chamadaRetorno: {
                        sucesso: function(dado){
                            alertify.success('Seus dados foram atualizados!');
                            $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_ATUALIZOU_DADOS_DO_CADASTRO);
                        },
                        falha: function(dado){
                            alertify.error('Por favor certifique-se de ter editado pelo menos um dos campo.');
                        },
                        erro: function(){
                            alertify.error('Ocorreu um erro ao atualizar seus dados.');
                        },
                        completo: function(){
                            $btn.button('reset');
                        }
                    }
                })
            );
        },
        enviarContatoNormal: function() {
            if( ! $(ClienteScript.vars.contato.normal.form).valid())
                return;

            var $btn = $(ClienteScript.vars.contato.normal.form + ' .btn-enviar').button('loading');

            var contato = $(ClienteScript.vars.contato.normal.form).jsonify();

            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url:    VariaveisSistema.BaseUrl + 'contato/novo',
                    dados:  contato,
                    chamadaRetorno: {
                        sucesso: function(dados){// successCallback
                            if(dados.usuario_logado_dados != undefined) {
                                if(dados.usuario_logado == 1)
                                    $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_LOGOU, dados.usuario_logado_dados);
                                else if(dados.usuario_logado == 2)
                                    $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU_E_LOGOU, dados.usuario_logado_dados);
                            }

                            alertify.success($.simplesImob.servicos.usuario.mensagens.REQUISITAR_CONTATO_NORMAL_ENVIADO_SUCESSO);
                        },
                        falha: function(dados){// failureCallback
                            if(dados.msg != undefined)
                                alertify.log(dados.msg);
                            else
                                alertify.error($.simplesImob.servicos.usuario.mensagens.REQUISITAR_CONTATO_NORMAL_ENVIADO_FALHA);
                        },
                        erro: function(){// errorCallback
                            alertify.error($.simplesImob.servicos.usuario.mensagens.REQUISITAR_CONTATO_NORMAL_ENVIADO_ERRO);
                        },
                        completo: function(){// completeCallback
                            $btn.button('reset');
                        }
                    }
                })
            );
        },
        enviarContatoInteresse: function() {
            if( ! $(ClienteScript.vars.contato.interesse.form).valid())
                return;

            var $btn = $(ClienteScript.vars.contato.interesse.btnEnviar).button('loading');
            var dados = $(ClienteScript.vars.contato.interesse.form).jsonify();

            var urlDestino = 'contato/interesse_imovel';

            if(dados.cod_condominio != undefined)
            {
                dados.assunto = 'Interesse em empreendimento';
                dados.mensagem = dados.obs + '<br><br>Link do empreendimento: ' + document.location.href;
                urlDestino = 'contato/novo';
            }

            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url:   VariaveisSistema.BaseUrl + urlDestino,
                    dados: dados,
                    chamadaRetorno: {
                        sucesso: function(dados){
                            if(dados.usuario_logado_dados != undefined) {
                                if(dados.usuario_logado == 1)
                                    $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_LOGOU, dados.usuario_logado_dados);
                                else if(dados.usuario_logado == 2)
                                    $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU_E_LOGOU, dados.usuario_logado_dados);
                            }

                            alertify.success($.simplesImob.servicos.usuario.mensagens.REQUISITAR_CONTATO_INTERESSE_ENVIADO_SUCESSO);
                            $(ClienteScript.vars.contato.interesse.modal).modal('hide');

                            $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_ENVIOU_CONTATO_INTERESSE_EM_IMOVEL, VariaveisSistema.Imovel);
                        },
                        falha: function(data){
                            alertify.error(data.msg || $.simplesImob.servicos.usuario.mensagens.REQUISITAR_CONTATO_INTERESSE_ENVIADO_FALHA);
                        },
                        erro: function(){
                            alertify.error($.simplesImob.servicos.usuario.mensagens.REQUISITAR_CONTATO_INTERESSE_ENVIADO_ERRO);
                        },
                        completo: function(){
                            $btn.button('reset');
                        }
                    }
                })
            );
        }
    }
};

$(document).ready(function(){
    ClienteScript.funcs.documentReady();
});