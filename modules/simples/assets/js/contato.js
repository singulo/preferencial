on_cliente_entrar.successCallback.push(function(usuario){
    usuario.telefone = usuario.telefone_1;

    $('#form-contato').dejsonify(usuario);
});

$(document).ready(function(){

    //USUARIO LOGADO
    if(JSON.parse($('#usuario').val()) != null)
    {
        $('.btn-interesse').bind('click', abrir_modal_contato_interesse);

        var usuario = JSON.parse($('#usuario').val());
        usuario.telefone = usuario.telefone_1;

        $('#form-contato').dejsonify(usuario);
    }
    else
        $('.btn-interesse').bind('click', informa_que_precisa_estar_logado);
});

function cliente_enviar_novo_contato()
{
    if(!$("#form-contato").valid()) return;

    var $btn = $('.btn-enviar').button('loading');

    var contato = $("#form-contato").jsonify();

    cliente_contato_novo(
        contato,
        {
            successCallback: function(data){//successCallback
                alertify.success('Contato enviado!');
            },
            failureCallback: function(data){//failureCallback
                if(data.msg != undefined)
                    alertify.error(data.msg);
                else
                    alertify.error('Ocorreu um erro ao enviar seu contato. Por favor tente mais tarde!');
            },
            errorCallback: function(){//errorCallback
                alertify.error('Ocorreu um erro ao enviar seu contato.');
            },
            completeCallback: function(){//completeCallback
                $btn.button('reset');
            }
        });
}

on_cliente_entrar.successCallback.push(function(usuario){
    $('.btn-interesse').unbind('click', informa_que_precisa_estar_logado);
    $('.btn-interesse').bind('click', abrir_modal_contato_interesse);
});

function abrir_modal_contato_interesse()
{
    $("#modal-contato-interesse").modal('show');
}

function cliente_enviar_contato_interesse()
{
    var $btn = $('.btn-enviar').button('loading');

    var contato = $('#form-interesse').jsonify();

    cliente_interesse_em_imovel(
        contato.cod_imovel,
        contato.obs,
        {
            successCallback: function(data){//successCallback
                alertify.success('Contato enviado!');
                $('#modal-contato-interesse').modal('hide');
            },
            failureCallback: function(data){//failureCallback
                if(data.msg != undefined)
                    alertify.error(data.msg);
                else
                    alertify.error('Ocorreu um erro ao enviar seu contato. Por favor tente mais tarde!');
            },
            errorCallback: function(){//errorCallback
                alertify.error('Ocorreu um erro ao enviar seu contato.');
            },
            completeCallback: function(){//completeCallback
                $btn.button('reset');
            }
        }
    );
}

function informa_que_precisa_estar_logado()
{
    alertify
        .okBtn("Ok")
        .alert('<h6>Você precisa logar antes dessa ação!</h6><br>Por favor realize o login e clique novamente aqui.');
}