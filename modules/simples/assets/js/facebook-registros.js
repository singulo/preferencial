$.simplesImobFacebook = {
    vars: {
        eventos: {
            descricoes: {
                CLIENTE_CADASTRADO:             'Cliente cadastrado.',
                CLIENTE_INTERESSADO_EM_IMOVEL:  'Cliente interessado em imóvel.',
                CLIENTE_OLHANDO_IMOVEL:         'Cliente olhando imóvel.',
            }
        }
    },
    eventos: {
        usuario: {
            cadastrado: function(e, usuario, metodoDeCadastro){
                var params = {};
                params[FB.AppEvents.ParameterNames.REGISTRATION_METHOD] = metodoDeCadastro;
                params[FB.AppEvents.ParameterNames.DESCRIPTION]         = $.simplesImobFacebook.vars.eventos.descricoes.CLIENTE_CADASTRADO;
                FB.AppEvents.logEvent(
                    FB.AppEvents.EventNames.COMPLETED_REGISTRATION,
                    null,
                    params
                );
            },
            visualizando: {
                imovel: function(e, imovel){
                    console.log('FB evento: ' + $.simplesImobFacebook.vars.eventos.descricoes.CLIENTE_OLHANDO_IMOVEL);
                    var params = {};
                    params[FB.AppEvents.ParameterNames.CONTENT_ID]      = $.simplesImobFacebook.params.CONTENT_TYPE.imovel + ':' + imovel.id;
                    params[FB.AppEvents.ParameterNames.CONTENT_TYPE]    = $.simplesImobFacebook.params.CONTENT_TYPE.imovelComFinalidade();
                    params[FB.AppEvents.ParameterNames.CURRENCY]        = $.simplesImobFacebook.params.CURRENCY.peloImovelEmScript(imovel);
                    params[FB.AppEvents.ParameterNames.DESCRIPTION]     = $.simplesImobFacebook.vars.eventos.descricoes.CLIENTE_OLHANDO_IMOVEL;
                    FB.AppEvents.logEvent(
                        FB.AppEvents.EventNames.VIEWED_CONTENT,
                        null,  // numeric value for this event - in this case, none
                        params
                    );
                }
            },
            contato: {
                normal: function(){

                },
                interesse: function(e, imovel){
                    console.log('FB evento: ' + $.simplesImobFacebook.vars.eventos.descricoes.CLIENTE_INTERESSADO_EM_IMOVEL);
                    var params = {};
                    params[FB.AppEvents.ParameterNames.CONTENT_ID]      = $.simplesImobFacebook.params.CONTENT_TYPE.imovel + ':' + imovel.id;
                    params[FB.AppEvents.ParameterNames.CONTENT_TYPE]    = $.simplesImobFacebook.params.CONTENT_TYPE.imovelComFinalidade();
                    params[FB.AppEvents.ParameterNames.CURRENCY]        = $.simplesImobFacebook.params.CURRENCY.peloImovelEmScript(imovel);
                    params[FB.AppEvents.ParameterNames.DESCRIPTION]     = $.simplesImobFacebook.vars.eventos.descricoes.CLIENTE_INTERESSADO_EM_IMOVEL;
                    FB.AppEvents.logEvent(
                        FB.AppEvents.EventNames.ADDED_TO_WISHLIST,
                        null,
                        params
                    );
                }
            }
        },
    },
    params: {
        CONTENT_TYPE: {
            imovel: 'imovel',
            condominio: 'condominio',
            imovelComFinalidade: function(finalidade){
                if(finalidade == undefined)
                {
                    var finalidadeClicada = $.simplesImob.servicos.ajudante.url.obterFinalidadeClicada();
                    if(finalidadeClicada > 0)
                        finalidade = finalidadeClicada;
                }

                if(VariaveisSistema.Finalidades[finalidade] != undefined)
                    return $.simplesImobFacebook.params.CONTENT_TYPE.imovel + VariaveisSistema.Finalidades[finalidade];
                else
                    return $.simplesImobFacebook.params.CONTENT_TYPE.imovel;
            }
        },
        CURRENCY: {
            peloImovelEmScript: function(imovel){
                var valor = '';
                if(imovel != undefined)
                {
                    var finalidade = $.simplesImob.servicos.ajudante.url.obterFinalidadeClicada();

                    if(finalidade == 0)
                    {
                        if(imovel.finalidade_venda == 1
                            && imovel.finalidade_aluguel == 0
                            && imovel.finalidade_temporada == 0)
                            finalidade = 1;
                        if(imovel.finalidade_venda == 0
                            && imovel.finalidade_aluguel == 1
                            && imovel.finalidade_temporada == 0)
                            finalidade = 2;
                        if(imovel.finalidade_venda == 0
                            && imovel.finalidade_aluguel == 0
                            && imovel.finalidade_temporada == 1)
                            finalidade = 3;
                    }

                    if(finalidade > 0)
                    {
                        switch (finalidade)
                        {
                            case 1:
                                valor = imovel.valor;
                                break;
                            case 2:
                                valor = imovel.valor_aluguel_mensal;
                                break;
                            case 3:
                                valor = imovel.valor_aluguel_diario;
                                break;
                        }

                        if(isNaN(valor))
                            valor = '';
                    }
                }

                return valor;
            }
        }
    },
    login: function(){
        $('.btn.btn-login-facebook').data('loading-text', "<i class='fa fa-facebook'></i><span class='text-center'>Aguardando sua permissão...</span>").button('loading'); // INFORMA AÇÃO DO FB NO BOTÃO
        FB.login(function(response) {
            if (response.authResponse) {
                $('.btn.btn-login-facebook').button('reset'); // RESETA BOTÃO
                $.simplesImobFacebook.getLoginStatus();
            } else {
                $('.btn.btn-login-facebook').button('reset'); // RESETA BOTÃO
            }
        }, { scope: 'public_profile,email' });
    },
    getLoginStatus: function() {
        $('.btn.btn-login-facebook').data('loading-text', "<i class='fa fa-facebook'></i><span class='text-center'>Aguarde...</span>").button('loading'); // INFORMA AÇÃO DO FB NO BOTÃO
        FB.getLoginStatus(function(response) {
            if(response.status === 'connected'){
                FB.api('/me?fields=id,name,email,picture', function(response){
                    if(response.email == undefined)
                    {
                        $('.btn.btn-login-facebook').button('reset'); // RESETA BOTÃO
                        alertify.alert('<small>Você não permitiu que o <b>Facebook</b> divulga-se seu <b>e-mail</b>.</small><br><br>Por favor entre com sua conta <b>Gmail</b> ou <b>cadastre-se</b>.');
                    }
                    else
                    {
                        var dados = {
                            nome: response.name,
                            id_facebook: response.id,
                            foto: undefined,
                            metodo_cadastro: $.simplesImob.servicos.usuario.vars.metodosDeCadastro.tipos.Facebook
                        };


                        if((response.id !== undefined) && (response.picture.data.is_silhouette === false))
                        {
                            FB.api(
                                '/'+ response.id +'/picture?height=200&width=200&redirect=false',
                                'GET',
                                {},
                                function(resposta_foto) {
                                    dados.foto = String(resposta_foto.data.url);

                                    $.simplesImobFacebook.enviarDadosClienteFacebook(response.email, dados);
                                }
                            );
                        }
                        else
                            $.simplesImobFacebook.enviarDadosClienteFacebook(response.email, dados);
                    }
                });
            } else if(response.status === 'not_authorized'){
                FB.login();
                $('.btn.btn-login-facebook').button('reset'); // RESETA BOTÃO
            }
        });
    },
    enviarDadosClienteFacebook: function (email_principal, dados) {

        $.simplesImob.servicos.usuario.requisitar.tentarLogarEmailSeNecessarioCadastrar(
            email_principal,dados,
            $.simplesImob.requisicao.chamadasRetorno({
                completo: function(){
                    $('.btn.btn-login-facebook').button('reset'); // RESETA BOTÃO
                }
            })
        );
    }
};

$.simplesImob.ambiente.on($.simplesImob.ambienteEventosNome.FB_API_INICIADA, function(){
    FB.AppEvents.logPageView();

    if($.simplesImob.servicos.ajudante.ambiente.visualiando.imovel())
        $.simplesImobFacebook.eventos.usuario.visualizando.imovel(null, VariaveisSistema.Imovel);

    $.simplesImob.ambiente.on(
        $.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU,
        $.simplesImobFacebook.eventos.usuario.cadastrado
    );
    $.simplesImob.ambiente.on(
        $.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU_E_LOGOU,
        $.simplesImobFacebook.eventos.usuario.cadastrado
    );
    $.simplesImob.ambiente.on(
        $.simplesImob.ambienteEventosNome.FB_CLIENTE_CLICK_BOTAO_LOGIN,
        $.simplesImobFacebook.login
    );
});