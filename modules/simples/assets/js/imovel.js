function obter_resultado_pesquisa(filtro, simpleRequest)
{
    ajaxPost(
        filtro,
        $('#base_url').val() + 'imovel/buscar',
        simpleRequest
    );
}

function finalidade_util(finalidade)
{
    var fin = {class : '', descricao : ''};

    switch(finalidade)
    {
        case '1':
            fin.class 	= 'sale';
            fin.descricao = 'À venda';
            break;
        case '2':
            fin.class 	= 'rent';
            fin.descricao = 'Aluguel';
            break;
    }

    return fin;
}

function registrar_visualizacao_imovel(cod)
{
    $.ajax({

        url : $('#base_url').val() + 'imovel/registrar_visualizacao_imovel',
        type : 'POST',
        data : {cod_imovel : cod},
        dataType: "json",
        success: function (data) {
            if(data.status)
                console.log('Registrada visualização de imóvel com sucesso');
            else
                console.log('Falha ao registrar visualização de imóvel');
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log('Falha ao registrar visualização de imóvel');
        }
    });
}

function exibir_valor_imovel(imovel, cifrao)
{
    switch(finalidades[imovel.finalidade])
    {
        case 'Venda' :
            return formata_valor_imovel(imovel.valor, cifrao);
            break;
        case 'Aluguel' :
            return formata_valor_imovel(imovel.valor_aluguel_mensal, cifrao);
            break;
        case 'Temporada' :
            return formata_valor_imovel(imovel.valor_aluguel_diario, cifrao);
            break;
    }
}

function formata_valor_imovel(valor_imovel, cifrao)
{
    if(valor_imovel == 'Consulte')
        return valor_imovel;
    else
    {
        var e = $('<div></div>');
        e.autoNumeric('init', {aSep: '.', aDec: ',', vMax: 999999999.99});

        var valor = e.autoNumeric('set', valor_imovel);
        return cifrao + valor.text();
    }
}