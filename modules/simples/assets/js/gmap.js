var GmapScript = {
    vars: {
        descentralizar: {
            valor: 100
        },
        mapas: {
            //marcador: null,
            //mapa: null,
        },
    },
    events: {
        documentReady: function(){
            if($('[data-ao-carregar-pagina-chamar]').length)
                eval($('[data-ao-carregar-pagina-chamar]').data('ao-carregar-pagina-chamar').replace(/(\\)/g, ''));
        }
    },
    funcs: {
        iniciar: function(elemId){
            var elementoMapa = document.getElementById(elemId);
            if(elementoMapa == null)
                return;

            GmapScript.vars.mapas[elemId] = {
                elemento: null,
                gmap: {
                    mapa: null,
                    opcoes: {},
                    marcador: null,
                    circulo: null,
                },
                configs: {
                    latLng: {},
                    zoom: 16,
                    circulo: {
                        raio: 300,
                        larguraBorda: 1,
                        bordaCor: '#FF0000',
                        cor: '#e55050'
                    }
                },
            };
            GmapScript.vars.mapas[elemId].elemento = elementoMapa;

            GmapScript.vars.mapas[elemId].configs.latLng.valor = $(GmapScript.vars.mapas[elemId].elemento).data('lat-lng').replace(' ', '').split(',');
            if($(GmapScript.vars.mapas[elemId].elemento).data('embaralhar-lat-lng'))
                GmapScript.vars.mapas[elemId].configs.latLng.valor = GmapScript.funcs.descentralizarLatLng(GmapScript.vars.mapas[elemId].configs.latLng.valor[0], GmapScript.vars.mapas[elemId].configs.latLng.valor[1]);

            GmapScript.vars.mapas[elemId].configs.latLng.formatoGmap = new google.maps.LatLng(GmapScript.vars.mapas[elemId].configs.latLng.valor[0], GmapScript.vars.mapas[elemId].configs.latLng.valor[1]);

            GmapScript.vars.mapas[elemId].configs.latLng.centro = {};
            if($(GmapScript.vars.mapas[elemId].elemento).data('center'))
            {
                GmapScript.vars.mapas[elemId].configs.latLng.centro.valor = $(GmapScript.vars.mapas[elemId].elemento).data('center').replace(' ', '').split(',');
                GmapScript.vars.mapas[elemId].configs.latLng.centro.formatoGmap = new google.maps.LatLng(GmapScript.vars.mapas[elemId].configs.latLng.centro.valor[0], GmapScript.vars.mapas[elemId].configs.latLng.centro.valor[1]);
            }
            else
            {
                GmapScript.vars.mapas[elemId].configs.latLng.centro.valor = GmapScript.vars.mapas[elemId].configs.latLng.valor;
                GmapScript.vars.mapas[elemId].configs.latLng.centro.formatoGmap = GmapScript.vars.mapas[elemId].configs.latLng.formatoGmap;
            }

            if($(GmapScript.vars.mapas[elemId].elemento).data('zoom'))
                GmapScript.vars.mapas[elemId].configs.zoom = $(GmapScript.vars.mapas[elemId].elemento).data('zoom');
            if($(GmapScript.vars.mapas[elemId].elemento).data('raio-proximidade-tamanho'))
                GmapScript.vars.mapas[elemId].configs.circulo.raio = $(GmapScript.vars.mapas[elemId].elemento).data('raio-proximidade-tamanho');
            if($(GmapScript.vars.mapas[elemId].elemento).data('raio-proximidade-cor'))
                GmapScript.vars.mapas[elemId].configs.circulo.cor = $(GmapScript.vars.mapas[elemId].elemento).data('raio-proximidade-cor');

            GmapScript.vars.mapas[elemId].gmap.opcoes = {
                zoom: GmapScript.vars.mapas[elemId].configs.zoom,
                center: GmapScript.vars.mapas[elemId].configs.latLng.centro.formatoGmap,
                scrollwheel: false
            };
            GmapScript.vars.mapas[elemId].gmap.mapa = new google.maps.Map(GmapScript.vars.mapas[elemId].elemento, GmapScript.vars.mapas[elemId].gmap.opcoes);

            if( ! $(GmapScript.vars.mapas[elemId].elemento).data('nenhum-marcador'))
            {
                if($(GmapScript.vars.mapas[elemId].elemento).data('raio-proximidade-mostrar'))
                {
                    GmapScript.vars.mapas[elemId].gmap.circulo = new google.maps.Circle({
                        map: GmapScript.vars.mapas[elemId].gmap.mapa,
                        radius: GmapScript.vars.mapas[elemId].configs.circulo.raio,
                        fillColor: GmapScript.vars.mapas[elemId].configs.circulo.cor,
                        strokeWeight: GmapScript.vars.mapas[elemId].configs.circulo.larguraBorda,
                        strokeColor: GmapScript.vars.mapas[elemId].configs.circulo.bordaCor,
                        center: GmapScript.vars.mapas[elemId].configs.latLng.centro.formatoGmap,
                        srcElement: elemId
                    });
                }
                else
                {
                    GmapScript.vars.mapas[elemId].gmap.icone = VariaveisSistema.ImgMapaMarcador != undefined ? VariaveisSistema.ImgMapaMarcador : null;
                    GmapScript.vars.mapas[elemId].gmap.marcador = new google.maps.Marker({
                        position: GmapScript.vars.mapas[elemId].configs.latLng.formatoGmap,
                        map: GmapScript.vars.mapas[elemId].gmap.mapa,
                        icon: GmapScript.vars.mapas[elemId].gmap.icone,
                        animation: google.maps.Animation.DROP,
                        draggable: false,
                        srcElement: elemId
                    });
                    google.maps.event.addListener(GmapScript.vars.mapas[elemId].gmap.marcador, 'click', GmapScript.funcs.aoClicarNoMarcador);
                }
            }
        },
        aoClicarNoMarcador: function(){
            if (GmapScript.vars.mapas[this.srcElement].gmap.marcador.getAnimation() != null) {
                GmapScript.vars.mapas[this.srcElement].gmap.marcador.setAnimation(null);
            } else {
                GmapScript.vars.mapas[this.srcElement].gmap.marcador.setAnimation(google.maps.Animation.BOUNCE);
            }
        },
        descentralizarLatLng: function(latitude, longitude){
            var embaralhadorLat = '0.001' + Math.floor((Math.random() * GmapScript.vars.descentralizar.valor) + 10);
            var embaralhadorLng = '0.001' + Math.floor((Math.random() * GmapScript.vars.descentralizar.valor) + 1);
            embaralhadorLat = Math.floor((Math.random() * 100) + 1) % 2 === 0 ? Math.abs(embaralhadorLat) : -Math.abs(embaralhadorLat);
            embaralhadorLng = Math.floor((Math.random() * 100) + 1) % 2 === 0 ? Math.abs(embaralhadorLng) : -Math.abs(embaralhadorLng);

            console.log(latitude, longitude);
            console.log(embaralhadorLat, embaralhadorLng);
            console.log(latitude - embaralhadorLat, longitude - embaralhadorLng);

            return [
                latitude - embaralhadorLat,
                longitude - embaralhadorLng,
            ];
        }
    }
};

$(document).ready(function(){
    GmapScript.events.documentReady();
});