var FiltroSliderScript = {
    vars: {
        filtros: {
            sliders: {
                valores: {},
                metragens: {}
            },
            forms: {}
        },
        valor: {
            finalidade: {
                1: {
                    min: 0,
                    max: 3000000,
                    intervalo: 50000
                },
                2: {
                    min: 0,
                    max: 2500,
                    intervalo: 250
                },
                3: {
                    min: 0,
                    max: 2500,
                    intervalo: 200
                },
            },
            formatarValorSlider: function(valores){
                var form = FiltroSliderScript.vars.filtros.forms[this.id];

                if(valores[0] != undefined)
                    FiltroSliderScript.funcs.atualizaLabelNoSliderValorImovelEFormata(form, valores[0], valores[1]);

                var valorMaxFinalidade = FiltroSliderScript.funcs._obterValorMaxPelaFinalidadeDoForm(form);

                if(valores[1] == valorMaxFinalidade)
                    return SimplesScript.funcs.formatarValor(valores[0], '', '0,00') + ' : ' + SimplesScript.funcs.formatarValor(valores[1], '', '0,00') + '+';
                else
                    return SimplesScript.funcs.formatarValor(valores[0] == undefined ? 0 : valores[0], '', '0,00') + ' : ' + SimplesScript.funcs.formatarValor(valores[1] == undefined ? 0 : valores[1], '', '0,00');
            }
        },
        metragem: {
            min: 0,
            max: 300,
            formatarValorSlider: function(valores){
                var form = FiltroSliderScript.vars.filtros.forms[this.id];

                if(valores[0] != undefined)
                    FiltroSliderScript.funcs.atualizaLabelNoSliderEFormatted(form, valores[0], valores[1]);

                if(valores[1] == FiltroSliderScript.vars.metragem.max)
                    return SimplesScript.funcs.formatarValor(valores[0], '', '0,00') + ' : ' + SimplesScript.funcs.formatarValor(valores[1], '', '0,00') + '+';
                else
                    return SimplesScript.funcs.formatarValor(valores[0] == undefined ? 0 : valores[0], '', '0,00') + ' : ' + SimplesScript.funcs.formatarValor(valores[1] == undefined ? 0 : valores[1], '', '0,00');
            }
        },
    },
    events: {
        atualizarSliderPelaFinalidade: function(elem){
            var formFinalidadeModificada = $(elem).closest('form')[0];
            $.each(FiltroSliderScript.vars.filtros.forms, function(index, form){
                if(formFinalidadeModificada == form[0]){
                    var config = FiltroSliderScript.funcs._obterConfigValoresPelaFinalidadeDoForm(form);

                    FiltroSliderScript.vars.filtros.sliders.valores[index].bootstrapSlider('setAttribute', 'min', config.min);
                    FiltroSliderScript.vars.filtros.sliders.valores[index].bootstrapSlider('setAttribute', 'max', config.max);
                    FiltroSliderScript.vars.filtros.sliders.valores[index].bootstrapSlider('setAttribute', 'step', config.intervalo);
                    FiltroSliderScript.vars.filtros.sliders.valores[index].bootstrapSlider('setValue', [config.min, config.max]);

                    return false;
                }
            });
        }
    },
    funcs: {
        documentReady: function(){
            if(VariaveisSistema.Filtro != undefined && VariaveisSistema.Filtro.config != undefined)
            {
                $.each(VariaveisSistema.Filtro.config.valores, function(index, config){
                    FiltroSliderScript.vars.valor.finalidade[index].min = config.min;
                    FiltroSliderScript.vars.valor.finalidade[index].max = config.max;
                    FiltroSliderScript.vars.valor.finalidade[index].intervalo = config.intervalo;
                });
            }

            $.each($('form.form-filtro'), function(index, form){
                if($(form).find('.slider-filtro').length > 0 || $(form).find('.slider-metragem').length > 0){
                    FiltroSliderScript.vars.filtros.forms[index] = $(form);
                    var config = FiltroSliderScript.funcs._obterConfigValoresPelaFinalidadeDoForm(form);
                    // VALORES
                    var sliderFiltroValores = $(form).find('.slider-filtro');
                    if(sliderFiltroValores.length){
                        FiltroSliderScript.vars.filtros.sliders.valores[index] = sliderFiltroValores.bootstrapSlider({
                            id: index,
                            formatter: FiltroSliderScript.vars.valor.formatarValorSlider,
                            step: config.intervalo,
                            max: config.max,
                            min: config.min
                        });
                    }
                    //METRAGEM
                    var sliderFiltroMetragens = $(form).find('.slider-metragem');
                    if(sliderFiltroMetragens.length) {
                        FiltroSliderScript.vars.filtros.sliders.metragens[index] = $(form).find('.slider-metragem').bootstrapSlider({
                            id: index,
                            formatter: FiltroSliderScript.vars.metragem.formatarValorSlider
                        });
                    }
                }
            });
        },
        formataMetragemImovel: function(tamanho, cifrao) {
            return tamanho + cifrao;
        },
        atualizaLabelNoSliderEFormatted: function(form, valMin, valMax) {
            $(form).find('input[name="metragem_min"]').val(valMin);
            $(form).find('input[name="metragem_max"]').val(valMax);

            var mostrarMais = valMax == FiltroSliderScript.vars.metragem.max ? '+' : '';

            $(form).find('.filtro-metragem').html('<span class="pull-left valor-minimo">' + FiltroSliderScript.funcs.formataMetragemImovel(valMin, '<small>m²</small>') + '</span><span class="pull-right valor-maximo">' + FiltroSliderScript.funcs.formataMetragemImovel(valMax, '<small>m²</small>') + mostrarMais + '</span>');
        },
        atualizaLabelNoSliderValorImovelEFormata: function(form, valMin, valMax) {
            $(form).find('input[name="preco_min"]').val(valMin);
            $(form).find('input[name="preco_max"]').val(valMax);

            var valorMaxFinalidade = FiltroSliderScript.funcs._obterValorMaxPelaFinalidadeDoForm(form);
            var mostrarMais = valMax == valorMaxFinalidade ? '+' : '';

            $(form).find('.filtro-valores').html('<span class="pull-left valor-minimo">' + SimplesScript.funcs.formatarValor(valMin, '<small>R$</small>') + '</span><span class="pull-right valor-maximo">' + SimplesScript.funcs.formatarValor(valMax, '<small>R$</small>') + mostrarMais + '</span>');
        },
        _obterConfigValoresPelaFinalidadeDoForm: function(form){
            var config = FiltroSliderScript.vars.valor.finalidade[1];
            if($(form).find('select[name=finalidade]').val() > 0)
                config = FiltroSliderScript.vars.valor.finalidade[$(form).find('select[name=finalidade]').val()];

            return config;
        },
        _obterValorMaxPelaFinalidadeDoForm: function(form){
            return FiltroSliderScript.funcs._obterConfigValoresPelaFinalidadeDoForm(form).max;
        },
    },
};

$(document).ready(function(){
    FiltroSliderScript.funcs.documentReady();
});