var FiltroSliderMobileScript = {
    vars:{
        slider: null,
        valor: {
            finalidade: {
                1: {
                    min: 0,
                    max: 3000000,
                    intervalo: 50000
                },
                2: {
                    min: 0,
                    max: 2500,
                    intervalo: 250
                },
                3: {
                    min: 0,
                    max: 2500,
                    intervalo: 200
                },
            }
        }
    },
    events: {
        atualizarSliderPelaFinalidade: function(elem){
            var finalidade = $(elem).val();
            var opcoes = {
                start: [ FiltroSliderMobileScript.vars.valor.finalidade[finalidade].min, FiltroSliderMobileScript.vars.valor.finalidade[finalidade].max ],
                range: {
                    min: [FiltroSliderMobileScript.vars.valor.finalidade[finalidade].min],
                    max: [FiltroSliderMobileScript.vars.valor.finalidade[finalidade].max]
                },
                step: FiltroSliderMobileScript.vars.valor.finalidade[finalidade].intervalo,
                margin: FiltroSliderMobileScript.vars.valor.finalidade[finalidade].intervalo * 2,
            };

            FiltroSliderMobileScript.vars.slider.noUiSlider.updateOptions(
                opcoes, // Object
                true // Boolean 'fireSetEvent'
            );
        }
    },
    funcs:{
        documentReady: function(){
            if( ! $.simplesImob.servicos.ajudante.acessoMobile())
                return;

            if(VariaveisSistema.Filtro != undefined && VariaveisSistema.Filtro.config != undefined)
            {
                $.each(VariaveisSistema.Filtro.config.valores, function(index, config){
                    FiltroSliderMobileScript.vars.valor.finalidade[index].min = config.min;
                    FiltroSliderMobileScript.vars.valor.finalidade[index].max = config.max;
                    FiltroSliderMobileScript.vars.valor.finalidade[index].intervalo = config.intervalo;
                });
            }

            FiltroSliderMobileScript.vars.slider = document.querySelector('#filtro-form-imovel-mobile div.slider-filtro-mobile');
            var finalidadeValoresConfig = FiltroSliderMobileScript.funcs._obterConfigValoresPelaFinalidadeDoForm();
            noUiSlider.create(FiltroSliderMobileScript.vars.slider, {
                start: [Number.parseInt(VariaveisSistema.Filtro.preco_min), Number.parseInt(VariaveisSistema.Filtro.preco_max)],
                range: {
                    min: [finalidadeValoresConfig.min],
                    max: [finalidadeValoresConfig.max],
                },
                step: finalidadeValoresConfig.intervalo,
                margin: finalidadeValoresConfig.intervalo * 2,
            });
            FiltroSliderMobileScript.vars.slider.noUiSlider.on('update', function(values, handle){
                FiltroSliderMobileScript.funcs.formatarHtmlValores(values);
                FiltroSliderMobileScript.funcs.setaValoresInputs(values);
            });
        },
        _obterConfigValoresPelaFinalidadeDoForm: function(){
            var config = FiltroSliderScript.vars.valor.finalidade[1];
            if($('#filtro-form-imovel-mobile select[name=finalidade]').val() > 0)
                config = FiltroSliderScript.vars.valor.finalidade[$('#filtro-form-imovel-mobile select[name=finalidade]').val()];

            return config;
        },
        setaValoresInputs: function(valores){
            $('#filtro-form-imovel-mobile input[name=preco_min]').val(Number.parseInt(valores[0]));
            $('#filtro-form-imovel-mobile input[name=preco_max]').val(Number.parseInt(valores[1]));
        },
        formatarHtmlValores: function(valores){
            $('#filtro-form-imovel-mobile .slider-valores .filtro-valores-mobile').html('<span class="pull-left"><small>R$</small>' + SimplesScript.funcs.formatarValor(valores[0]) + '</span><span class="pull-right"><small>R$</small>' + SimplesScript.funcs.formatarValor(valores[1]) + '</span>');
        },
    },
};

$(document).ready(function(){
    FiltroSliderMobileScript.funcs.documentReady();
});