var CondominioFiltroScript = {
    vars: {
        filtro: null,
        html: {
            filtro: '#filtro-condominios',
            btnPesquisar: '.filtro .btn-pesquisar'
        }
    },
    funcs: {
        documentoPronto: function() {
            CondominioFiltroScript.vars.filtro = new TemplateFiltroClass(
                CondominioFiltroScript.vars.html.filtro,
                '/condominio/filtro',
                {
                    limiteResultados:       18,
                    multiplo:               false,
                    html: {
                        resultadoItemClass: 'condominio',
                        btnPesquisar: CondominioFiltroScript.vars.html.btnPesquisar
                    },
                    objeto: {
                        nome:               'condominio'
                    },
                    busca: {
                        objCampo:           'condominios'
                    }
                }
            );

            CondominioFiltroScript.vars.filtro.itensParaHTML = function(_this, condominios){
                var html = '';
                $.each(condominios, function(index, condominio){
                    var imagemUrl = (condominio.id_empresa_agenciadora ? '' : VariaveisSistema.FilialFotosCondominios) + condominio.foto_destaque;

                    html += '<div class="' + _this.config.html.resultadoItemClass + ' wow slideInUp"><a href="#"><div class="imagem" style="background-image: url(' + imagemUrl + ')"></div><h3 class="nome">' + condominio.nome + '</h3></a></div>';
                });

                return html;
            };


            CondominioFiltroScript.vars.filtro.antesDeItensParaHtml = function(_this) {
                $(_this.config.html.resultadosItens).scrollTop(0);
            };

            CondominioFiltroScript.vars.filtro.depoisDeItensParaHtml = function(_this) {
                var wow = new WOW(
                    {
                        boxClass:     'wow',      // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset:       0,          // distance to the element when triggering the animation (default is 0)
                        mobile:       true,       // trigger animations on mobile devices (default is true)
                        live:         true,       // act on asynchronously loaded content (default is true)
                        callback:     function(box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: '.condominios-filtro .resultados .itens',    // optional scroll container selector, otherwise use window,
                        resetAnimation: false,     // reset animation on end (default is true)
                    }
                );

                wow.init();
            };

            $(CondominioFiltroScript.vars.html.btnPesquisar).on('click', function(){
                CondominioFiltroScript.vars.filtro.buscar(1);
            });
        }
    }
};

$(document).ready(function(){
    CondominioFiltroScript.funcs.documentoPronto();
});