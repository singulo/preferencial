function TemplateFiltroClass(form, urlBusca, config) {

    this.modos = {
        link: 'link',
        selecao: 'selecao',
    };

    this.form                               = form;
    this.urlBusca                           = urlBusca;

    this.config                             = { };
    this.config.multiplo                    = false;
    this.config.limiteResultados            = 16;
    this.config.ultimoTotalObtido           = 0;
    this.config.atualizarUrl                = true;
    this.config.parametroUrl                = 'ultimo-filtro';
    this.config.aoIniciarSeTiverParamBuscar = true;
    this.config.modo                        = this.modos.link; // link | selecao

    this.config.html                        = {};
    this.config.html.container              = 'body';
    this.config.html.resultadosContainer    = '.resultados';
    this.config.html.resultadosItens        = '.resultados .itens';
    this.config.html.resultadoItemClass     = '.item';
    this.config.html.nadaEncontrado         = '.resultados .nada-encontrado';
    this.config.html.paginacao              = '.rodape .filtro-paginacao';
    this.config.html.btnVerSelecionados     = '.rodape .ver-selecionados';
    this.config.html.btnVerResultadosBusca  = '.rodape .ver-resultados-pesquisa';
    this.config.html.btnFinalizar           = '.rodape .btn-finalizar';
    this.config.html.btnPesquisar           = '.rodape .btn-pesquisar';
    this.config.html.qntResultados          = '.rodape .quantidade-resultados';

    this.config.objeto                      = {};
    this.config.objeto.nome                 = 'obj';
    this.config.objeto.campoId              = 'id';

    this.config.busca                       = {};
    this.config.busca.objCampo              = 'itens';

    this.config = $.simplesImob.servicos.ajudante.objeto.extensaoProfunda(this.config, config);

    this.config.html.resultadosContainer    = this.config.html.container + ' ' + this.config.html.resultadosContainer;
    this.config.html.resultadosItens        = this.config.html.container + ' ' + this.config.html.resultadosItens;
    this.config.html.btnVerSelecionados     = this.config.html.container + ' ' + this.config.html.btnVerSelecionados;
    this.config.html.btnVerResultadosBusca  = this.config.html.container + ' ' + this.config.html.btnVerResultadosBusca;
    this.config.html.paginacao              = this.config.html.container + ' ' + this.config.html.paginacao;
    this.config.html.btnFinalizar           = this.config.html.container + ' ' + this.config.html.btnFinalizar;
    this.config.html.btnPesquisar           = this.config.html.container + ' ' + this.config.html.btnPesquisar;
    this.config.html.qntResultados          = this.config.html.container + ' ' + this.config.html.qntResultados;

    this.selecionados       = {};
    this.ultimoDadosObtidos = {};

    if(this.config.modo == this.modos.selecao) {
        $(this.config.html.btnVerSelecionados).on('click', function(){  _this.mostrarSelecionados(_this); });

        $(this.config.html.btnVerResultadosBusca).on('click', function(){  _this.mostrarUltimosDadosObtidos(_this); });

        $(this.config.html.btnFinalizar).on('click', function(){  _this.preFinalizar(_this); });
    }
    else {
        $(this.config.html.btnVerSelecionados).hide();

        $(this.config.html.btnVerResultadosBusca).hide();

        $(this.config.html.btnFinalizar).hide();
    }

    var _this = this;

    this.paginacao = $(this.config.html.paginacao).pagination({
        items: 0,
        itemsOnPage: _this.config.limiteResultados,
        cssStyle: 'light-theme',
        nextText: 'Próximo',
        prevText: 'Anterior',
        hrefTextPrefix: 'javascript:void(0)',
        displayedPages: 4,
        onPageClick : function(pagina) {
            _this.buscar(pagina);
        }
    });

    if(this.config.aoIniciarSeTiverParamBuscar) {
        try {
            var filtro = JSON.parse($.simplesImob.servicos.ajudante.url.obterParametro(this.config.parametroUrl));

            if(filtro !== null && filtro.pagina) {
                $(this.form).dejsonify(filtro.filtro);

                this.buscar(filtro.pagina, true);
            }

        } catch (e) {  }
    }
};

TemplateFiltroClass.prototype = {
    constructor: TemplateFiltroClass,

    itensParaHTML: function(_this, itens){
        var html = '';
        $.each(itens, function(index, item){
            html += '<div class="' + _this.config.html.resultadoItemClass + '">#' + item[_this.config.objeto.campoId] + '</div>';
        });

        return html;
    },
    antesDeItensParaHtml: function(_this){
        //
    },
    depoisDeItensParaHtml: function(_this){
        $.simplesImob.servicos.ajudante.ui.ativarTooltip();
    },
    obterObjetoElemento: function(elem){
        return $(elem).data('obj');
    },
    monstrarMensagemNadaEncontrado: function (_this)
    {
        $(_this.config.html.nadaEncontrado).show();
    },
    esconderMensagemNadaEncontrado: function (_this)
    {
        $(_this.config.html.nadaEncontrado).hide();
    },
    clickElemento: function(_this, elem){
        var obj = _this.obterObjetoElemento(elem);

        if($(elem).hasClass('selecionado'))
        {
            $(elem).removeClass('selecionado');

            delete _this.selecionados[obj[_this.config.objeto.campoId]];
        }
        else
        {
            if( ! _this.config.multiplo)
            {
                _this.selecionados = {};
                $(elem).closest(_this.config.html.resultadosItens).find('.' + _this.config.html.resultadoItemClass).removeClass('selecionado');
            }

            $(elem).addClass('selecionado');

            _this.selecionados[obj[_this.config.objeto.campoId]] = obj;
        }

        _this.atualizarQuantidadeSelecionados(_this);
    },
    montarResultado: function(_this, itens) {
        _this.antesDeItensParaHtml(_this);

        $(_this.config.html.resultadosItens).fadeOut(200, function(){

            $(this).html(_this.itensParaHTML(_this, itens));

            _this.depoisDeItensParaHtml(_this);

            $.each(_this.selecionados, function(index, item){
                $(_this.config.html.resultadosItens).find('.' + _this.config.html.resultadoItemClass + '[data-' + _this.config.objeto.nome + '-id="' + item[_this.config.objeto.campoId] + '"]').addClass('selecionado');
            });

            $(this).fadeIn(200);

            if(_this.config.modo == _this.modos.selecao) {
                $(_this.config.html.resultadosItens).find('.' + _this.config.html.resultadoItemClass).on('click', function(){
                    _this.clickElemento(_this, this);
                });
            }

            _this.atualizarQuantidadeSelecionados(_this);
        });
    },
    buscar: function(pagina, forcarTotal) {

        var btn = $(this.config.html.btnPesquisar);
        btn.prop('disabled', true);

        var resultadosContainer = $(this.config.html.resultadosContainer);
        $.simplesImob.servicos.ajudante.ui.bloquearElemento(resultadosContainer, 'Buscando...');

        var dados = {
            filtro: $(this.form).jsonify({ignoreCheckboxes: false, inputIfCheckedToInt: true}),
            pagina: pagina || 1,
            forcarTotal: forcarTotal || false,
            limite: this.config.limiteResultados
        };

        var _this = this;

        _this.paginacao.pagination('disable');

        if(_this.config.atualizarUrl)
            $.simplesImob.servicos.ajudante.url.setarParametro(_this.config.parametroUrl, JSON.stringify(dados));

        $.simplesImob.requisitar(
            $.simplesImob.requisicao.nova({
                dados: dados,
                url: this.urlBusca,
                chamadaRetorno: {
                    sucesso: function(dados){

                        if(dados.total)
                        {
                            _this.ultimoTotalObtido = dados.total;
                            _this.paginacao.pagination('updateItems', dados.total);
                        }

                        _this.ultimoDadosObtidos = dados[_this.config.busca.objCampo];

                        _this.mostrarUltimosDadosObtidos(_this);

                        if(dados[_this.config.busca.objCampo].length > 0)
                            _this.esconderMensagemNadaEncontrado(_this);
                        else
                            _this.monstrarMensagemNadaEncontrado(_this);

                    },
                    falha: function(dados){
                        console.log('Falha ao buscar informações', dados);
                    },
                    erro: function(){
                        console.log('Erro ao buscar informações');
                    },
                    completo: function(){
                        btn.prop('disabled', false);
                        _this.paginacao.pagination('enable');
                        _this.paginacao.pagination('drawPage', pagina);
                        $.simplesImob.servicos.ajudante.ui.desbloquearElemento(resultadosContainer);
                    },
                }
            })
        );
    },
    atualizarQuantidadeSelecionados: function(_this){
        var qntResultados = $(_this.config.html.qntResultados);

        var quantidadeSelecionados = Object.values(_this.selecionados).length;

        qntResultados.find('.selecionados').text(quantidadeSelecionados);
        qntResultados.find('.total').text((_this.config.modo == _this.modos.selecao ? ' / ' : '') + _this.ultimoTotalObtido);

        $(_this.config.html.btnFinalizar).prop('disabled', quantidadeSelecionados == 0)
    },
    mostrarSelecionados: function(_this){
        _this.montarResultado(_this, Object.values(_this.selecionados));

        $(_this.config.html.qntResultados).fadeOut(200, function(){
            $(_this.config.html.btnVerResultadosBusca).fadeIn(200);
        });

        $(_this.config.html.paginacao).fadeOut(200);
    },
    mostrarUltimosDadosObtidos: function(_this){

        _this.montarResultado(_this, _this.ultimoDadosObtidos);

        $(_this.config.html.btnVerResultadosBusca).fadeOut(200, function(){
            $(_this.config.html.qntResultados).fadeIn(200);
        });

        $(_this.config.html.paginacao).fadeIn(200);
    },
    preFinalizar: function(_this){
        var valor = Object.values(_this.selecionados);

        if( ! _this.config.multiplo)
        {
            if(valor.length > 0)
                valor = valor[0];
            else
                valor = null;
        }

        _this.finalizar(valor);
    },
    finalizar: function(valor){
        console.log(valor);
    }
};