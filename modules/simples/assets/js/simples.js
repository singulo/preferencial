//EXIBE NOTIFICAÇÔES A DIREITA
alertify.logPosition("bottom right");

//TROCA MENSAGEM DA VALIDAÇÂO
$.validator.messages.required   = 'Requerido';
$.validator.messages.minlength  = 'Preencha com no mínimo {0} characters';
$.validator.messages.email      = 'Digite um email valido';

$.simplesImob = {
    ambiente: $(document.createElement('simplesImob')),
    ambienteEventosNome: {
        FB_API_INICIADA:                                'fb_api_iniciada',
        FB_CLIENTE_CLICK_BOTAO_LOGIN:                   'fb_cliente_click_botao_login',
        GOOGLE_API_INICIADA:                            'google_api_iniciada',
        GOOGLE_CLIENTE_CLICK_BOTAO_LOGIN:               'google_cliente_click_botao_login',
        CLIENTE_LOGOU:                                  'simplesimob_cliente_logou',
        CLIENTE_SE_CADASTROU:                           'simplesimob_cliente_cadastrou',
        CLIENTE_SE_CADASTROU_E_LOGOU:                   'simplesimob_cliente_se_cadastrou_e_logou',
        CLIENTE_ATUALIZOU_DADOS_DO_CADASTRO:            'simplesimob_cliente_atualizou_dados_do_cadastro',
        CLIENTE_ENVIOU_CONTATO_NORMAL:                  'simplesimob_cliente_enviou_contato_normal',
        CLIENTE_ENVIOU_CONTATO_INTERESSE_EM_IMOVEL:     'simplesimob_cliente_enviou_contato_interesse_em_imovel',
        CLIENTE_VISUALIZANDO_IMOVEL:                    'simplesimob_cliente_visualizando_imovel',
    },
    requisicao: {
        chamadasRetorno: function(opcoes){
            return $.extend( true, {}, $.simplesImob.requisicao.padrao.chamadaRetorno, opcoes);
        },
        nova: function(opcoes){
            return $.extend( true, {}, $.simplesImob.requisicao.padrao, opcoes);
        },
        padrao: {
            url: '',
            dados: {},
            metodo: 'POST',
            chamadaRetorno: {
                sucesso:  function(dados){},
                falha:    function(dados){},
                erro:     function(jqXhr, textStatus, errorThrown){},
                completo: function(){}
            },
        }
    },
    requisitar: function(simplesImobRequisicao){
        $.ajax({
            url: simplesImobRequisicao.url,
            type: simplesImobRequisicao.metodo,
            dataType: "json",
            data: simplesImobRequisicao.dados,
            success: function (dados) {
                if(dados.status)
                    simplesImobRequisicao.chamadaRetorno.sucesso(dados);
                else
                    simplesImobRequisicao.chamadaRetorno.falha(dados);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                simplesImobRequisicao.chamadaRetorno.erro(jqXhr, textStatus, errorThrown);
            },
            complete: function(){
                simplesImobRequisicao.chamadaRetorno.completo();
            }
        });
    },
    servicos: {},
    preparar: function(){
        var funcUsuarioEmVariaveisSistema = function(usuario){
            if(usuario != undefined)
                VariaveisSistema.Usuario = usuario;
        };

        $.simplesImob.ambiente.on($.simplesImob.ambienteEventosNome.CLIENTE_LOGOU, funcUsuarioEmVariaveisSistema);
        $.simplesImob.ambiente.on($.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU, funcUsuarioEmVariaveisSistema);
        $.simplesImob.ambiente.on($.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU_E_LOGOU, funcUsuarioEmVariaveisSistema);

        if($.simplesImob.servicos.ajudante.ambiente.visualiando.imovel())
            $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_VISUALIZANDO_IMOVEL, VariaveisSistema.Imovel);
    }
};

$.simplesImob.servicos.ajudante = {
    ambiente: {
        visualiando: {
            imovel: function(){
                return VariaveisSistema.Imovel != undefined;
            },
            condominio: function(){
                return VariaveisSistema.Condominio != undefined;
            },
        }
    },
    url: {
        obterParametro: function(parametro, link){
            if (!link) link = window.location.href;

            var url = new URL(link);

            return url.searchParams.get(parametro);
        },
        setarParametro: function(parametro, valor) {
            if (history.pushState) {
                var params = new URLSearchParams(window.location.search);
                params.set(parametro, valor);
                var novaUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + params.toString();
                window.history.pushState({ path: novaUrl },'', novaUrl);
            }
        },
        obterFinalidadeClicada: function(){
            var finalidadeClicada = Number.parseInt($.simplesImob.servicos.ajudante.url.obterParametro('finalidade_clique_miniatura'));
            if(Number.isInteger(finalidadeClicada))
                return finalidadeClicada;
            else
                return 0;
        }
    },
    formatarBaseUrlFilial: function(acrescentar, filial_na_sessao){
        if(filial_na_sessao === true)
            return  VariaveisSistema.BaseUrl + acrescentar;
        else
        {
            var filial_acrescentar = 'filial=' + VariaveisSistema.FilialSessao;

            if (acrescentar.indexOf("?") !== -1)
                filial_acrescentar = acrescentar + '&' + filial_acrescentar;
            else
                filial_acrescentar = acrescentar + '?' + filial_acrescentar;

            return VariaveisSistema.BaseUrl + filial_acrescentar;
        }
    },
    acessoMobile: function(){
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    },
    texto: {
        paraPlural: function(valor, singular, plural) {
            return valor > 1 ? plural : singular;
        }
    },
    objeto: {
        extensaoProfunda: function(objeto, sobrecarga){
            return $.extend( true, {}, objeto, sobrecarga || {});
        }
    },
    ui: {
        bloquearElemento: function(item, msg){
            $(item).block({
                message: '<img src="' + VariaveisSistema.GifCarregando + '" width="20px" alt=""><p style="margin-bottom: 0px; margin-top: 10px; color: #909090;">' + (msg || '') + '</p>',
                css: {
                    border: 'none',
                    padding: '0px',
                    width: '100%',
                    height: '20px',
                    backgroundColor: 'transparent'
                },
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.98,
                    cursor: 'wait'
                }
            });
        },
        desbloquearElemento: function(item){
            $(item).unblock();
        },
        ativarTooltip: function(){
            $('[data-toggle="tooltip"]').tooltip();
        }
    }
};

$.simplesImob.servicos.imovel = {
    vars: {
        filtro: {
            params:         {},
            pagina:         {},
            limite:         12,
            ordenacao:      'valor ASC',
            forcarTotal:    false
        }
    },
    mensagens:{
        REQUISITAR_MIDIAS_FALHA:            'Ocorreu uma falha ao tentar obter as mídias do imóvel. Por favor tente mais tarde!',
        REQUISITAR_MIDIAS_ERRO:             'Ocorreu um erro ao tentar obter as mídias do imóvel. Por favor tente mais tarde!',
        REQUISITAR_PESQUISAR_FALHA:         'Ocorreu uma falha ao obter os imóveis. Por favor tente mais tarde!',
        REQUISITAR_PESQUISAR_ERRO:          'Ocorreu um erro ao obter os imóveis. Por favor tente mais tarde!',
    },
    requisitar: {
        pesquisar: function(filtro, simplesImobChamadasRetorno){
            filtro = $.extend( true, {}, $.simplesImob.servicos.imovel.vars.filtro, filtro);

            simplesImobChamadasRetorno = $.extend( true, {},
                $.simplesImob.requisicao.chamadasRetorno({
                    falha: function (dados){
                        alertify.error($.simplesImob.servicos.imovel.mensagens.REQUISITAR_PESQUISAR_FALHA);
                    },
                    erro: function(jqXhr, textStatus, errorThrown){
                        alertify.error($.simplesImob.servicos.imovel.mensagens.REQUISITAR_PESQUISAR_ERRO);
                    }
                }), simplesImobChamadasRetorno);

            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url: 	$.simplesImob.servicos.ajudante.formatarBaseUrlFilial('imovel/buscar', true),
                    dados: 	filtro,
                    chamadaRetorno: simplesImobChamadasRetorno
                })
            );

            if(simplesImobChamadasRetorno.atualizarUrl !== false)
                window.history.pushState('', document.title, VariaveisSistema.BaseUrl + 'imovel/pesquisar?' + $.param(filtro));
        },
        midias: function(id, simplesImobChamadasRetorno){
            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url: 	VariaveisSistema.BaseUrl + 'imovel/midias',
                    dados: 	{ id: id},
                    chamadaRetorno: simplesImobChamadasRetorno
                })
            );
        }
    },
    montarURL: function(imovel){
        return $.simplesImob.servicos.ajudante.formatarBaseUrlFilial(( imovel.seo_link != null && imovel.seo_link.length > 0 ?
            imovel.seo_link + '?finalidade_clique_miniatura=' + imovel.finalidade: 'imovel?id=' + imovel.id + '&finalidade_clique_miniatura=' + imovel.finalidade), true);
    }
};

$.simplesImob.servicos.usuario = {
    mensagens: {
        REQUISITAR_TENTATIVA_LOGIN_CADASTRO_FALHA:      'Ocorreu uma falha ao tentar logar/cadastrar',
        REQUISITAR_TENTATIVA_LOGIN_CADASTRO_ERRO:       'Ocorreu um erro ao tentar logar/cadastrar.',
        REQUISITAR_CONTATO_NORMAL_ENVIADO_SUCESSO:      'Contato enviado. Por favor aguarde nosso retorno.',
        REQUISITAR_CONTATO_NORMAL_ENVIADO_FALHA:        'Ocorreu uma falha ao enviar seu contato. Por favor tente mais tarde!',
        REQUISITAR_CONTATO_NORMAL_ENVIADO_ERRO:         'Ocorreu um erro ao enviar seu contato. Por favor tente mais tarde!',
        REQUISITAR_CONTATO_INTERESSE_ENVIADO_SUCESSO:   'Contato enviado. Por favor aguarde nosso retorno.',
        REQUISITAR_CONTATO_INTERESSE_ENVIADO_FALHA:     'Ocorreu uma falha ao enviar seu contato. Por favor tente mais tarde!',
        REQUISITAR_CONTATO_INTERESSE_ENVIADO_ERRO:      'Ocorreu um erro ao enviar seu contato. Por favor tente mais tarde!',
    },
    vars: {
        metodosDeCadastro: {
            ultimo: '',
            tipos: {
                Nenhum:     '',
                Normal:     'Normal',
                Facebook:   'Facebook',
                Gmail:      'Gmail',
            }
        }
    },
    requisitar: {
        tentarLogarEmailSeNecessarioCadastrar: function(email, dados, chamadaRetornoCompleto) {
            // No caso do email não existir é tentado o cadastro
            // se o parametro "nome" for informado
            var _dados;
            if(dados != null && typeof dados === 'object')
            {
                dados.email = email;
                _dados = dados;
            }
            else
                _dados = {email: email};

            var finalidadeClicada = $.simplesImob.servicos.ajudante.url.obterFinalidadeClicada();
            if(finalidadeClicada > 0)
            {
                if(VariaveisSistema.Imovel != undefined){
                    _dados.visualizando_imovel = VariaveisSistema.Imovel.id;
                    _dados.finalidade_clicada = finalidadeClicada;
                }
            }

            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url:    VariaveisSistema.BaseUrl + 'cliente/login',
                    dados:  _dados,
                    chamadaRetorno: {
                        sucesso: function(dados) {
                            if(dados.foi_cadastrado == true)
                                $.simplesImob.ambiente.trigger(
                                    $.simplesImob.ambienteEventosNome.CLIENTE_SE_CADASTROU_E_LOGOU,
                                    dados.usuario,
                                    (_dados.metodo_cadastro != undefined && _dados.metodo_cadastro.length > 0 ?
                                        _dados.metodo_cadastro : $.simplesImob.servicos.usuario.vars.metodosDeCadastro.tipos.Nenhum));
                            else
                                $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.CLIENTE_LOGOU, dados.usuario);
                        },
                        falha: function (dados){
                            alertify.error(dados.msg || $.simplesImob.servicos.usuario.mensagens.REQUISITAR_TENTATIVA_LOGIN_CADASTRO_FALHA);
                        },
                        erro: function(jqXhr, textStatus, errorThrown){
                            alertify.error($.simplesImob.servicos.usuario.mensagens.REQUISITAR_TENTATIVA_LOGIN_CADASTRO_ERRO);
                        },
                        completo: function(){
                            if(chamadaRetornoCompleto && $.isFunction(chamadaRetornoCompleto))
                                chamadaRetornoCompleto();
                        }
                    }
                })
            );
        }
    }
};

$.simplesImob.servicos.incc = {
    documentoPronto: function(){
        $.simplesImob.servicos.incc.preecheElementos();
    },
    /*
     * Substitui o texto de todos elementos INCC utilizando o attr data-text como formatador do texto.*
     *
     * Ex elemento:
     * <incc data-text="INCC-M / {data}: {valor}"></incc>
     * <incc data-text="{data}: {valor}%"></incc>
     *
     * Ex saída:
     * <incc data-text="INCC-M / {data}: {valor}">INCC-M / 11/2016: 0,17</incc>
     * <incc data-text="{data}: {valor}%">11/2016: 0,17%</incc>
     */
    preecheElementos: function(){
        if($('incc').length){
            $.simplesImob.servicos.incc.mesAtual({
                sucesso: function(dados){
                    $.each($('incc'), function(index, elem) {
                        var text = '{data}: {valor}';

                        if($(elem).data('text') != undefined)
                            text = $(elem).data('text');

                        $(elem).text(text.replace('{data}', dados.incc.data).replace('{valor}', dados.incc.valor));
                    });
                }
            })
        }
    },
    requisitar: {
        mesAtual: function(simplesImobChamadasRetorno){
            var agora                   = new Date();
            var mesAtual                = (agora.getMonth() + 1);
            var mesAnoAtual             = (mesAtual > 9 ? mesAtual : '0' + mesAtual) + '/' + agora.getFullYear();
            var incc                    = localStorage.getItem('incc_' + mesAnoAtual);
            simplesImobChamadasRetorno  = $.simplesImob.requisicao.chamadasRetorno(simplesImobChamadasRetorno);

            if(incc == null){
                $.simplesImob.requisitar(
                    $.simplesImob.requisicao.nova({
                        url: VariaveisSistema.SimplesLink + 'api/conteudo/incc',
                        chamadaRetorno: {
                            sucesso: function(dados){
                                localStorage.setItem('incc_' + dados.incc.data, dados.incc.valor);
                                simplesImobChamadasRetorno.sucesso(dados);
                            },
                            falha: function(dados){
                                simplesImobChamadasRetorno.falha(dados);
                            },
                            erro: function(jqXhr, textStatus, errorThrown){
                                simplesImobChamadasRetorno.erro(jqXhr, textStatus, errorThrown);
                            },
                            completo: function(){
                                simplesImobChamadasRetorno.completo();
                            }
                        }
                    })
                );
            }
            else {
                simplesImobChamadasRetorno.sucesso({
                    status: true,
                    incc: {
                        data: mesAnoAtual,
                        valor: incc
                    }
                });
            }
        }
    }
};

$(document).ready(function(){
    $.simplesImob.servicos.incc.documentoPronto();
});

document.addEventListener("DOMContentLoaded", function(event) {
    if(VariaveisSistema.SiteConfigInteracoesJsonExterno != undefined)
        $.getJSON(
            VariaveisSistema.SiteConfigInteracoesJsonExterno + '?v=' + Math.floor(Math.random() * 101),
            function( interacoes ){

                var urlInteracao = document.location.href.replace(VariaveisSistema.BaseUrl, '');
                var interacao = null;

                var interacoesCodigos = [];

                $.each(interacoes, function(index, _interacao) {

                    interacoesCodigos.push(_interacao.codigo);

                    // BUSCA A INTERAÇÃO PARA A URL ATUAL
                    $.each(_interacao.urls, function (index, url) {
                        if(url == urlInteracao)
                        {
                            var exibir_apartir = new Date(_interacao.exibido_de + ' 00:00');
                            var exibir_ate = new Date(_interacao.exibido_ate + ' 00:00');
                            var hoje = new Date(new Date().toISOString().slice(0,10) + ' 00:00');

                            if((_interacao.exibido_de.length == 0 ||
                                    (exibir_apartir != 'Invalid Date' && exibir_apartir <= hoje))
                                &&
                                (_interacao.exibido_ate.length == 0 ||
                                    (exibir_ate != 'Invalid Date' && hoje <= exibir_ate))
                            ){
                                interacao = _interacao;
                            }
                            return;
                        }
                    });
                });

                // REMOVE INTERAÇÕES ANTIGAS QUE NÃO EXISTEM MAIS
                var interacoesConcluidas = localStorage.getItem('interacoes');
                interacoesConcluidas = interacoesConcluidas == null ? [] : interacoesConcluidas.split(',');

                interacoesConcluidas = interacoesConcluidas.filter(function(e) {
                    return $.inArray(e, interacoesCodigos) != -1;
                });

                if(interacoesConcluidas.length == 0)
                    localStorage.removeItem('interacoes');
                else
                    localStorage.setItem('interacoes', interacoesConcluidas);

                // NÃO MOSTRA NENHUMA INTERAÇÃO POR ESTAR MARCADA COMO CONCLUÍDA OU NÃO EXIBIR MAIS
                if(interacao == null || $.inArray(interacao.codigo, interacoesConcluidas) != -1)
                    return;

                if(interacao.tipo == 1)
                {
                    if($.magnificPopup != undefined)
                    {
                        setTimeout(function(){
                            $.magnificPopup.open({
                                tClose: 'Fechar (Esc)',
                                tLoading: 'Aguarde...',
                                image: {
                                    tError: '<a href="%url%">A imagem</a> não pode ser carregada.'
                                },
                                items: {
                                    src: interacao.img_src
                                },
                                type: 'image',
                                callbacks: {
                                    imageLoadComplete: function() {
                                        if(interacao.redirecionamento.length > 0)
                                        {
                                            $('img.mfp-img[src="' + interacao.img_src + '"]').on('click', function(){
                                                document.location.href = interacao.redirecionamento;
                                            });
                                        }
                                    },
                                    open: function() {
                                        $('.mfp-figure').append('<label style="position: absolute; margin-top: -75px; margin-left: 20px; color: #fff; font-weight: 300; text-shadow: 1px 1px #0c0c0c;"><input type="checkbox" onchange="SimplesScript.usuario.modais.funcs.checkboxExibicaoInteracao(this);" value="' + interacao.codigo + '"> Não mostrar mais</label>');
                                    }
                                }
                            });
                        }, interacao.esperar * 1000);
                    }
                    else if($.fancybox != undefined)
                    {
                        setTimeout(function(){
                            $.fancybox({
                                href: interacao.img_src,
                                afterShow: function () {
                                    $("img.fancybox-image").click(function () {
                                        document.location.href = interacao.redirecionamento;
                                    });

                                    $('.fancybox-inner').append('<label style="position: absolute; margin-top: -30px; margin-left: 15px; color: #fff; font-weight: 300; text-shadow: 1px 1px #0c0c0c;"><input type="checkbox" onchange="SimplesScript.usuario.modais.funcs.checkboxExibicaoInteracao(this);" value="' + interacao.codigo + '"> Não mostrar mais</label>');
                                },
                                tpl: {
                                    error    : '<p class="fancybox-error">O arquivo requisitado não pode ser carregado.<br/>Por favor tente mais tarde.</p>',
                                    closeBtn : '<a title="Fechar" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                                }
                            });
                        }, interacao.esperar * 1000);
                    }
                }
                else if(interacao.tipo == 2)
                {
                    var modalInteracao = $(SimplesScript.usuario.modais.vars.modalInteracao);

                    if(modalInteracao.length)
                    {
                        modalInteracao.find('h4.modal-title').text(interacao.form_titulo);
                        modalInteracao.find('p.interacao-descricao').html(interacao.form_descricao);
                        modalInteracao.find('span.mensagem-conclusao-sucesso').html(interacao.form_mensagem_ao_concluir_formulario);

                        var formModalInteracao = $(SimplesScript.usuario.modais.vars.formEmModalInteracao);

                        formModalInteracao.find('input[name=codigo]').val(interacao.codigo);
                        formModalInteracao.find('input[name=destinatarios]').val(interacao.emails);
                        formModalInteracao.find('input[name=telefone_1]').prop('required', (interacao.form_campo_telefone_obrigatorio == 1));
                        formModalInteracao.find('select[name=uf]').prop('required', (interacao.form_campo_telefone_uf_cidade == 1));
                        formModalInteracao.find('select[name=cidade]').prop('required', (interacao.form_campo_telefone_uf_cidade == 1));
                        formModalInteracao.find('input[name=cadastrar_cliente_automaticamente]').val(interacao.cadastrar_cliente_automaticamente);
                        formModalInteracao.find('input[name=email_assunto]').val(interacao.email_assunto);
                        formModalInteracao.find('textarea[name=obs]').prop('required', interacao.form_campo_observacao == 1);

                        modalInteracao.find('.iteracao-codigo').text('Ação: ' + interacao.codigo);

                        setTimeout(function(){
                            modalInteracao.modal('show');
                        }, interacao.esperar * 1000);
                    }
                    else
                        throw new Error('A modal de interação do tipo formulário não foi encontrada.');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                if(jqXHR.status == 404)
                    console.log('Arquivo de interação ainda não criado');
            });
    else
        throw new Error('A input com o valor do caminho para o arquivo das interações não foi encontrado.');
});

var SimplesScript = {
    usuario: {
        modais: {
            vars: {
                modalLoginCadastro: '#modal-login-cadastro',
                modalDados: '#modal-cliente-dados',
                modalInteracao: '#modal-interacao-formulario',
                formEmModalDados: '#modal-cliente-dados #form-cliente-dados',
                formEmModalInteracao: '#modal-interacao-formulario #form-interacao-formulario',

                salvandoInteracao: false,
            },
            funcs: {
                preecherFormModalDadosComUsuarioLogado: function(usuario){
                    var formDados = $(SimplesScript.usuario.modais.vars.formEmModalDados);
                    formDados.dejsonify(usuario);

                    if(usuario.uf != null && usuario.uf.length > 0){
                        formDados.find('select[name=cidade]').attr('data-cidade-default', usuario.cidade.toUpperCase());
                        formDados.find('select[name=uf]').selectpicker('refresh');
                        SimplesScript.funcs.obterCidadesEPreencherSelect($(SimplesScript.usuario.modais.vars.formEmModalDados), usuario.cidade.toUpperCase());
                    }

                    var creci = '';

                    if(usuario.corretor.nivel != 4)
                        creci = 'creci: ' + usuario.corretor.creci;

                    var modalDados = $(SimplesScript.usuario.modais.vars.modalDados);

                    modalDados.find('img.corretor-foto').attr('src', VariaveisSistema.FilialFotosCorretores + usuario.corretor.id_corretor + '.jpg');
                    modalDados.find('.corretor-nome').text(usuario.corretor.nome);
                    modalDados.find('.corretor-creci').text(creci);
                    modalDados.find('.corretor-email').html('email: <a href="mailto:' + usuario.corretor.email + '" target="_top">' + usuario.corretor.email + '</a>');
                },
                obterCidadesDeAcordoComAUFSelecionadaNoFormDaModalDadosUsuarioLogado: function(){
                    SimplesScript.funcs.obterCidadesEPreencherSelect($(SimplesScript.usuario.modais.vars.formEmModalDados));
                },
                checkboxExibicaoInteracao: function(checkbox){
                    var codigo = $(checkbox).val();

                    var interacoesConcluidas = localStorage.getItem('interacoes');
                    interacoesConcluidas = interacoesConcluidas == null ? [] : interacoesConcluidas.split(',');

                    if($(checkbox).is(':checked') && $.inArray(codigo, interacoesConcluidas) == -1)
                        interacoesConcluidas.push(codigo);
                    else
                        interacoesConcluidas = interacoesConcluidas.filter(function(e) { return e != codigo });

                    localStorage.setItem('interacoes', interacoesConcluidas);
                },
                salvarInteracao: function() {
                    if($(SimplesScript.usuario.modais.vars.formEmModalInteracao).valid())
                    {
                        $(SimplesScript.usuario.modais.vars.modalInteracao + ' .modal-footer .btn').button('loading');
                        SimplesScript.usuario.modais.vars.salvandoInteracao = true;

                        $.simplesImob.requisitar(
                            $.simplesImob.requisicao.nova({
                                url:    VariaveisSistema.BaseUrl + 'contato/enviar_interacao_formulario',
                                dados:  $(SimplesScript.usuario.modais.vars.formEmModalInteracao).jsonify(),
                                chamadaRetorno: {
                                    sucesso: function(dados){
                                        alertify.success($(SimplesScript.usuario.modais.vars.modalInteracao + ' span.mensagem-conclusao-sucesso').text());

                                        var interacoesConcluidas = localStorage.getItem('interacoes');

                                        //REGISTRA INTERAÇÃO PARA NÃO EXIBIR NOVAMENTE
                                        interacoesConcluidas = interacoesConcluidas == null ? [] : interacoesConcluidas.split(',');
                                        var codigo = $(SimplesScript.usuario.modais.vars.formEmModalInteracao + ' input[name=codigo]').val();
                                        if($.inArray(codigo, interacoesConcluidas) == -1) {
                                            interacoesConcluidas.push(codigo);
                                            localStorage.setItem('interacoes', interacoesConcluidas);
                                        }

                                        SimplesScript.usuario.modais.vars.salvandoInteracao = false;
                                        $(SimplesScript.usuario.modais.vars.modalInteracao).modal('hide');
                                    },
                                    falha: function(data){
                                        alertify.error(data.msg || 'Ocorreu uma falha ao enviar seu contato.');
                                    },
                                    erro: function(jqXhr, textStatus, errorThrown){
                                        alertify.error('Ocorreu um erro ao enviar seu contato.');
                                    },
                                    completo: function(){
                                        SimplesScript.usuario.modais.vars.salvandoInteracao = false;
                                        $(SimplesScript.usuario.modais.vars.modalInteracao + ' .modal-footer .btn').button('reset');
                                    }
                                }
                            })
                        );
                    }
                },
            }
        },
        perfil: {
            vars: {
                imovelValoresRange:{
                    5000: { intervalos: 250 },
                    10000: { intervalos: 500 },
                    50000: { intervalos: 2500 },
                    100000: { intervalos: 10000 },
                    350000: { intervalos: 25000 },
                    500000: { intervalos: 50000 },
                    850000: { intervalos: 100000 },
                    1350000: { intervalos: 250000 },
                    2500000: { intervalos: 500000 },
                },
            },
            funcs: {
                _calcularRangeValor: function(valorMax, valor, intervalo){
                    valorMax = Number.parseInt(valorMax);
                    valor = Number.parseInt(valor);
                    intervalo = Number.parseInt(intervalo);

                    if(valorMax == valor)
                        return {
                            min: valorMax - intervalo,
                            max: valorMax
                        };

                    if(valorMax > valor){
                        while(valorMax > valor && valorMax > 0)
                            valorMax = valorMax - intervalo;

                        if(valorMax <= 0)
                            return null;
                        else
                            return {
                                min: valorMax,
                                max: valorMax + intervalo,
                            };
                    }
                    else{
                        while(valorMax < valor)
                            valorMax = valorMax + intervalo;

                        return {
                            min: valorMax - intervalo,
                            max: valorMax,
                        };
                    }
                },
                peloImovel: function(imovel, finalidade){
                    var valor = imovel.valor;
                    if(finalidade == 2)
                        valor = imovel.valor_aluguel_mensal;
                    else if(finalidade == 3)
                        valor = imovel.valor_aluguel_diario;

                    var valores = Object.keys(SimplesScript.usuario.perfil.vars.imovelValoresRange);
                    if(valores[valores.length - 1] <= valor)
                        console.log(SimplesScript.usuario.perfil.funcs._calcularRangeValor(valores[valores.length - 1], valor, SimplesScript.usuario.perfil.vars.imovelValoresRange[valores[valores.length - 1]].intervalos));
                    else
                        $.each(SimplesScript.usuario.perfil.vars.imovelValoresRange, function(valorMax, intervalo){
                            if(valorMax >= valor)
                            {
                                console.log(SimplesScript.usuario.perfil.funcs._calcularRangeValor(valorMax, valor, intervalo.intervalos));
                                return false;
                            }
                        });
                }
            },
        }
    },
    ui: {
        gifCarregando: VariaveisSistema.GifCarregando,
        bloquear: function(item) {
            $(item).block({
                message: '<img src="' + SimplesScript.ui.gifCarregando + '" width="50px" alt="">',
                css: {
                    border: 'none',
                    padding: '0px',
                    width: '50px',
                    height: '50px',
                    backgroundColor: 'transparent'
                },
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.9,
                    cursor: 'wait'
                }
            });
        },
        desbloquear: function (item) {
            $(item).unblock();
        }
    },
    funcs: {
        documentReady: function(){
            $('.selectpicker').selectpicker();

            $(".dinheiro").maskMoney({symbol:'', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
            $('.telefone').mask('(99)9999-9999Z',{translation:  {'Z': {pattern: /[0-9]/, optional: true}}});

            $("#modal-cliente-dados").on('show.bs.modal', function () {
                if($(SimplesScript.usuario.modais.vars.formEmModalDados + ' select[name=uf]').val() != undefined && $(SimplesScript.usuario.modais.vars.formEmModalDados + ' select[name=uf]').val() != '')
                    SimplesScript.funcs.obterCidadesEPreencherSelect($(SimplesScript.usuario.modais.vars.formEmModalDados), $(SimplesScript.usuario.modais.vars.formEmModalDados + ' select[name=cidade]').attr('data-cidade-default'));
            });

            if($.simplesImob.servicos.ajudante.acessoMobile()) {
                $('select').attr('data-mobile', 'true');
            }
        },
        obterCidadesPelaUf: function(uf, simplesImobChamadasRetorno) {
            $.simplesImob.requisitar(
                $.simplesImob.requisicao.nova({
                    url: 	VariaveisSistema.BaseUrl + 'cidade/obter_cidade_pela_uf',
                    dados: 	{ uf: uf },
                    chamadaRetorno: simplesImobChamadasRetorno
                })
            );
        },
        obterCidadesEPreencherSelect: function(form, cidadeSelected) {
            var select_cidade = $($(form).find('select[name="cidade"]')[0]);
            select_cidade.find('.bs-title-option').text('Carregando...');
            select_cidade.find('option:not(.bs-title-option)').remove();
            select_cidade.prop('disabled', true);
            select_cidade.selectpicker('refresh');

            SimplesScript.funcs.obterCidadesPelaUf( $(form).find('select[name="uf"]').val(),
                $.simplesImob.requisicao.chamadasRetorno({
                    sucesso: function(dados) {
                        $.each(dados.cidades, function(index, cidade){
                            select_cidade.append($('<option>', {
                                value: cidade.nome.toUpperCase(),
                                text: cidade.nome
                            }));
                        });

                        select_cidade.find('.bs-title-option').text('Cidade');
                        select_cidade.prop('disabled', false);

                        if(cidadeSelected != undefined)
                            select_cidade.val(cidadeSelected);

                        if(cidadeSelected == "")
                            select_cidade.prop('disabled', true);

                        select_cidade.selectpicker('refresh');
                    },
                    falha: function(dados){
                        alertify.error('Ocorreu um erro ao carregar as cidades. Por favor tente mais tarde!');
                    },
                    erro: function(){
                        alertify.error('Ocorreu um erro ao carregar as cidades.');
                    }
                }
            ));
        },
        formatarValor: function(valor, cifrao) {
            if(valor == 'Consulte')
                return valor;
            else
            {
                if(typeof valor !== 'string' || valor.indexOf(',') === -1)
                {
                    var e = $('<div></div>');
                    e.autoNumeric('init', {aSep: '.', aDec: ',', vMax: 999999999.99});
                    valor = e.autoNumeric('set', valor).text();
                }

                return (cifrao || '') + valor;
            }
        },
        elemento: {
            posicionar: {
                em: function(deslocar, velocidade){
                    $('html, body').animate({scrollTop: deslocar}, velocidade);
                },
                noCentroDaTela: function (elemento, velocidade) {
                    var posicaoElem = $(elemento).offset().top;
                    var alturaElem = $(elemento).height();
                    var alturaJanela = $(window).height();
                    var deslocar;

                    if (alturaElem < alturaJanela) {
                        deslocar = posicaoElem - ((alturaJanela / 2) - (alturaElem / 2));
                    }
                    else {
                        deslocar = posicaoElem;
                    }
                    SimplesScript.funcs.elemento.posicionar.em(deslocar, velocidade);
                }
            }
        }
    }
};

$(document).ready(function(){
    $.simplesImob.preparar();

    SimplesScript.funcs.documentReady();
});