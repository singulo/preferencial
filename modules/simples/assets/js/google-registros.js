$.simplesImobGoogle = {
    atualizaStatus: function(status){
        console.log('evento atualiza status');
        if(status)
        {
            $('.btn.btn-login-google').data('loading-text', "<i class='fa fa-google'></i><span class='text-center'>Aguarde...</span>").button('loading'); // INFORMA AÇÃO DO GOOGLE NO BOTÃO
            gapi.client.plus.people.get({
                'userId': 'me'
                // 'requestMask.includeField': 'person.names,person.emailAddresses,person.photos,person.addresses,person.birthdays,'
            }).then(function(response) {

                var dados = {
                    nome: response.result.displayName,
                    id_google: response.result.id,
                    email: response.result.emails[0].value,
                    foto: response.result.image.url,
                    link_google: response.result.url
                };

                if(response.result.emails[1] != undefined)
                    dados.email_alternativo = response.result.emails[1];

                dados.metodo_cadastro = $.simplesImob.servicos.usuario.vars.metodosDeCadastro.tipos.Gmail;
                $.simplesImob.servicos.usuario.requisitar.tentarLogarEmailSeNecessarioCadastrar(
                    response.result.emails[0].value, //EMAIL PRINCIPAL
                    dados,
                    $.simplesImob.requisicao.chamadasRetorno({
                        completo: function(){
                            $('.btn.btn-login-google').button('reset'); // RESETA BOTÃO
                        }
                    })
                );

            }, function(reason) {
                //console.log('Error: ' + reason.result.error.message);
                $('.btn.btn-login-google').button('reset');
            });
        }
        else
            gapi.auth2.getAuthInstance().signIn();
    },
    login: function() {
        if( ! gapi.auth2.getAuthInstance().isSignedIn.get())
            gapi.auth2.getAuthInstance().signIn();
        else
        {
            console.log('desloga atual, e evento abre a modal de login');
            gapi.auth2.getAuthInstance().signOut();
        }
    }
};

$.simplesImob.ambiente.on($.simplesImob.ambienteEventosNome.GOOGLE_API_INICIADA, function(){
    gapi.auth2.getAuthInstance().isSignedIn.listen($.simplesImobGoogle.atualizaStatus);

    $.simplesImob.ambiente.on(
        $.simplesImob.ambienteEventosNome.GOOGLE_CLIENTE_CLICK_BOTAO_LOGIN,
        $.simplesImobGoogle.login
    );
});