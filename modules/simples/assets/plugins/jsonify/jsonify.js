(function($) {
    $.fn.jsonify = function(options) {
        var settings = $.extend({
            stringify : false,
            ignoreCheckboxes: true,
            inputIfCheckedToInt: false
        }, options);
        var json = {};
        $.each(this.serializeArray(), function() {
            if (json[this.name]) {
                if (!json[this.name].push)
                    json[this.name] = [json[this.name]];
                json[this.name].push(this.value || '');
            } else
                json[this.name] = this.value || '';
        });

        if(settings.ignoreCheckboxes == false)
            $(this).find("input:checkbox").each(function(){
                if(settings.inputIfCheckedToInt === true)
                    json[this.name] = Number(this.checked);
                else
                    json[this.name] = this.checked;
            });

        return settings.stringify ? JSON.stringify(json) : json;
    };

    $.fn.dejsonify = function(data) {
        if (typeof data === 'string')
            data = JSON.parse(data);

        $.each(this.find('*[name]'), function() {

            if($(this)[0].type === 'checkbox')
                $(this)[0].checked = (data[$(this).attr('name')] == true);
            else
                $(this).val(data[$(this).attr('name')]);
        });
    };
})(jQuery);