<?php
require_once MODULESPATH . 'simples/helpers/filial_helper.php';
require_once MODULESPATH . 'simples/helpers/cliente_helper.php';

/**
 * @property Clientes_Model $clientes_model
 * @property Imobiliaria_Model $imobiliaria_model
 * @property CI_Session $session
 */
class Base_Controller extends MX_Controller
{
    public function __construct($redirecionar_se_db_nao_setado = true)
    {
        parent::__construct();

        if(isset($_GET['app']) && $_GET['app'] == 1)
        {
            foreach ((array)$this->request_para_obj() as $index => $value)
                if(! isset($_POST[$index]))
                    $_POST[$index] = $value;
        }

        $filial = NULL;
        if( $this->tenta_obter_sessao($filial) )
        {
            require_once APPPATH . 'config/filial/' . $filial . '.php';

            if( ! isset($_SESSION['filial']['chave']))
                armazena_filial_em_sessao();
        }
        else
        {
            redirect( base_url() );
            die;
        }

        if($this->session->has_userdata('cliente_filiais'))
        {
            $chave = $_SESSION['filial']['chave'];

            if( isset($_GET['filial']) && isset($this->session->userdata('cliente_filiais')[$chave]) )
            {
                $cliente = $this->session->userdata('cliente_filiais')[$chave];

                $this->session->unset_userdata('usuario');

                if( ! isset($cliente->ultimo_acesso_registrado) || ! $cliente->ultimo_acesso_registrado )
                {
                    $cliente->ultimo_acesso_registrado = registrar_acesso($cliente->id) > 0;
                    $this->session->set_userdata('usuario', $cliente);
                }
                else
                    $this->session->set_userdata('usuario', $cliente);
            }
        }
        else if(isset($_COOKIE['email_usado_para_login']))
        {
            seta_usuario_em_sessao_filiais($this, obter_registros_cliente_nas_filiais_pelo_email($_COOKIE['email_usado_para_login']), $_SESSION['filial']['chave']);
        }
    }

    protected function tenta_obter_sessao(&$filial = NULL)
    {
        // FILIAL PASSADA IGUAL A QUE JÁ ESTA EM SESSÃO
        if(isset($_GET['filial']) && isset($_SESSION['filial']) && $_SESSION['filial']['chave'] == $_GET['filial'])
        {
            $filial = $_SESSION['filial']['chave'];
            return TRUE;
        }
        else if( ! isset($_GET['filial']) && isset($_SESSION['filial']) )
        {
            $filial = $_SESSION['filial']['chave'];
            return TRUE;
        }
        else if(count($this->config->item('filiais')) == 1)
        {
            /**
             * No caso de não informar uma filial no parametro $_GET
             * e conter apenas uma filial essa mesma filial é setada automaticamente
             * para o carregamento futuro das configurações especificar da filial.
             */
            $filiais = $this->config->item('filiais');
            $filial = reset($filiais)['chave'];
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    private function request_para_obj()
    {
        $request = new stdClass();
        $postdata = file_get_contents("php://input");
        if (isset($postdata))
        {
            $request = json_decode($postdata);
        }

        return $request;
    }
}