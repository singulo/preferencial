<?php
require_once MODULESPATH . 'simples/core/Base_Model.php';

class Empreendimento_Model extends Base_Model
{
    protected $table;
    protected $tb_foto;
    public $tb_foto_foreign_key;
    public $tb_video;
    public $tb_video_foreign_key;
    protected $tb_empreendimento_complemento;
    protected $tb_complemento_foreign_key;
    protected $tb_complemento = 'tb_complemento';

    public function destaques($limite = 10, $rand = true, $tipos = array(), $complementos = array())
    {
        if($rand)
            $this->db->order_by('RAND()');

        if(count($tipos) > 0)
            $this->db->where_in('id_tipo', $tipos);

        if(count($complementos) > 0)
            $this->join_complementos($complementos);

        return $this->db->select()
                                ->where('destaque', 1)
                                    ->limit($limite)
                                        ->get($this->table)
                                            ->result();
    }

    protected function join_complementos($complementos)
    {
        $this->db->join($this->tb_empreendimento_complemento, "$this->table.id = $this->tb_empreendimento_complemento.$this->tb_complemento_foreign_key AND $this->tb_empreendimento_complemento.id_complemento IN (" . join(',', $complementos) . ")");
    }

    public function pelo_codigo($id, $foto_principal = true)
    {

        $this->table = 'tb_condominio_site';
        return $this->obter($id);
    }

    protected function select_foto_principal($add_ao_select = '')
    {
        if($add_ao_select == '')
            $add_ao_select = "$this->table.*";

        return $this->db->select("$add_ao_select, (SELECT arquivo FROM $this->tb_foto WHERE $this->tb_foto.$this->tb_foto_foreign_key = $this->table.id AND $this->tb_foto.mostrar_site = 1  ORDER BY $this->tb_foto.destaque DESC LIMIT 1) as foto");
    }

    public function pelos_codigos(array $ids)
    {
        return $this->db
            ->where_in('id', $ids)
            ->get($this->table)
            ->result();
    }

    public function complementos($id)
    {
        return $this->db
            ->from($this->tb_complemento)
            ->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_complemento = $this->tb_complemento.id AND $this->tb_empreendimento_complemento.$this->tb_complemento_foreign_key = $id")
            ->get()
            ->result();
    }

    public function total_fotos($id)
    {
        return $this->db
            ->where($this->tb_foto_foreign_key, $id)
            ->where('mostrar_site', 1)
            ->from($this->tb_foto)
            ->count_all_results();
    }

    public function total_midias($id)
    {
        return $this->db->query("SELECT 
                                      (SELECT count(*) FROM $this->tb_foto WHERE id_imovel = $id AND mostrar_site = 1) as fotos,
                                      (SELECT count(*) FROM $this->tb_video WHERE id_imovel = $id) as videos")->first_row();
    }

    public function fotos($id)
    {
        return $this->db
            ->select('fotos')
            ->where('id', $id)
            ->get($this->table)
            ->row();
    }

    public function obter_valores_e_finalidades($id)
    {
        return $this->db
            ->select('finalidade, valor_site')
            ->where('id', $id)
            ->get($this->table)
            ->result();
    }

    public function videos($id)
    {
        return $this->db
            ->where($this->tb_video_foreign_key, $id)
            ->get($this->tb_video)
            ->result();
    }
}