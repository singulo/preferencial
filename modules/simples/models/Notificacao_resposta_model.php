<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Notificacao_Resposta_Model extends Base_Model
{
    public $id;
    public $cliente_email;
    public $corretor_email;
    public $id_corretor;
    public $resposta;
    public $contato_id;
    public $contato_tipo;
    public $respondido_em;

    protected $table = 'tb_notificacao_resposta';

    public function novo(Notificacao_Resposta_Model $resposta)
    {
        $this->db->insert($this->table, $resposta);
        return $this->db->insert_id();
    }

}