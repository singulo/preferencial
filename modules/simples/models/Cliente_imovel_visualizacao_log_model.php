<?php
require_once(MODULESPATH . 'simples/core/Base_Model.php');

class Cliente_imovel_visualizacao_log_model extends Base_Model
{
    protected $table = 'tb_cliente_imovel_visualizacao_log';

    public function salvar($registro)
    {
        return $this->updateOnDuplicate($registro);
    }

    protected function updateOnDuplicateQuery($table, $values)
    {
        $sql  = $this->db->insert_string($table, $values);
        $sql .= "ON DUPLICATE KEY UPDATE visualizacoes = visualizacoes+1, data_ultima_visualizacao = '$values->data_ultima_visualizacao'";

        return $sql;
    }

    public function salvarNaoLogado($registros, $id_cliente)
    {
        $this->db->trans_begin();

        foreach(get_object_vars($registros) as $registro)
        {
            $registro->id_cliente           = $id_cliente;
            $registro->visualizou_logado    = 0;
            unset($registro->data_penultima_visualizacao);
            $this->salvar($registro);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}

class ClienteImovelVisualizacaoLogDomain
{
    public $id_cliente;
    public $id_imovel;
    public $finalidade;
    public $visualizou_logado;
    public $visualizacoes;
    public $data_ultima_visualizacao;
}