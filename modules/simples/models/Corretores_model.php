<?php
require_once MODULESPATH . 'simples/core/Base_Model.php';

class Corretores_Model extends Base_Model
{
    protected $table = 'tb_corretores';

    public function obter($id, $campo = 'id', $table = NULL)
    {
        return parent::obter($id, $campo, $table); // TODO: Change the autogenerated stub
    }

    public function set_db($filial)
    {
        parent::set_db($filial); // TODO: Change the autogenerated stub
    }

    public function obter_informacoes_basicas($id)
    {
        $this->db->select('id_corretor, nome, email, nivel, creci, bloqueado');
        return $this->obter($id, 'id_corretor');
    }

    public function listar_todos()
    {
        $this->db
            ->select('id_corretor, nome, email,celular, nivel, creci')
            ->where('exibir_site', 1)
            ->order_by('nivel', 'ASC')
            ->order_by('nome', 'ASC');

        return parent::listar_todos(); // TODO: Change the autogenerated stub
    }

    public function pelo_email($email)
    {
        $this->db->select('id_corretor, nome, email, nivel, creci');
        return $this->obter($email, 'email');
    }
}

class CorretorDomain
{
    public $id_corretor;
    public $nome;
    public $email;
    public $telefone;
    public $celular;
    public $endereco;
    public $data_nasc;
    public $creci;
    public $cpf;
    public $rg;
    public $senha;
    public $bloqueado;
    public $nivel;
    public $exibir_site;
    public $criado_em;
}