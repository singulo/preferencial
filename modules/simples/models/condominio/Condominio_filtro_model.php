<?php
require_once MODULESPATH . 'simples/core/Base_Model.php';

/**
 * @property CI_DB $db
 */
class Condominio_Filtro_Model extends Base_Model
{
    protected $table = 'tb_condominio';

    public function filtrar(CondominioFiltroDomain $filtro, $pagina = 0, $limite = 16)
    {
        $this->montarCondicoes($filtro);

        $this->db->limit($limite, ($pagina * $limite));

        return $this->db->get($this->table)->result();
    }

    public function filtroTotal(CondominioFiltroDomain $filtro)
    {
        $this->montarCondicoes($filtro);

        return $this->db->count_all_results($this->table);
    }

    public function montarCondicoes(CondominioFiltroDomain $filtro)
    {
        if(is_string($filtro->nome) && strlen($filtro->nome) > 0)
            $this->db->like('nome', $filtro->nome);

        if(!is_null($filtro->status) && (count($filtro->status) > 0))
            $this->db->where_in('status', $filtro->status);

        if(is_numeric($filtro->tipo))
            $this->db->where('tipo', $filtro->tipo);

        if(is_numeric($filtro->finalidade))
            $this->db->where('finalidade', $filtro->finalidade);

        if(is_string($filtro->estado) && strlen($filtro->estado) > 0)
            $this->db->where('estado', $filtro->estado);

        if(is_string($filtro->bairro) && strlen($filtro->bairro) > 0)
            $this->db->where('bairro', $filtro->bairro);

        if(!is_null($filtro->cidade))
        {
            $this->db->group_start();

            if(is_array($filtro->cidade))
            {
                foreach ($filtro->cidade as $cidadeBairro)
                {
                    $cidadeBairroSeparados = explode('_', $cidadeBairro);

                    $this->db->or_group_start()
                        ->where('cidade', $cidadeBairroSeparados[0])
                        ->where('bairro', $cidadeBairroSeparados[1])
                        ->group_end();
                }
            }
            else
            {
                $cidadeBairroSeparados = explode('_', $filtro->cidade);

                $this->db->or_group_start()
                    ->where('cidade', $cidadeBairroSeparados[0])
                    ->where('bairro', $cidadeBairroSeparados[1])
                    ->group_end();
            }

            $this->db->group_end();
        }
    }

    public function montarFiltro($dados = array())
    {
        $filtro = new CondominioFiltroDomain();

        foreach($dados as $campo => $valor)
        {
            if(property_exists($filtro, $campo))
                $filtro->$campo = $valor;
        }

        return $filtro;
    }
}

class CondominioFiltroDomain
{
    public $nome;
    public $status;
    public $tipo;
    public $finalidade;
    public $estado;
    public $cidade;
    public $bairro;
}