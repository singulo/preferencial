<?php
require_once MODULESPATH . 'simples/core/Base_Model.php';

class Cidades_Model extends Base_Model
{
    protected $table = 'tb_cidade';

    public function obter_cidades_por_uf($uf)
    {
        return $this->db
            ->where('uf', $uf)
            ->get($this->table)->result();
    }
}

class CidadeDomain
{
    public $id;
    public $codigo;
    public $nome;
    public $uf;

}