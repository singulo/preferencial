<?php
require_once MODULESPATH . 'simples/core/Base_Model.php';

class Notificacao_Model extends Base_Model {

	public $id;
	public $tipo;
	public $nome;
	public $email;
	public $telefone;
	public $criado_em;
	public $respondido_em;
	public $assunto;
	public $id_corretor;

	protected $table = 'view_notificacoes';
	protected $tb_notificacao_corretor = 'tb_corretor_notificacao';

	public function cliente_vinculado($id_corretor_enviando, $id_corretor_para, $id_cliente = NULL, $mensagem = 'Um cliente foi vinculado a você.')
	{
		$notificacao = new NotificacaoCorretorDomain();
		$notificacao->id_corretor_enviou    = $id_corretor_enviando;
		$notificacao->id_corretor_para      = $id_corretor_para;
		$notificacao->id_cliente            = $id_cliente;
		$notificacao->tipo                  = 'cliente_vinculado';
		$notificacao->mensagem              = $mensagem;
		$notificacao->enviado_em            = date('Y-m-d H:i:s');

		$this->db->insert($this->tb_notificacao_corretor, $notificacao);
		return $this->db->insert_id();
	}

	public function proposta_lida($id_proposta, $id_corretor, $id_cliente, $mensagem = 'Uma de suas propostas enviadas acaba de ser lida.')
	{
		$notificacao = new NotificacaoCorretorDomain();
		$notificacao->id_corretor_para      = $id_corretor;
		$notificacao->id_cliente            = $id_cliente;
		$notificacao->id_proposta           = $id_proposta;
		$notificacao->tipo                  = 'proposta_lida';
		$notificacao->mensagem              = $mensagem;
		$notificacao->enviado_em            = date('Y-m-d H:i:s');

		$this->db->insert($this->tb_notificacao_corretor, $notificacao);
		return $this->db->insert_id();
	}
}

class NotificacaoCorretorDomain
{
	public $id;
	public $id_corretor_enviou;
	public $id_corretor_para;
	public $id_cliente;
	public $mensagem;
	public $tipo;
	public $enviado_em;
}