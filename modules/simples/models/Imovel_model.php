<?php
require_once MODULESPATH . 'simples/core/Empreendimento_model.php';

class Imovel_Model extends Empreendimento_Model
{
    protected $table = 'tb_imovel_site';
    protected $tb_tipo = 'tb_imovel_tipo';
    protected $tb_foto = 'tb_imovel_foto';
    public $tb_foto_foreign_key = 'id_imovel';
    public $tb_video = 'tb_imovel_video';
    public $tb_video_foreign_key = 'id_imovel';
    protected $tb_empreendimento_complemento = 'tb_imovel_complemento';
    protected $tb_complemento_foreign_key = 'id_imovel';
    protected $tb_condominio = 'tb_condominio';
    protected $tb_condominio_foreign_key = 'id_condominio';
    protected $tb_condominio_fotos = 'tb_condominio_foto';
    protected $tb_condominio_fotos_foreign_key = 'id_condominio';
    protected $tb_condominio_videos = 'tb_condominio_video';
    protected $tb_condominio_videos_foreign_key = 'id_condominio';

    protected $filtro_sql_condition = array(
        'id_tipo'       => 'where_in',
        'id_situacao_agenciadora' => 'where_in'
//        'id_condominio' => 'where',
    );

    protected $finalidade_para_campo = array(
        '1' => array('campo_finalidade' => 'finalidade_venda', 'campo_valor_finalidade' => 'valor'),
        '2' => array('campo_finalidade' => 'finalidade_aluguel', 'campo_valor_finalidade' => 'valor_aluguel_mensal'),
        '3' => array('campo_finalidade' => 'finalidade_temporada', 'campo_valor_finalidade' => 'valor_aluguel_diario'),
    );

    public function todos_complementos()
    {
        return $this->db
            ->order_by('tipo')
            ->order_by('complemento')
            ->get($this->tb_complemento)
            ->result();
    }

    public function cidades_imoveis()
    {
        return $this->db->query('SELECT distinct(cidade) FROM ' . $this->table . ' ORDER BY cidade ASC')->result();
    }

    public function tipos()
    {
        return $this->db
            ->select('id, tipo, mostrar_site')
            ->order_by('tipo')
            ->get($this->tb_tipo)
            ->result();
    }

    public function pesquisar(array $filtro, $pagina, $limite, $order_by = NULL)
    {
        $this->db->select("$this->table.*");

        $this->monta_filtro($filtro);

        if( ! is_null($order_by))
            $this->db->order_by($order_by);

        return $this->db->limit($limite, ($pagina * $limite))->get($this->table)->result();
    }

    public function pesquisar_total_resultados(array $filtro)
    {
        $this->monta_filtro($filtro);

        return $this->db->from($this->table)->count_all_results();
    }

    protected function monta_filtro(array $filtro)
    {
        $valoresCond = $this->monta_condicao_valores_filtro($filtro);

        if( isset($filtro['cidade']) && (SiteFilial::configs()->filtro_cidade_e_bairros))
        {
            $this->db->group_start();

            if(is_array($filtro['cidade']))
            {
                foreach ($filtro['cidade'] as $cidadeBairro)
                {
                    $cidadeBairroSeparados = explode('_', $cidadeBairro);

                    $this->db->or_group_start()
                        ->where('cidade', $cidadeBairroSeparados[0])
                        ->where('bairro', $cidadeBairroSeparados[1])
                        ->group_end();
                }
            }
            else
            {
                $cidadeBairroSeparados = explode('_', $filtro['cidade']);

                $this->db->or_group_start()
                    ->where('cidade', $cidadeBairroSeparados[0])
                    ->where('bairro', $cidadeBairroSeparados[1])
                    ->group_end();
            }

            $this->db->group_end();

            unset($filtro['cidade']);
        }
        if(isset($filtro['metragem_min']) && $filtro['metragem_min'] > 0)
            $valoresCond['area_total >='] = (int)$filtro['metragem_min'];
        if(isset($filtro['metragem_max']) && $filtro['metragem_max'] > 0 && $filtro['metragem_max'] != 300)
            $valoresCond['area_total <='] = (int)$filtro['metragem_max'];

        unset($filtro['preco_min']);
        unset($filtro['preco_max']);
        unset($filtro['metragem_min']);
        unset($filtro['metragem_max']);

        foreach($filtro as $key => $value)
        {
            if($value == '' || is_null($value))
                unset($filtro[$key]);
        }

        foreach ($filtro as $key => $val)
            if (is_numeric($key)) // only numbers, a point and an `e` like in 1.1e10
                unset($filtro[$key]);

        if(isset($filtro['complementos']))
        {
            if(is_array($filtro['complementos']))
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento IN (" . implode(', ', $filtro['complementos']) . ")");
            else
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento = " . $filtro['complementos']);

            unset($filtro['complementos']);
        }

        if(count($valoresCond) > 0)
            $this->db->where($valoresCond);

        $pesqConfigs = $this->config->item('pesquisa');

        if(SiteFilial::configs()->usar_codigo_referencia_imovel === TRUE && isset($filtro['id']))
        {
            if(SiteFilial::configs()->ao_usar_codigo_referencia_imovel_pesquisar_pelos_2_campos === TRUE)
            {
                $this->db->group_start();
                $this->db->or_where('id', $filtro['id']);
                $this->db->or_where('codigo_referencia', $filtro['id']);
                $this->db->group_end();
            }
            else
                $this->db->where('codigo_referencia', $filtro['id']);

            unset($filtro['id']);
        }

        foreach ($filtro as $key => $value)
        {
            if(isset($this->filtro_sql_condition[$key]))
            {
                $function = $this->filtro_sql_condition[$key];
                $this->db->$function($key, $value);
            }
            else if(is_array($value))
                $this->db->where_in($key, $value);
            else if(isset($pesqConfigs[$key]) && $pesqConfigs[$key] == $value)
                $this->db->where($key . ' >=', $value);
            else
                $this->db->like($key, $value);
        }
    }

    protected function monta_condicao_valores_filtro($filtro)
    {
        $valoresCond = array();

        $utilizarRegrasFinalidade = isset($filtro['finalidade']) && isset(SiteFilial::configs()->numero_maximo_valores_no_filtro[$filtro['finalidade']]) ?
            $filtro['finalidade'] : Finalidades::Venda;

        if(isset($filtro['preco_min']) && $filtro['preco_min'] > SiteFilial::configs()->numero_maximo_valores_no_filtro[$utilizarRegrasFinalidade]['min'])
            $valoresCond['valor >='] = (int)$filtro['preco_min'];
        if(isset($filtro['preco_max']) && $filtro['preco_max'] > 0 && $filtro['preco_max'] < SiteFilial::configs()->numero_maximo_valores_no_filtro[$utilizarRegrasFinalidade]['max'])
            $valoresCond['valor <='] = (int)$filtro['preco_max'];

        return $valoresCond;
    }

    /**
     * @param $imovel
     * @param $limit
     * @return mixed
     */
    public function similares($imovel, array $finalidades, $limit)
    {
        $id_ignorados = [];

        $similares = $this->buscar_similares($imovel, $finalidades, $limit);

        if($limit - (count($similares)) > 0)
        {
            foreach( $similares as $imovel_similar)
            {
                $id_ignorados[] = $imovel_similar->id;
            }

            $similares = array_merge($similares, $this->buscar_similares($imovel, $finalidades, $limit - count($similares), false, $id_ignorados ));
        }

        return $similares;
    }

    protected function valores_similares($imovel_valor, $porcentagem)
    {
        $valor = (($imovel_valor * $porcentagem) / 100);

        $valor_maximo = $imovel_valor + $valor;
        $valor_minimo = $imovel_valor - $valor;

        if($imovel_valor > 0)
        {
            $this->db->where('valor <=', $valor_maximo);

            if($valor_minimo > 0)
            {
                $this->db->where('valor >=', $valor_minimo);
            }
        }
    }

    public function pelo_condominio($id_condominio)
    {
        return $this->db
            ->where('id_condominio', $id_condominio)
            ->get($this->table)
            ->result();
    }

    public function destaques_com_lat_lng($limite = 10, $rand = true, $tipos = array(), $complementos = array())
    {
        $this->db->where('lat_lng !=', '');

        return $this->destaques($limite, $rand, $tipos, $complementos);
    }


    public function buscar_similares($imovel, array $finalidades, $limit, $do_condominio = true, $id_ignorados = [])
    {
        $id_ignorados[] = $imovel->id;

        $this->db
            ->order_by('RAND()')
            ->where_not_in('id', $id_ignorados);

        if($do_condominio && $imovel->id_condominio > 0 )
            $this->db->where('id_condominio', $imovel->id_condominio);

        foreach($finalidades as $finalidade)
        {
            $this->db->where('finalidade', $finalidade);

            if(isset(SiteFilial::configs()->numero_maximo_valores_no_filtro[$finalidade]))
            {
                $campo_valor_finalidade = $this->finalidade_para_campo[$finalidade]['campo_valor_finalidade'];
                $this->valores_similares($imovel->$campo_valor_finalidade, SiteFilial::configs()->numero_maximo_valores_no_filtro[$finalidade]['similares_porcentagem']);
            }
        }

        return $this->db
            ->limit($limit)
            ->get($this->table)
            ->result();
    }

    public function bairros_imoveis()
    {
        return $this->db->query('SELECT distinct(bairro), cidade FROM ' . $this->table . ' ORDER BY bairro ASC')->result();
    }

    public function pelo_seo_link($link)
    {
        $imovel = $this->obter($link, 'seo_link');
        return $imovel;
    }

    public function pelo_codigo($id, $foto_principal = TRUE)
    {
        $this->table = 'view_imovel';
        $data = parent::pelo_codigo($id, TRUE);
        $this->table = 'tb_imovel_site';

        return $data;
    }

    public function pelos_codigos(array $ids)
    {
        $this->table = 'tb_imovel';

        $data = $this->db
            ->where_in('id', $ids)
            ->get($this->table)
            ->result();

        $this->table = 'tb_imovel_site';

        return $data;
    }
    public function fotos_condominio_pelo_imovel($id)
    {
        $this->db
            ->where('mostrar_site', 1)
            ->order_by("destaque DESC, ordem ASC");
        return $this->midias_condominio_pelo_imovel($id, $this->tb_condominio_fotos, $this->tb_condominio_fotos_foreign_key);
    }

    public function videos_condominio_pelo_imovel($id)
    {
        return $this->midias_condominio_pelo_imovel($id, $this->tb_condominio_videos, $this->tb_condominio_videos_foreign_key);
    }

    protected function midias_condominio_pelo_imovel($id, $tb_midia, $tb_midia_foreign_key)
    {
        return $this->db
            ->where("$tb_midia_foreign_key = (SELECT $this->table.$this->tb_condominio_foreign_key FROM $this->table WHERE id = $id LIMIT 1)", NULL, FALSE)
            ->get($tb_midia)
            ->result();
    }

    public function obter($id, $campo = 'id', $table = NULL)
    {
        return parent::obter($id, $campo, $table);
    }
}