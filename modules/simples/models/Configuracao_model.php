<?php
require_once MODULESPATH . 'simples/core/Base_Model.php';

class Configuracao_Model extends Base_Model
{
    protected $table = 'tb_configuracao';

    public $email;

    public function __construct()
    {
        parent::__construct();

        $this->email = new ConfigEmail();
    }
}

class ConfigEmail extends Base_Model
{
    protected $table                    = 'tb_configuracao';

    protected $chave_estruturas_html    = 'email_html';
    protected $chave_envio_email_smtp   = 'envio_email_smtp';

    public function __construct()
    {
        parent::__construct();
    }

    public function estruturasHtml()
    {
        $email_html = parent::obter($this->chave_estruturas_html, 'chave', $this->table);

        if( ! is_null($email_html))
            return json_decode($email_html->valor);

        return NULL;
    }

    public function smtp()
    {
        $email_smtp = parent::obter($this->chave_envio_email_smtp, 'chave', $this->table);

        if( ! is_null($email_smtp))
            return json_decode($email_smtp->valor);

        return NULL;
    }
}