<?php
/* MOBILE */
function eh_mobile()
{
    if(isset($_GET['mobile']) && $_GET['mobile'] == 1)
        return true;

    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

/* URL */
function base_url_filial($acrescentar = '', $setar_filial = true)
{
    if(! $setar_filial)
        return base_url($acrescentar);

    $filial_acrescentar = 'filial=' . $GLOBALS[FILIAL_CHAVE];

    if(strpos($acrescentar, '?') !== false)
        $filial_acrescentar = $acrescentar . '&' . $filial_acrescentar;
    else
        $filial_acrescentar = $acrescentar . '?' . $filial_acrescentar;

    return base_url($filial_acrescentar);
}

function imovel_url($imovel, $finalidade_clique_miniatura = TRUE)
{
    $link = '';

    $link = base_url_filial(( ! is_null($imovel->seo_link) && strlen($imovel->seo_link) > 0) ?
        $imovel->seo_link : 'imovel?id=' . $imovel->id);

    if($finalidade_clique_miniatura)
        $link .= '&finalidade_clique_miniatura=' . $imovel->finalidade;

    return $link;
}

function imovel_filial_id($imovel, $prefixId = 'cód:', $prefixCodRef = 'c.ref:')
{
    if(SiteFilial::configs()->usar_codigo_referencia_imovel === TRUE && ! is_null($imovel->codigo_referencia) && $imovel->codigo_referencia != '')
        return $prefixCodRef . ' ' . $imovel->codigo_referencia;
    else
        return $prefixId . ' ' . $imovel->id;
}

function obter_objeto_padrao_filtro()
{
    $CI = get_instance();

    $filtro = new stdClass();

    $filtro->id = "";
    $filtro->preco_min = SiteFilial::configs()->numero_maximo_valores_no_filtro[Finalidades::Venda]['min'];
    $filtro->preco_max = SiteFilial::configs()->numero_maximo_valores_no_filtro[Finalidades::Venda]['max'];
    $filtro->metragem_min = 0;
    $filtro->metragem_max = 300;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->finalidade = array();
    $filtro->dormitorios = NULL;
    $filtro->garagem = NULL;
    $filtro->banheiros = NULL;
    $filtro->suites = NULL;
    $filtro->id_situacao_agenciadora = array();
    return $filtro;
}

function obter_objeto_config_filtro()
{
    $config = new stdClass();

    $config->dormitorios    = SiteFilial::configs()->numero_maximo_dormitorios_no_filtro;
    $config->suites         = SiteFilial::configs()->numero_maximo_suites_no_filtro;
    $config->vagas          = SiteFilial::configs()->numero_maximo_vagas_no_filtro;
    $config->banheiros      = SiteFilial::configs()->numero_maximo_banheiros_no_filtro;
    $config->valores        = SiteFilial::configs()->numero_maximo_valores_no_filtro;

    return $config;
}

/* TEXTO */
function texto_para_plural_se_necessario($valor, $palavra, $plural = 's')
{
    if($valor > 1)
        $palavra .= $plural;

    return $palavra;
}

function apenas_numeros_na_string($string)
{
    return preg_replace('/[^0-9]/', '', $string);
}

function saudacao()
{
    $hr = date('H');
    if ($hr >= 0 && $hr < 12)
        return 'Bom dia';
    else if($hr >= 12 && $hr < 18)
        return "Boa tarde";
    else
        return 'Boa noite';
}

function remover_caracteres_especiais($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
        '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}


/* VALORES */
function formatar_valor($valor, $cifrao = FALSE)
{
    if($valor == 'Consulte')
        return $valor;
    else
    {
        if(strpos($valor, ',') === FALSE)
            $valor = number_format($valor, 2, ',', '.');

        if($cifrao != FALSE)
            $valor = $cifrao . $valor;

        return $valor;
    }
}

/* MAPA */
function montar_div_mapa($id, $latLng, array $configs = array(), $classCss = '')
{
    $_configs = obter_objeto_padrao_config_mapa();
    foreach ($configs as $chave => $valor)
    {
        if(isset($_configs[$chave]))
            $_configs[$chave] = $valor;
    }

    $datas = [];
    foreach ($_configs as $chave => $valor)
    {
        $datas[] = 'data-' . $chave . '="' . trim(var_export($valor, 1), "'") . '"';
    }

    return '<div id="' . $id . '" class="' . $classCss . '" data-lat-lng="' . $latLng . '" ' . implode(' ', $datas) . '></div>';
}

function obter_objeto_padrao_config_mapa()
{
    return array(
        'nenhum-marcador'               => FALSE,
        'raio-proximidade-mostrar'      => FALSE,
        'raio-proximidade-tamanho'      => 300,
        'raio-proximidade-cor'          => '#e55050',
        'raio-proximidade-cor-borda'    => '#FF0000',
        'embaralhar-lat-lng'            => FALSE,
        'zoom'                          => 16,
        'centro'                        => FALSE,
        'ao-carregar-pagina-chamar'     => ''
    );
}

/* ENDEREÇO */
function endereco_obter_estados_brasil()
{
    $estados['ac'] = 'Acre' ;
    $estados['al'] = 'Alagoas' ;
    $estados['am'] = 'Amazonas' ;
    $estados['ap'] = 'Amapá' ;
    $estados['ba'] = 'Bahia' ;
    $estados['ce'] = 'Ceará' ;
    $estados['df'] = 'Distrito Federal' ;
    $estados['es'] = 'Espírito Santo' ;
    $estados['go'] = 'Goiás' ;
    $estados['ma'] = 'Maranhão' ;
    $estados['mt'] = 'Mato Grosso' ;
    $estados['ms'] = 'Mato Grosso do Sul' ;
    $estados['mg'] = 'Minas Gerais' ;
    $estados['pa'] = 'Pará' ;
    $estados['pb'] = 'Paraíba' ;
    $estados['pr'] = 'Paraná' ;
    $estados['pe'] = 'Pernambuco' ;
    $estados['pi'] = 'Piauí' ;
    $estados['rj'] = 'Rio de Janeiro' ;
    $estados['rn'] = 'Rio Grande do Norte' ;
    $estados['rs'] = 'Rio Grande do Sul' ;
    $estados['ro'] = 'Rondônia' ;
    $estados['rr'] = 'Roraima' ;
    $estados['sc'] = 'Santa Catarina' ;
    $estados['se'] = 'Sergipe' ;
    $estados['sp'] = 'São Paulo' ;
    $estados['to'] = 'Tocantins' ;

    return $estados;
}

/* FORM */
function form_set_value($obj, $property, $default = '')
{
    return (isset($obj->$property)) ? $obj->$property : $default;
}

function select_value($value, $in_array_or_value)
{
    return ((is_array($in_array_or_value) && in_array($value, $in_array_or_value)) || ($value == $in_array_or_value));
}

/* HTML */
function html_titulo_elemento_filial($pagina = '', $tagTitle = TRUE)
{
    $CI = get_instance();

    $titulo = '';
    if($pagina != '')
        $titulo = $pagina . ' - ' . $GLOBALS[FILIAL_NOME];
    else if(isset($GLOBALS[CONFIG_SITE_PAGINA_TITULOS][$CI->router->fetch_class() . '/' . $CI->router->fetch_method()]))
        $titulo = $GLOBALS[CONFIG_SITE_PAGINA_TITULOS][$CI->router->fetch_class() . '/' . $CI->router->fetch_method()] . ' - ' . $GLOBALS[FILIAL_NOME];
    else if(isset($GLOBALS[FILIAL_HTML_ELEMENTO_TITULO_PERSONALIZADO]))
        $titulo = $GLOBALS[FILIAL_HTML_ELEMENTO_TITULO_PERSONALIZADO];
    else
        $titulo = $GLOBALS[FILIAL_NOME];

    if($tagTitle === TRUE)
        return '<title>' . $titulo . '</title>';

    return $titulo;
}

function html_meta_tags_filial($descricao = '')
{
    $metas = [];

    if($descricao != '')
        $metas['description'] = $descricao;
    else if(isset($GLOBALS[FILIAL_HTML_META_TAG_DESCRICAO]) && strlen($GLOBALS[FILIAL_HTML_META_TAG_DESCRICAO]) > 0)
        $metas['description'] = $GLOBALS[FILIAL_HTML_META_TAG_DESCRICAO];

    $meta_tags = '';
    foreach ($metas as $propriedade => $conteudo)
        $meta_tags .= '<meta name="' . $propriedade . '" content="' . $conteudo . '">';

    $meta_tags .= '<meta charset="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"><meta name="viewport" content="width=device-width, user-scalable=no" />';

    return $meta_tags;
}

function html_meta_tags_facebook_filial()
{
    $metas = [];
    $metas['og:image']              = $GLOBALS[CONFIG_IMG_IMOBILIARIA_LOGO_FACEBOOK];
    $metas['og:locale']             = 'pt_BR';
    $metas['og:url']                = base_url();
    $metas['og:site_name']          = $GLOBALS[FILIAL_NOME];
    $metas['og:type']               = 'website';

    if(isset($GLOBALS[FILIAL_HTML_META_TAG_FACEBOOK_TITULO]) && strlen($GLOBALS[FILIAL_HTML_META_TAG_FACEBOOK_TITULO]) > 0)
        $metas['og:title']          = html_titulo_elemento_filial('', FALSE);
    else
        $metas['og:title']          = $GLOBALS[FILIAL_NOME];

    if(isset($GLOBALS[FILIAL_HTML_META_TAG_FACEBOOK_DESCRICAO]) && strlen($GLOBALS[FILIAL_HTML_META_TAG_FACEBOOK_DESCRICAO]) > 0)
        $metas['og:description']    = $GLOBALS[FILIAL_HTML_META_TAG_FACEBOOK_DESCRICAO];
    else
        $metas['og:description']    = $GLOBALS[FILIAL_NOME];

    if(facebook_api_habilitada())
        $metas['fb:app_id']      = $GLOBALS[FILIAL_FACEBOOK_ID_API];

    $meta_tags = '';
    foreach ($metas as $propriedade => $conteudo)
        $meta_tags .= '<meta property="' . $propriedade . '" content="' . $conteudo . '">';

    return $meta_tags;
}

function html_titulo_elemento_imovel($imovel, $nomeDaFilialNoFinal = TRUE, $tagTitle = TRUE)
{
    $titulo = SiteFilial::imovelTipo($imovel->id_tipo) . ' ' . ($imovel->dormitorios > 0 ? $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório') : '') . ' ' . $imovel->cidade . ($nomeDaFilialNoFinal ? ' - ' . $GLOBALS[FILIAL_NOME] : '');

    if($tagTitle === TRUE)
        return '<title>' . $titulo . '</title>';

    return $titulo;
}

function html_meta_tags_imovel($imovel)
{
    return html_meta_tags_filial(substr($imovel->descricao,0,152) . "...");
}

function html_meta_tags_facebook_imovel($imovel)
{
    $metas = [];
    $metas['og:title']          = html_titulo_elemento_imovel($imovel,TRUE, FALSE);
    $metas['og:site_name']      = $GLOBALS[FILIAL_NOME];
    $metas['og:image']          = isset($imovel->fotos_destaque) ? $imovel->fotos_destaque : $GLOBALS[CONFIG_IMG_IMOVEL_SEM_FOTO];
    $metas['og:description']    = substr($imovel->descricao,0,152) . "...";
    $metas['og:locale']         = 'pt_BR';
    $metas['og:url']            = imovel_url($imovel);
    $metas['og:type']           = 'website';

    if(facebook_api_habilitada())
        $metas['fb:app_id']  = $GLOBALS[FILIAL_FACEBOOK_ID_API];

    $meta_tags = '';
    foreach ($metas as $propriedade => $conteudo)
        $meta_tags .= '<meta property="' . $propriedade . '" content="' . $conteudo . '">';

    return $meta_tags;
}

function html_titulo_elemento_condominio($condominio, $nomeDaFilialNoFinal = TRUE, $tagTitle = TRUE)
{
    $titulo = $condominio->nome . ' - ' . $condominio->cidade . '/' . $condominio->estado . ($nomeDaFilialNoFinal ? ' | ' . $GLOBALS[FILIAL_NOME] : '');

    if($tagTitle === TRUE)
        return '<title>' . $titulo . '</title>';

    return $titulo;
}

function html_meta_tags_condominio($condominio)
{
    return html_meta_tags_filial(substr($condominio->descricao,0,152) . "...");
}

function html_meta_tags_facebook_condominio($condominio)
{
    $metas = [];
    $metas['og:title']          = html_titulo_elemento_condominio($condominio, TRUE, FALSE);
    $metas['og:site_name']      = $GLOBALS[FILIAL_NOME];
    if(isset($condominio->dados_orulo))
    {
        $metas['og:image']          = $condominio->dados_orulo->empresa_agenciadora_fotos[0];
        $metas['og:image:width']    =  '200px';
        $metas['og:image:height']    = '200px';
    }
    else
        $metas['og:image']          = isset($condominio->fotos_principais[0]->arquivo) ? $GLOBALS[FILIAL_FOTOS_CONDOMINIOS_CAMINHO_EXTERNO] . $condominio->fotos_principais[0]->arquivo : $GLOBALS[CONFIG_IMG_CONDOMINIO_SEM_FOTO];

    $metas['og:description']    = substr($condominio->descricao,0,152) . "...";
    $metas['og:locale']         = 'pt_BR';
    $metas['og:type']           = 'website';

    if(facebook_api_habilitada())
        $metas['fb:app_id']  = $GLOBALS[FILIAL_FACEBOOK_ID_API];

    $meta_tags = '';
    foreach ($metas as $propriedade => $conteudo)
        $meta_tags .= '<meta property="' . $propriedade . '" content="' . $conteudo . '">';

    return $meta_tags;
}

/* FB */
function facebook_api_habilitada()
{
    return isset($GLOBALS[FILIAL_FACEBOOK_ID_API]) && strlen($GLOBALS[FILIAL_FACEBOOK_ID_API]) > 0;
}

/* GOOGLE */
function google_people_api_habilitada()
{
    return isset($GLOBALS[FILIAL_GOOGLE_CLIENT_ID]) && strlen($GLOBALS[FILIAL_GOOGLE_CLIENT_ID]) > 0;
}