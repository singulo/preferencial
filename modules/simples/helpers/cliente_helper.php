<?php

function registra_cliente(array $dados, $prefix = '', &$cliente_id = 0, &$msg_erro = '')
{
    
    $CI = get_instance();

    if( ! is_null($CI->clientes_model->pelo_email($dados['email'])))
    {
        $msg_erro = 'O e-mail "' . $dados['email'] . '" já cadastrado.';
        return FALSE;
    }

    $corretor = obter_corretor_cliente();

    require_once(MODULESPATH . 'simples/libraries/RequestMapper.php');

    $mapper = array(
        'cadastrado_em'         => array('default_value' => date("Y-m-d H:i:s")),
        'email_alternativo'     => array('default_value' => NULL),
        'id_corretor'           => array('default_value' => is_null($corretor) ? NULL : $corretor->id_corretor),
        'id_facebook'           => array('default_value' => NULL),
        'id_google'             => array('default_value' => NULL),
        'link_google'           => array('default_value' => NULL),
        'cidade'                => array('default_value' => ''),
        'bloqueado'             => array('default_value' => 0),
        'excluido'              => array('default_value' => 0),
        'atualizado_em'         => array('default_value' => date("Y-m-d H:i:s")),
        'atualizado_por'        => array('default_value' => $dados['email']));

    $CI->load->model('simples/clientes_model');
    $cliente = RequestMapper::parseToObject($dados, $mapper, new ClienteDomain(), $prefix);

    $cliente->id_origem = SiteFilial::configs()->id_origem_cadastro_site;
    $cliente->id = $CI->clientes_model->inserir($cliente);

    //ENVIAR EMAIl SE NOVO CADASTRO DE CLIENTE REGISTRADO COM SUCESSO
    if($cliente->id > 0)
    {

        if((isset($dados['id_google']) || isset($dados['id_facebook'])) && isset($dados['foto']))
        {
            if(! file_exists($GLOBALS[FILIAL_SITE_FOTO_CLIENTE_INTERNO] .  $cliente->id . '.jpg'))
            {
                $foto_cliente = (isset($dados['id_google'])) ?  file_get_contents(str_replace('=50','=100',$dados['foto'])) : file_get_contents($dados['foto']);

                $caminho_foto_cliente_interno = $GLOBALS[FILIAL_SITE_FOTO_CLIENTE_INTERNO] .  $cliente->id . '.jpg';

                file_put_contents($caminho_foto_cliente_interno, $foto_cliente);
            }
        }


        require_once MODULESPATH . 'simples/libraries/EmailSimples.php';

        $emailSimples = new EmailSimples();

        //EMAIL DE BOAS-VINDAS
        $emailCliente           = new EmailSimplesDomain($cliente->email, $cliente->nome . ' seja bem-vindo!', $corretor->id_corretor);
        $emailCliente->corpo    = $emailSimples->corpoBoasVindasParaCliente($cliente->nome);

        $emailSimples->enviar($emailCliente);

        //EMAIL DE NOVO CADASTRO
        if( ! is_null($corretor))
        {
            $emailCorretor          = new EmailSimplesDomain($corretor->email, $cliente->nome . ' acaba de se CADASTRAR!', $corretor->id_corretor);
            $emailCorretor->corpo   = $emailSimples->corpoClienteCadastradoParaCorretorVinculado($cliente->id, $cliente);

            $emailSimples->enviar($emailCorretor);

            $CI->load->model('simples/notificacao_model');
            $CI->notificacao_model->cliente_vinculado(0, $corretor->id_corretor, $cliente->id);
        }

        $cliente_id = $cliente->id;

        return TRUE;
    }

    return FALSE;
}

function logar_cliente_enviou_contato_se_necessario_registrar($email, $nome, $telefone_1, $filial, &$cliente_id = 0, &$msg_erro = '')
{
    $CI = get_instance();
    $clientes = obter_registros_cliente_nas_filiais_pelo_email($email);
    if( ! isset($clientes[$filial]))
    {
        $cliente['nome']            = $nome;
        $cliente['email']           = $email;
        $cliente['telefone_1']      = $telefone_1;
        $cliente['ultimo_acesso']   = date('Y-m-d H:i:s');
        if(registra_cliente($cliente, '', $cliente_id, $msg_erro))
        {
            $clientes[$filial] = obter_registros_cliente_na_filial_pelo_email($CI, $email, $filial);
        }
    }

    seta_usuario_em_sessao_filiais($CI, $clientes, $filial);
}

function seta_usuario_em_sessao_filiais($CI, $clientes, $filial_atual, $registrar_imoveis_visualizados_sem_login = TRUE, $registrar_acesso = TRUE)
{
    if( ! isset($clientes[$filial_atual]->ultimo_acesso_registrado) || ! $clientes[$filial_atual]->ultimo_acesso_registrado )
        $clientes[$filial_atual]->ultimo_acesso_registrado = registrar_acesso($clientes[$filial_atual]->id) > 0;

    $CI->session->set_userdata('cliente_filiais', $clientes);
    $CI->session->set_userdata('usuario', $clientes[$filial_atual]);
    setcookie('email_usado_para_login', $clientes[$filial_atual]->email, 0, '/');

    if($registrar_imoveis_visualizados_sem_login)
    {
        try // TENTA REGISTRAR ANTIGAS VIZUALIZAÇÕES DO CLIENTE
        {
            $CI->load->model('simples/cliente_imovel_visualizacao_log_model');
            $registros_sem_login = obter_imoveis_visualizado_sem_login();
            if (!is_null($registros_sem_login) && isset($registros_sem_login->$filial_atual))
                $CI->cliente_imovel_visualizacao_log_model->salvarNaoLogado($registros_sem_login->$filial_atual, $clientes[$filial_atual]->id);

            limpar_visualizacao_imovel_para_usuario_nao_logado_na_filial($filial_atual);
        }
        catch(Exception $ex) { }
    }
}

function deslogar_usuario()
{
    $cookie_email_usado_para_login = 'email_usado_para_login';

    $CI = get_instance();
    $CI->session->unset_userdata('usuario');
    $CI->session->unset_userdata('cliente_filiais');

    unset( $_COOKIE[$cookie_email_usado_para_login] );
    setcookie($cookie_email_usado_para_login, "", time()-3600, '/');
}

function atualizar_facebook_cliente($id_cliente, $id_facebook, $filial_chave)
{
    $CI = get_instance();
    $CI->load->model('simples/clientes_model');

    $cliente = new stdClass();
    $cliente->id = $id_cliente;
    $cliente->id_facebook = $id_facebook;

    $CI->clientes_model->set_db($filial_chave);
    return $CI->clientes_model->editar($cliente, TRUE) >= 0;
}

function atualizar_google_cliente($id_cliente, $id_google, $filial_chave)
{
    $CI = get_instance();
    $CI->load->model('simples/clientes_model');

    $cliente = new stdClass();
    $cliente->id = $id_cliente;
    $cliente->id_google = $id_google;

    $CI->clientes_model->set_db($filial_chave);
    return $CI->clientes_model->editar($cliente, TRUE) >= 0;
}

function obter_corretor_cliente($id_corretor = NULL)
{
    $CI = get_instance();
    $CI->load->model('simples/corretores_model');

    if($id_corretor == NULL)//CASO O USUARIO NAO TENHA UM CORRETOR VINCULADO
    {
        $CI->load->model('simples/imobiliaria_model');
        $id_corretor = SiteFilial::configs()->id_corretor_padrao;

        if(is_null($id_corretor))
            return NULL;
    }

    return $CI->corretores_model->obter_informacoes_basicas($id_corretor);
}

function registrar_acesso($id)
{
    $CI = get_instance();
    $CI->load->model('simples/clientes_model');

    $cliente = array('id' => $id, 'ultimo_acesso' => date('Y-m-d H:i:s'));

    return $CI->clientes_model->atualizar($id, $cliente);
}

function registrar_visualizacao_imovel_para_usuario_nao_logado($id_imovel, $finalidade, $filial)
{
    $cookie_imoveis_visualizado_sem_login = 'imoveis_visualizado_sem_login';

    $ultimos_registros = obter_imoveis_visualizado_sem_login();
    if( ! isset($ultimos_registros->$filial))
        $ultimos_registros->$filial = new stdClass();

    $index                                  = $id_imovel . '_' . $finalidade;
    $registro                               = new stdClass();
    $registro->id_imovel                    = $id_imovel;
    $registro->finalidade                   = $finalidade;
    $registro->data_ultima_visualizacao     = date('Y-m-d H:i:s');
    $registro->data_penultima_visualizacao  = isset($ultimos_registros->$filial->$index) ? $ultimos_registros->$filial->$index->data_ultima_visualizacao : NULL;
    $registro->visualizacoes                = isset($ultimos_registros->$filial->$index) ? $ultimos_registros->$filial->$index->visualizacoes + 1 : 1;

    $ultimos_registros->$filial->$index = $registro;

    return setcookie($cookie_imoveis_visualizado_sem_login, json_encode($ultimos_registros), 0, '/');
}

function obter_imoveis_visualizado_sem_login()
{
    $cookie_imoveis_visualizado_sem_login = 'imoveis_visualizado_sem_login';

    $ultimos_registros = new stdClass();

    if(isset($_COOKIE[$cookie_imoveis_visualizado_sem_login]))
    {
        $registros = @json_decode($_COOKIE[$cookie_imoveis_visualizado_sem_login]);

        if( ! is_null($registros))
            $ultimos_registros = $registros;
    }

    return $ultimos_registros;
}

function registrar_visualizacao_unidade_agenciadora_nao_logado($id_unidade, $filial)
{
    $CI = get_instance();
    $CI->load->model('simples/imovel_model');

    $imovel = $CI->imovel_model->obter($id_unidade, 'id_unidade_agenciadora');

    $cookie_imoveis_visualizado_sem_login = 'imoveis_visualizado_sem_login';

    $ultimos_registros = obter_imoveis_visualizado_sem_login();
    if( ! isset($ultimos_registros->$filial))
        $ultimos_registros->$filial = new stdClass();

    $index                                  = $imovel->id . '_' . $imovel->finalidade;
    $registro                               = new stdClass();
    $registro->id_imovel                    = $imovel->id;
    $registro->finalidade                   = $imovel->finalidade;
    $registro->data_ultima_visualizacao     = date('Y-m-d H:i:s');
    $registro->data_penultima_visualizacao  = isset($ultimos_registros->$filial->$index) ? $ultimos_registros->$filial->$index->data_ultima_visualizacao : NULL;
    $registro->visualizacoes                = isset($ultimos_registros->$filial->$index) ? $ultimos_registros->$filial->$index->visualizacoes + 1 : 1;

    $ultimos_registros->$filial->$index = $registro;

    return setcookie($cookie_imoveis_visualizado_sem_login, json_encode($ultimos_registros), 0, '/');
}


function registrar_visualizacao_de_cliente_a_unidade_agenciadora($id_cliente, $id_unidade)
{
    $CI = get_instance();
    $CI->load->model('simples/cliente_imovel_visualizacao_log_model');
    $CI->load->model('simples/imovel_model');

    $imovel = $CI->imovel_model->obter($id_unidade, 'id_unidade_agenciadora');

    $registro = new ClienteImovelVisualizacaoLogDomain();
    $registro->id_cliente               = $id_cliente;
    $registro->id_imovel                = $imovel->id;
    $registro->finalidade               = $imovel->finalidade;
    $registro->visualizou_logado        = 1;
    $registro->visualizacoes            = 1;
    $registro->data_ultima_visualizacao = date('Y-m-d H:i:s');

    return $CI->cliente_imovel_visualizacao_log_model->salvar($registro);
}


function limpar_visualizacao_imovel_para_usuario_nao_logado_na_filial($filial)
{
    $cookie_imoveis_visualizado_sem_login = 'imoveis_visualizado_sem_login';

    $registros_sem_login = obter_imoveis_visualizado_sem_login();
    if( ! is_null($registros_sem_login))
    {
        unset($registros_sem_login->$filial);

        if(count(get_object_vars($registros_sem_login)) == 0)
            limpar_visualizacao_imovel_para_usuario_nao_logado();
        else
        {
            $json_registros_sem_login = json_encode($registros_sem_login);
            setcookie($cookie_imoveis_visualizado_sem_login, $json_registros_sem_login, 0, '/');
            $_COOKIE[$cookie_imoveis_visualizado_sem_login] = $json_registros_sem_login;
        }
    }
}

function limpar_visualizacao_imovel_para_usuario_nao_logado()
{
    $cookie_imoveis_visualizado_sem_login = 'imoveis_visualizado_sem_login';
    if(isset($_COOKIE[$cookie_imoveis_visualizado_sem_login]))
    {
        unset( $_COOKIE[$cookie_imoveis_visualizado_sem_login] );
        setcookie($cookie_imoveis_visualizado_sem_login, "", time()-3600, '/');
    }
}

function diminuir_uma_visualizacao_em_imovel($id_imovel, $finalidade, $filial)
{
    $cookie_imoveis_visualizado_sem_login = 'imoveis_visualizado_sem_login';

    $ultimos_registros = obter_imoveis_visualizado_sem_login();
    $index = $id_imovel . '_' . $finalidade;
    if(isset($ultimos_registros->$filial) && isset($ultimos_registros->$filial->$index))
    {
        if(($ultimos_registros->$filial->$index->visualizacoes - 1) > 0)
        {
            $registro                               = new stdClass();
            $registro->id_imovel                    = $id_imovel;
            $registro->finalidade                   = $finalidade;
            $registro->visualizacoes                = $ultimos_registros->$filial->$index->visualizacoes - 1;
            $registro->data_ultima_visualizacao     = $ultimos_registros->$filial->$index->data_penultima_visualizacao;
            $registro->data_penultima_visualizacao  = NULL;

            $ultimos_registros->$filial->$index = $registro;
        }
        else
            unset($ultimos_registros->$filial->$index);

        $json_ultimos_registros = json_encode($ultimos_registros);
        setcookie($cookie_imoveis_visualizado_sem_login, $json_ultimos_registros, 0, '/');
        $_COOKIE[$cookie_imoveis_visualizado_sem_login] = $json_ultimos_registros;
    }
}

function registrar_visualizacao_de_cliente_ao_imovel($id_cliente, $id_imovel, $finalidade)
{
    $CI = get_instance();
    $CI->load->model('simples/cliente_imovel_visualizacao_log_model');
    $registro = new ClienteImovelVisualizacaoLogDomain();
    $registro->id_cliente               = $id_cliente;
    $registro->id_imovel                = $id_imovel;
    $registro->finalidade               = $finalidade;
    $registro->visualizou_logado        = 1;
    $registro->visualizacoes            = 1;
    $registro->data_ultima_visualizacao = date('Y-m-d H:i:s');

    return $CI->cliente_imovel_visualizacao_log_model->salvar($registro);
}

function obter_registros_cliente_nas_filiais_pelo_email($email, array $ignorar_filiais = array())
{
    $CI = get_instance();
    $CI->load->model('simples/clientes_model');

    $clientes = array();
    foreach($CI->config->item('filiais') as $filial)
    {
        if(in_array($filial['chave'], $ignorar_filiais))
            continue;

        $cliente = obter_registros_cliente_na_filial_pelo_email($CI, $email, $filial['chave']);
        if( ! is_null($cliente))
            $clientes[$filial['chave']] = $cliente;
    }

    return $clientes;
}

function obter_registros_cliente_na_filial_pelo_email($CI, $email, $filial)
{
    $CI->load->model('simples/clientes_model');
    $CI->load->model('simples/corretores_model');

    $CI->clientes_model->set_db($filial);
    $CI->corretores_model->set_db($filial);

    $cliente = $CI->clientes_model->pelo_email($email);
    if( ! is_null($cliente))
        $cliente->corretor = $CI->corretores_model->obter_informacoes_basicas($cliente->id_corretor);

    return $cliente;
}