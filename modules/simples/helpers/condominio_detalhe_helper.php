<?php

/**
 *
 */
function exibir_valor_min_max($obj, $propriedade, $cola = 'de %s a %s')
{
    $valores = propriedade_para_array($obj, $propriedade);

    printf($cola, min($valores), max($valores));
}

function obter_valor_min($obj, $propriedade)
{
    return min(propriedade_para_array($obj, $propriedade));
}

function propriedade_para_array($obj, $propriedade)
{
    $valores = array();
    foreach($obj as $ob)
        $valores[] = $ob->$propriedade;

    return $valores;
}