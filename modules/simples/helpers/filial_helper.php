<?php

function armazena_filial_em_sessao()
{
    $CI = get_instance();

    $_SESSION['filial']['chave'] = $GLOBALS[FILIAL_CHAVE];

    $CI->load->model('simples/imobiliaria_model');
    $imobs = $CI->imobiliaria_model->listar(0,1);

    if(isset($imobs[0]))
        $CI->session->set_userdata('filial', ((array)$imobs[0] + (array)$CI->session->userdata('filial')));

    $CI->load->model('simples/imovel_model');
    $CI->load->model('simples/condominio_model');

    //CONDOMÍNIOS
    $_SESSION['filial']['condominios'] = $CI->condominio_model->listar_todos();

    //ARMAZENADO EM
    $_SESSION['filial']['armazenada_em'] = date('Y-m-d H:i:s');

    //PARA ADICIONAR FUNCIONALIDADES ESPECIFICAS DE CADA SITE NECESSÁRIAS AO CARREGAMENTO
    if(file_exists(APPPATH."helpers/carregar_na_filial_helper.php"))
    {
        $CI->load->helper('carregar_na_filial_helper');
        if (function_exists('carregar_na_filial'))
            carregar_na_filial();
    }
}