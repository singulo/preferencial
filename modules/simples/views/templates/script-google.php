<? if(google_people_api_habilitada()) : ?>
    <script async defer src="https://apis.google.com/js/api.js" onload="gapiInit()"></script>
    <script>
        function gapiInit()
        {
            gapi.load('client:auth2', function(){
                gapi.client.init({
                    discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/plus/v1/rest"],
                    clientId: '<?= $GLOBALS[FILIAL_GOOGLE_CLIENT_ID]; ?>',
                    scope: 'profile'
                }).then(function () {
                    $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.GOOGLE_API_INICIADA);
                });
            });
        }
    </script>
<? endif; ?>