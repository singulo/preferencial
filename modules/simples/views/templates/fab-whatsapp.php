<? $numeros_whatsapp = (array)SiteFilial::dadosImobiliaria()->numeros_whatsapp; ?>
<? if(count($numeros_whatsapp) > 0) : ?>
    <? $parametros = []; ?>
    <? if(isset($imovel)) : ?>
        <? $parametros['text'] = 'Oi, gostaria de saber mais informações sobre o imóvel: ' . urlencode(imovel_url($imovel)); ?>
    <? endif; ?>
    <? $link = ''; ?>
    <? foreach($parametros as $parametro => $valor): ?>
        <? $link .= '&' . $parametro . '=' . $valor; ?>
    <? endforeach; ?>

    <? if(count($numeros_whatsapp) == 1): ?>
        <a href="<?= 'https://api.whatsapp.com/send?1=pt_BR&phone=' . apenas_numeros_na_string(array_values($numeros_whatsapp)[0]) . $link; ?>" class="fab-whatsapp" target="_blank">
            <i class="fa fa-whatsapp"></i>
        </a>
    <? else: ?>
        <div class="fab-whatsapp btn-group dropup">
            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-whatsapp"></i>
            </button>
            <ul class="dropdown-menu">
                <li class="titulo">Deseja falar com:</li>
                    <? $_numeros_whatsapp = []; ?>
                    <? foreach($numeros_whatsapp as $setor => $numero): ?>
                        <? $_numeros_whatsapp[] = ['setor' => $setor, 'numero' => $numero]; ?>
                    <? endforeach; ?>

                    <? for($i = 0; $i < count($_numeros_whatsapp); $i++): ?>
                        <li>
                            <a href="<?= 'https://api.whatsapp.com/send?1=pt_BR&phone=' . apenas_numeros_na_string($_numeros_whatsapp[$i]['numero']) . $link; ?>" target="_blank"><b><?= $_numeros_whatsapp[$i]['setor']; ?></b><br><span><?= $_numeros_whatsapp[$i]['numero']; ?></span></a>
                        </li>
                        <? if($i != count($_numeros_whatsapp) - 1) : ?>
                            <li role="separator" class="divider"></li>
                        <? endif; ?>
                    <? endfor; ?>
                </ul>
        </div>
    <? endif; ?>
<? endif; ?>