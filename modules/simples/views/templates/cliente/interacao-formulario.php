<?  $estados = endereco_obter_estados_brasil();?>
<div class="modal fade" id="modal-interacao-formulario" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="interacao-descricao"></p>
                    </div>
                    <span class="mensagem-conclusao-sucesso" style="display: none;"></span>
                    <form id="form-interacao-formulario" onsubmit="SimplesScript.usuario.modais.funcs.salvarInteracao(); return false;">
                        <input type="hidden" name="codigo">
                        <input type="hidden" name="destinatarios">
                        <input type="hidden" name="cadastrar_cliente_automaticamente">
                        <input type="hidden" name="email_assunto">
                        <? $usuario = $this->session->has_userdata('usuario') ? $this->session->userdata('usuario') : NULL; ?>
                        <div class="form-group col-xs-12">
                            <label for="nome">Nome</label>
                            <input class="form-control" type="text" name="nome" placeholder="Nome" required value="<? if( ! is_null($usuario)) echo  $usuario->nome; ?>">
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="email">Email</label>
                            <input class="form-control" type="email" name="email" placeholder="Email" required value="<? if( ! is_null($usuario)) echo  $usuario->email; ?>">
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="telefone">Telefone</label>
                            <input class="form-control telefone" type="text" name="telefone_1" placeholder="Telefone" value="<? if( ! is_null($usuario)) echo  $usuario->telefone_1; ?>">
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="uf">Estado</label>
                            <select name="uf" class="selectpicker" onchange="SimplesScript.funcs.obterCidadesEPreencherSelect($('#form-interacao-formulario'));" data-title="Estado" data-live-search="true" data-width="100%">
                                <? foreach ($estados as $uf => $estado) : ?>
                                    <option value="<?= strtoupper($uf); ?>" <? if( ! is_null($usuario) && strtoupper($usuario->uf) == strtoupper($uf)) echo  "selected";?>><?= $estado; ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="cidade">Cidade</label>
                            <select name="cidade" class="selectpicker" data-live-search="true" disabled data-width="100%" data-title="Cidade"></select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="obs">Observação</label>
                            <textarea name="obs" rows="5" class="form-control" placeholder="Digite aqui sua observação..."></textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="pull-left" style="line-height: 10px;">
                            <label style="font-weight: 100;"><input type="checkbox" onchange="SimplesScript.usuario.modais.funcs.checkboxExibicaoInteracao(this);"> Não mostrar mais</label>
                            <br>
                            <small class="iteracao-codigo pull-left" style="font-size: 10px; color: #ccc;"></small>
                        </div>
                        <button type="button" data-loading-text="Aguarde..." class="btn btn-danger" onclick="SimplesScript.usuario.modais.funcs.salvarInteracao();">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
