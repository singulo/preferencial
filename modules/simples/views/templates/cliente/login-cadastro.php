<!-- Modal -->
<div class="modal fade" id="modal-login-cadastro" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                <div class="col-xs-11 col-md-5">
                    <div class="row">
                        <h4 class="modal-title">Entre</h4>
                    </div>
                </div>
                <div class="hidden-xs hidden-sm col-md-6">
                    <h4 class="modal-title">Cadastre-se</h4>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-5 lado-cadastro">
                        <form id="form-login" onsubmit="ClienteScript.funcs.login(); return false;">
                            <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email" autofocus required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-success btn-login" type="button" onclick="ClienteScript.funcs.login();" data-loading-text="Aguarde..." autocomplete="off">Verificar</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                        <? $possivel_entrar_com_facebook = facebook_api_habilitada(); ?>
                        <? $possivel_entrar_com_google   = google_people_api_habilitada(); ?>
                        <? if($possivel_entrar_com_facebook || $possivel_entrar_com_google) : ?>
                        <div class="login-redes-sociais-container">
                            <div class="row divisoria-login-redes-sociais">
                                <div class="col-xs-5"><hr></div>
                                <div class="col-xs-2"><span class="text-center">ou</span></div>
                                <div class="col-xs-5"><hr></div>
                            </div>
                            <? if($possivel_entrar_com_facebook) : ?>
                                <div class="form-group">
                                    <button class="btn btn-block btn-login-facebook" type="button" onclick="$.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.FB_CLIENTE_CLICK_BOTAO_LOGIN);">
                                        <i class="fa fa-facebook"></i>
                                        <span class="text-center">Entrar com Facebook</span>
                                    </button>
                                </div>
                            <? endif; ?>
                            <? if($possivel_entrar_com_google) : ?>
                                <div class="form-group">
                                    <button class="btn btn-block btn-login-google" type="button" onclick="$.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.GOOGLE_CLIENTE_CLICK_BOTAO_LOGIN);">
                                        <i class="fa fa-google"></i>
                                        <span class="text-center">Entrar com Gmail</span>
                                    </button>
                                </div>
                            <? endif; ?>
                        </div>
                        <? endif; ?>
                    </div>
                    <div class="col-xs-12 col-md-7 lado-cadastro">
                        <?  $estados = endereco_obter_estados_brasil();?>
                        <div class="visible-xs visible-sm">
                            <hr>
                            <div class="form-group">
                                <h4 class="modal-title">Cadastre-se</h4>
                            </div>
                        </div>
                        <div class="row">
                            <form id="form-cadastrar" onsubmit="return false;">
                                <div class="form-group col-xs-12">
                                    <input class="form-control" type="text" name="nome" placeholder="Nome*" required minlength="5">
                                </div>
                                <div class="form-group col-xs-6">
                                    <input class="form-control" type="email" name="email" placeholder="Email*" required>
                                </div>
                                <div class="form-group col-xs-6">
                                    <input class="form-control telefone" type="tel" name="telefone_1" placeholder="Telefone <?= SiteFilial::configs()->formulario_cadastro_telefone_obrigatorio ? '*' : ''; ?>" <?= SiteFilial::configs()->formulario_cadastro_telefone_obrigatorio ? 'required' : ''; ?>>
                                </div>
                                <div class="form-group col-xs-6">
                                    <select name="uf" class="selectpicker" onchange="SimplesScript.funcs.obterCidadesEPreencherSelect($('#form-cadastrar'));" data-live-search="true" title="Estado" data-width="100%">
                                        <? foreach ($estados as $uf => $estado) : ?>
                                            <option value="<?= strtoupper($uf); ?>"><?= $estado; ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-xs-6">
                                    <select name="cidade" class="selectpicker" data-live-search="true" disabled data-width="100%" title="Cidade" depends="function(elem){ return true; }"></select>
                                </div>
                                <div class="col-xs-12">
                                    <button type="button" onclick="ClienteScript.funcs.cadastrar();" data-loading-text="Aguarde..." class="btn btn-success btn-block" autocomplete="off">Cadastrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
