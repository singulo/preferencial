<!-- Modal -->
<div class="modal fade" id="modal-contato-interesse" tabindex="-1" role="dialog" aria-labelledby="contatoInteresseModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?if (isset($imovel)) :?>
                <div class="modal-body" data-rodape-informacao="<?= isset($imovel->id) ? 'Código do imóvel: ' . $imovel->id . (isset($imovel->codigo_referencia) && strlen($imovel->codigo_referencia) > 0 ? ' / ' . $imovel->codigo_referencia : '') : ''; ?>">
            <?elseif (isset($condominio)) :?>
                <div class="modal-body" data-rodape-informacao="<?= isset($condominio->id) ? 'Código do condominio: ' . $condominio->id  : ''; ?>">
            <?endif; ?>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row explicacao">
                                    <h4>O que é isso?</h4>
                                    <p>Por este formulário você pode demonstrar o interesse ao <?= (isset($imovel) ? 'imóvel' : 'condominio')?>, solicitar mais informações específicas ao corretor que está lhe atendendo. Quanto mais você descrever a sua necessidade melhor vamos conseguir informar e te ajudar na negociação.</p>
                                    <b>Não está sendo atendido?</b>
                                    <p>Ao preencher e enviar o formulário a seu interesse/dúvida será direcionado para a pessoa com a maior capacidade de responder ao seu contato.</p>
                                    <hr>
                                    <div class="contatos">
                                        <b>Se preferir:</b>
                                        <?  $telefones = array();
                                            foreach (array('telefone_1', 'telefone_2', 'telefone_3') as $index) :
                                                if(strlen($_SESSION['filial'][$index]) > 0)
                                                    $telefones[] = $_SESSION['filial'][$index];
                                            endforeach;
                                        ?>
                                        <? if(count($telefones) > 0) : ?>
                                            <p class="telefones">
                                                <? foreach ($telefones as $telefone) : ?>
                                                    <a href="tel:<?= $telefone; ?>" target="_blank"><?= $telefone; ?></a>
                                                <? endforeach; ?>
                                            </p>
                                        <? endif; ?>
                                    </div>
                                    <div class="redes-sociais">
                                        <div class="row">
                                            <?  $redes_sociais = array();
                                                foreach (array('facebook', 'instagram', 'twitter') as $index) :
                                                    if(strlen($_SESSION['filial'][$index]) > 0)
                                                        $redes_sociais[$index] = $_SESSION['filial'][$index];
                                                endforeach;
                                                if(strlen($_SESSION['filial']['whatsapp']) > 0)
                                                    $redes_sociais['whatsapp'] = 'https://api.whatsapp.com/send?phone=' . preg_replace('/[^0-9]/', '', $_SESSION['filial']['whatsapp']);
                                            ?>
                                            <?  $quantidade_redes_sociais = count($redes_sociais); ?>
                                            <? if($quantidade_redes_sociais > 0) : ?>
                                                <?  $redes_sociais_md = 12;
                                                    $redes_sociais_xs = 6;
                                                    switch ($quantidade_redes_sociais)
                                                    {
                                                        case 1 :
                                                            $redes_sociais_md = 12; $redes_sociais_xs = 12;
                                                            break;
                                                        case 2 :
                                                            $redes_sociais_md = 6; $redes_sociais_xs = 6;
                                                            break;
                                                        case 3 :
                                                            $redes_sociais_md = 4; $redes_sociais_xs = 4;
                                                            break;
                                                        case 4 :
                                                            $redes_sociais_md = 3; $redes_sociais_xs = 3;
                                                            break;
                                                    }
                                                ?>
                                                <? foreach ($redes_sociais as $index => $link): ?>
                                                    <div class="col-xs-<?= $redes_sociais_xs; ?> col-md-<?= $redes_sociais_md; ?>">
                                                        <a href="<?= $link; ?>" target="_blank"><i class="fa fa-<?= $index; ?>" aria-hidden="true"></i></a>
                                                    </div>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h4>Seus dados</h4>
                                <? $possivel_entrar_com_facebook = facebook_api_habilitada(); ?>
                                <? $possivel_entrar_com_google   = google_people_api_habilitada(); ?>
                                <? if($possivel_entrar_com_facebook || $possivel_entrar_com_google) : ?>
                                    <div class="login-redes-sociais-container">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4">
                                                <p><b>Preencha seus dados usando:</b></p>
                                            </div>
                                            <? if($possivel_entrar_com_facebook) : ?>
                                                <div class="col-xs-6 col-md-4 no-padding-left-md">
                                                    <div class="form-group">
                                                        <button class="btn btn-block btn-login-facebook" type="button" onclick="$.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.FB_CLIENTE_CLICK_BOTAO_LOGIN);">
                                                            <i class="fa fa-facebook"></i>
                                                            <span class="text-center">Facebook</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <? if($possivel_entrar_com_google) : ?>
                                                <div class="col-xs-6 col-md-4 no-padding-left-md">
                                                    <div class="form-group">
                                                        <button class="btn btn-block btn-login-google" type="button" onclick="$.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.GOOGLE_CLIENTE_CLICK_BOTAO_LOGIN);">
                                                            <i class="fa fa-google"></i>
                                                            <span class="text-center">Gmail</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                        <div class="row divisoria-login-redes-sociais">
                                            <div class="col-xs-5"><hr></div>
                                            <div class="col-xs-2"><span class="text-center">ou</span></div>
                                            <div class="col-xs-5"><hr></div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <form id="form-interesse" onsubmit="return false;">
                                    <? $usuario = $this->session->userdata('usuario'); ?>
                                    <?if (isset($imovel)) : ?>
                                        <input type="hidden" name="cod_imovel" value="<?= $imovel->id; ?>">
                                    <?elseif (isset($condominio)) : ?>
                                        <input type="hidden" name="cod_condominio" value="<?= $condominio->id; ?>">
                                    <?endif; ?>

                                    <div class="form-group">
                                        <input type="text" name="nome" class="form-control" placeholder="Nome *" required value="<?= form_set_value($usuario, 'nome'); ?>">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control" placeholder="Email *" required value="<?= form_set_value($usuario, 'email'); ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-5 no-padding-left-md">
                                            <div class="form-group">
                                                <input type="tel" name="telefone" class="form-control telefone" placeholder="Telefone <?= SiteFilial::configs()->formulario_contato_interesse_telefone_obrigatorio ? '*' : ''; ?>" minlength="13" <?= SiteFilial::configs()->formulario_contato_interesse_telefone_obrigatorio ? 'required' : ''; ?> value="<?= form_set_value($usuario, 'telefone_1'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <b>Deixe seu interesse/dúvida:</b>
                                    <div class="form-group">
                                        <textarea class="form-control" name="obs" rows="4" style="resize: vertical;" placeholder="Se desejar, descreva suas dúvidas sobre o imóvel ou aguarde nosso retorno..."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-block btn-enviar" onclick="ClienteScript.funcs.enviarContatoInteresse();" data-loading-text="<i class='fa fa-paper-plane-o'></i>Enviando, aguarde..."><i class="fa fa-paper-plane-o"></i>ENVIAR</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>