<? $estados = endereco_obter_estados_brasil();?>
<div class="modal fade" id="modal-cliente-dados" tabindex="-1" role="dialog" aria-labelledby="dadosModalLabel" ao-cliente-logar="SimplesScript.usuario.modais.funcs.preecherFormModalDadosComUsuarioLogado(usuario)">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seus dados</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <? $usuario_logado = $this->session->has_userdata('usuario') ? $this->session->userdata('usuario') : NULL; ?>
                    <form id="form-cliente-dados" onsubmit="return false;">
                        <div class="form-group col-xs-12">
                            <input class="form-control" type="text" name="nome" placeholder="Nome" required minlength="5" value="<?= form_set_value($usuario_logado, 'nome'); ?>">
                        </div>
                        <div class="form-group col-xs-6">
                            <input class="form-control" type="email" name="email" placeholder="Email" disabled required value="<?= form_set_value($usuario_logado, 'email'); ?>">
                        </div>
                        <div class="form-group col-xs-6">
                            <input class="form-control telefone" type="text" name="telefone_1" placeholder="Telefone" value="<?= form_set_value($usuario_logado, 'telefone_1'); ?>">
                        </div>
                        <div class="form-group col-xs-6">
                            <select name="uf" class="selectpicker" onchange="SimplesScript.usuario.modais.funcs.obterCidadesDeAcordoComAUFSelecionadaNoFormDaModalDadosUsuarioLogado();" data-live-search="true" title="Estado" data-width="100%">
                                <? foreach ($estados as $uf => $estado) : ?>
                                    <option value="<?= strtoupper($uf) ?>" <? if( ! is_null($usuario_logado) && strtoupper($usuario_logado->uf) == strtoupper($uf)) : echo  "selected"; endif;?>><?= $estado; ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <select name="cidade" class="selectpicker" data-cidade-default="<?= ! is_null($usuario_logado) && ! is_null($usuario_logado->cidade) ? strtoupper($usuario_logado->cidade) : '' ; ?>" data-live-search="true" disabled data-width="100%" title="Cidade"></select>
                        </div>
                    </form>
                    <div class="col-xs-12">
                        <hr>
                    </div>
                    <div class="col-xs-12 corretor">
                        <div class="row">
                            <div class="col-md-4">
                                <? $cliente_corretor_vinculado = ! is_null($usuario_logado) ? $usuario_logado->corretor : NULL ; ?>
                                <? $foto_corretor = ! is_null($cliente_corretor_vinculado) ? $GLOBALS[FILIAL_FOTOS_CORRETORES_CAMINHO_EXTERNO] . $cliente_corretor_vinculado->id_corretor .'.jpg' : $GLOBALS[CONFIG_IMG_USUARIO_SISTEMA_SEM_FOTO]; ?>
                                <img class="img-responsive corretor-foto" src="<?= $foto_corretor; ?>" alt="Corretor Simples" onError="this.src=VariaveisSistema.ImgUsuarioSistemaSemFoto;" />
                            </div>
                            <div class="col-md-8 dados-corretor">
                                <h4><b>Seu corretor</b></h4>
                                <h4 class="corretor-nome"><?= form_set_value($cliente_corretor_vinculado, 'nome'); ?></h4>
                                <p class="corretor-email">email: <a href="mailto:<?= form_set_value($cliente_corretor_vinculado, 'email'); ?>" target="_top"><?= form_set_value($cliente_corretor_vinculado, 'email'); ?></a></p>
                                <p><small class="corretor-creci">creci: <?= strtoupper(form_set_value($cliente_corretor_vinculado, 'creci')); ?></small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?=base_url('historico'); ?>" class="btn btn-default pull-left">Histórico</a>
                <button type="button" onclick="ClienteScript.funcs.salvarDados();" data-loading-text="Aguarde..." class="btn btn-info btn-salvar" autocomplete="off">Salvar</button>
                <button type="button" onclick="ClienteScript.funcs.logout();" class="btn btn-default">Sair</button>
            </div>
        </div>
    </div>
</div>
