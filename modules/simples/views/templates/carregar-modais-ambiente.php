<? if( ! $this->session->has_userdata('usuario')) : ?>
    <? $this->load->view('simples/templates/cliente/login-cadastro'); ?>
<? endif; ?>
<? $this->load->view('simples/templates/cliente/dados'); ?>
<? $this->load->view('simples/templates/cliente/interacao-formulario'); ?>
<? if(isset($imovel) || (isset($condominio))) : ?>
    <? $this->load->view('simples/templates/cliente/contato-interesse'); ?>
<? endif; ?>
