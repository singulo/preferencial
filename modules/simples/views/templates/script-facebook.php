<? if(facebook_api_habilitada()) : ?>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '<?= $GLOBALS[FILIAL_FACEBOOK_ID_API]; ?>',
                cookie     : true,
                xfbml      : true,
                version    : '<?= $GLOBALS[FILIAL_FACEBOOK_API_VERSAO]; ?>',
            });
            $(window).load(function(){
                $.simplesImob.ambiente.trigger($.simplesImob.ambienteEventosNome.FB_API_INICIADA);
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/pt_BR/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<? endif; ?>