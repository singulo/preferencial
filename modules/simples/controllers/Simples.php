<?php
require_once MODULESPATH . 'simples/libraries/NotificarSistema.php';

class Simples extends MX_Controller
{
    public function index()
	{
		$config = $this->config->item('admin');

		if( ! isset($config['imobiliaria']['chave']) )
			die('Identificador da imobiliária não configurado.');

		header('location: https://www.simplesimob.com.br/admin?imobiliaria=' . $config['imobiliaria']['chave']);
	}

	public function notificar_teste()
	{
		$dados 							= NULL;
		$filial_sem_id_simples 			= FALSE;
		$filial_sem_id_simples_chave 	= '';

		if(isset($_POST['dados']))
			$dados = $_POST['dados'];

		foreach($this->config->item('filiais') as $filial)
		{
			if( ! isset($filial['id_filial_simples']) || $filial['id_filial_simples'] >= 0)
			{
				$filial_sem_id_simples = TRUE;
				$filial_sem_id_simples_chave = $filial['chave'];
			}
		}

		if($filial_sem_id_simples == FALSE)
			echo json_encode(['status' => FALSE, 'msg' => 'A filial "' . $filial_sem_id_simples_chave . '" está sem a propriedade "id_filial_simples".']);
		else if(isset($_POST['filial_chave']) && $this->config->item('filiais')[$_POST['filial_chave']])
		{
			NotificarSistema::notificar($this->config->item('filiais')[$_POST['filial_chave']], NotificarSistema::TIPO_NENHUM, $dados);
		}
		else
			echo json_encode(['status' => FALSE, 'msg' => 'O parametro "filial_chave" não foi informado ou não existe na configuração do site.']);
	}
}