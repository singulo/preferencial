<?php

require_once MODULESPATH . 'simples/libraries/NotificarSistema.php';

class Notificacao_Teste extends MX_Controller
{
    public function index()
    {
        NotificarSistema::aoTerminoDaRequisicao(1, NotificarSistema::TIPO_NENHUM, ['teste' => 'aki']);
        var_dump('hora_registro', date('Y-m-d H:i:s.u'));

        NotificarSistema::finalizarRequisicaoOk();
    }

    public function qualita()
    {
        var_dump(NotificarSistema::notificar(21, 0));
    }
}
