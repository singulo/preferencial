<?php
require_once MODULESPATH . 'simples/core/Base_Controller.php';
require_once MODULESPATH . 'simples/helpers/cliente_helper.php';
require_once MODULESPATH . 'simples/libraries/NotificarSistema.php';

/**
 * @property Imovel_Model $imovel_model
 * @property Condominio_Model $condominio_model
 * @property Cliente_imovel_visualizacao_log_model $cliente_imovel_visualizacao_log_model
 * @property CI_Session $session
 */
class Base_Imovel_Controller extends Base_Controller
{
    protected $quantidade_fotos_principais = 1;
    protected $quantidade_sugestoes = 6;
    protected $pesquisa_order_by = 'valor ASC, id_tipo ASC';
    protected $filtro_parametros = array(
        array('param' => 'cidade',          'default_value' => array()),
        array('param' => 'id_tipo',         'default_value' => array()),
        array('param' => 'id_condominio',   'default_value' => array()),
        array('param' => 'finalidade',      'default_value' => array()),
        array('param' => 'preco_min',       'default_value' => 0),
        array('param' => 'preco_max',       'default_value' => 3000000),
        array('param' => 'metragem_min',    'default_value' => 0),
        array('param' => 'metragem_max',    'default_value' => 300),
        array('param' => 'id',       		'default_value' => ''),
        array('param' => 'dormitorios', 	'default_value' => ''),
        array('param' => 'garagem', 		'default_value' => ''),
        array('param' => 'suites', 			'default_value' => ''),
        array('param' => 'banheiros', 		'default_value' => ''),
        array('param' => 'id_situacao_agenciadora', 'default_value' => '')
    );

    protected $imovel_custom_model = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('simples/imovel_model');

        $this->imovel_custom_model = $this->imovel_model;
    }

    public function index($imovel = NULL)
    {
        $data = array();

        $data['imovel'] = NULL;

        if( ! is_null($imovel))
            $data['imovel'] = $imovel;
        else if(isset($_GET['id']))
            $data['imovel'] = $this->imovel_custom_model->pelo_codigo($_GET['id'], false);

        if( ! is_null($data['imovel']))
        {

            foreach ($this->imovel_custom_model->obter_valores_e_finalidades($data['imovel']->id) as $imovel)
            {
                if($imovel->finalidade == 1)
                {
                    $data['imovel']->finalidade_venda = 1;
                    $data['imovel']->valor = $imovel->valor_site;
                }
                elseif($imovel->finalidade == 2)
                {
                    $data['imovel']->finalidade_aluguel = 1;
                    $data['imovel']->valor_aluguel_mensal = $imovel->valor_site;
                }
                else
                {
                    $data['imovel']->finalidade_temporada = 1;
                    $data['imovel']->valor_aluguel_diario = $imovel->valor_site;
                }
            }

            if(isset($_GET['id_proposta']))
            {
                $this->load->model('simples/proposta_model');

                $proposta = $this->proposta_model->obter($_GET['id_proposta']);

                if( ! is_null($proposta) )
                {
                    if( $this->proposta_model->marcar_como_lida($proposta->id) == FALSE )
                        $proposta = NULL;
                }
                else
                    $proposta = $this->proposta_model->obter_lida($_GET['id_proposta']);

                if( ! is_null($proposta) )
                {
                    $this->load->model('simples/notificacao_model');
                    $this->load->model('simples/clientes_model');

                    $id_cliente = NULL;
                    $cliente = $this->clientes_model->pelo_email($proposta->cliente_email);

                    if( ! is_null($cliente))
                    {
                        if( ! $this->session->has_userdata('usuario') &&
                            ($cliente->bloqueado || $cliente->excluido) == false )
                        {
                            $this->clientes_model->registrar_ultimo_acesso($cliente->id);

                            $this->load->model('simples/corretores_model');
                            $cliente->corretor = $this->corretores_model->obter_informacoes_basicas($cliente->id_corretor);

                            $this->session->set_userdata('usuario', $cliente);
                        }

                        if( $this->session->has_userdata('usuario')
                            && isset($proposta->lida_em) == FALSE )
                        {
                            if($cliente->email == $this->session->userdata('usuario')->email)
                                $this->notificacao_model->proposta_lida($proposta->id, $proposta->id_corretor, $cliente->id);
                            else
                                $this->notificacao_model->proposta_lida($proposta->id, $proposta->id_corretor, NULL, "A proposta enviada para o cliente de email $proposta->cliente_email foi lida pelo usuário logado no site de email " . $this->session->userdata('usuario')->email);
                        }
                    }
                }
            }

            if($this->session->has_userdata('usuario') || SiteFilial::configs()->apenas_usuario_cadastrado_visualiza_as_midias_do_imovel === FALSE)
            {
                $midias = new stdClass();
                $midias->fotos->normais = [];
                $midias->fotos->planta = [];

                foreach (json_decode($data['imovel']->fotos) as $foto)
                {
                    if($foto->tipo == 1)
                        $midias->fotos->normais[] = $foto;
                    else
                        $midias->fotos->planta[]  = $foto;
                }

                $midias->videos = $this->midiasPeloImovel($data['imovel']->id);

                $data['imovel']->midias = $midias;
                unset($data['imovel']->fotos);
            }
            else
            {
                $data['total_midias_disponiveis'] = count(json_decode($data['imovel']->fotos));
                $data['imovel']->midias = new stdClass();
            }

            $data['imovel']->complementos = $this->imovel_custom_model->complementos($data['imovel']->id);

            if($data['imovel']->id_condominio > 0)
            {
                $this->load->model('simples/condominio_model');

                $data['imovel']->condominio->fotos = $this->condominio_model->fotos($data['imovel']->id_condominio);
                $data['imovel']->condominio->videos = $this->condominio_model->videos($data['imovel']->id_condominio);
                $data['imovel']->condominio->complementos = $this->condominio_model->complementos($data['imovel']->id_condominio);
            }

            $finalidade_simulares = [];
            if(isset($_GET['finalidade_clique_miniatura']) && $_GET['finalidade_clique_miniatura'] > 0 && $_GET['finalidade_clique_miniatura'] <= 3)
                $finalidade_simulares[] = $_GET['finalidade_clique_miniatura'];
            else
            {
                if($data['imovel']->finalidade_venda == 1)
                    $finalidade_simulares[] = Finalidades::Venda;
                if($data['imovel']->finalidade_aluguel == 1)
                    $finalidade_simulares[] = Finalidades::Aluguel;
                if($data['imovel']->finalidade_temporada == 1)
                    $finalidade_simulares[] = Finalidades::Temporada;
            }

            $data['imoveis_similares'] = $this->imovel_custom_model->similares($data['imovel'], $finalidade_simulares, $this->quantidade_sugestoes);

            //ARMAZEMANA VISUALIZAO DE IMOVEL SE CLIENTE LOGADO
            if($this->session->has_userdata('usuario'))
            {
                registrar_visualizacao_de_cliente_ao_imovel(
                    $this->session->userdata('usuario')->id,
                    $data['imovel']->id,
                    isset($_GET['finalidade_clique_miniatura']) && $_GET['finalidade_clique_miniatura'] > 0 && $_GET['finalidade_clique_miniatura'] < 4 ? $_GET['finalidade_clique_miniatura'] : 0);
            }
            else
                registrar_visualizacao_imovel_para_usuario_nao_logado($data['imovel']->id, isset($_GET['finalidade_clique_miniatura']) ? $_GET['finalidade_clique_miniatura'] : 0, $GLOBALS[FILIAL_CHAVE]);
        }

        return $data;
    }

    private function formataMidiasAgenciadora($imovel)
    {
        $fotos = array();

        foreach (json_decode($imovel->fotos) as $foto)
        {
            array_push($fotos, $foto);
        }

        return $fotos;
    }

    public function condominios()
    {
        $this->load->model('admin/imoveis_model');
        $data['condominios'] = $this->imoveis_model->condominios();

        return $data;
    }

    public function lancamentos()
    {
        $this->load->model('admin/imoveis_model');
        $data['lancamentos'] = $this->imoveis_model->lancamentos('f_cidade ASC, f_condominio ASC');

        return $data;
    }

    public function pesquisar()
    {
        $data['filtro'] = $this->monta_filtro();
        return $data;
    }

    protected function monta_filtro()
    {
        $filtro = new stdClass();

        if( ! isset($_GET['params']))
        {
            if(strlen($_SERVER['QUERY_STRING']) > 0)
            {
                $query  = explode('&', $_SERVER['QUERY_STRING']);
                $params = array();

                foreach( $query as $param )
                {
                    list($name, $value) = explode('=', $param, 2);
                    $params[urldecode($name)][] = urldecode($value);
                }

                foreach( $params as $key => $param)
                {
                    if(is_array($param) && count($param) == 1)
                        $params[$key] = $param[0];
                }
            }

            foreach($this->filtro_parametros as $parametro)
            {
                if(isset($params[$parametro['param']]))
                    $filtro->$parametro['param'] = $params[$parametro['param']];
                else if(isset($params[$parametro['param'] . '[]']))
                    $filtro->$parametro['param'] = $params[$parametro['param'] . '[]'];
                else if(isset($parametro['default_value']))
                    $filtro->$parametro['param'] = $parametro['default_value'];
            }
        }
        else
        {
            $filtro = (object)$_GET['params'];
        }

        return $filtro;
    }

    public function buscar()
    {
        $pagina = $this->input->post('pagina') != null ? $this->input->post('pagina') : 0;
        $limite = $this->input->post('limite') != null ? $this->input->post('limite') : 10;

        $params = $this->input->post('params');

        if(! is_null($this->input->post('ordenacao')))
            $this->pesquisa_order_by = $this->input->post('ordenacao');

        if(is_null($params))
            $params = array();

        $data['imoveis'] = $this->imovel_custom_model->pesquisar($params, $pagina, $limite, $this->pesquisa_order_by);
        if(count($data['imoveis']) == 0)
            $data['total'] = 0;
        else if($pagina == 0 || $this->input->post('forcarTotal'))
            $data['total'] = $this->imovel_custom_model->pesquisar_total_resultados($params);

        return $data;
    }

    protected function transforma_valor_imovel($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);

        return (double)filter_var($valor, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    }

    protected function registraChamadaAoTerminoDaRequisicaoParaNotificarUsuario($id_imovel)
    {
        $notificacao_tipo = NotificarSistema::TIPO_CLIENTE_OLHANDO_IMOVEL;
        $notificacao_dados['id_imovel'] = $id_imovel;

        if(isset($_GET['id_proposta']))
        {
            $notificacao_dados['id_proposta'] = $_GET['id_proposta'];
            $notificacao_tipo = NotificarSistema::TIPO_CLIENTE_LEU_PROPOSTA;
        }
        if($this->session->has_userdata('usuario'))
        {
            $notificacao_dados['id_cliente'] = $this->session->userdata('usuario')->id;
            NotificarSistema::aoTerminoDaRequisicao($GLOBALS[FILIAL_ID_SIMPLESIMOB], $notificacao_tipo, $notificacao_dados);
        }
    }

    public function midias()
    {
        if( ! isset($_POST['id']))
            echo json_encode(array('status' => FALSE, 'msg' => 'Identificador do imóvel é obrigatório'));
        else if($this->session->has_userdata('usuario'))
        {
            echo json_encode(array('status' => TRUE, 'midias' => $this->midiasPeloImovel(NULL, $_POST['id'], TRUE)));
        }
        else
            echo json_encode(array('status' => FALSE, 'msg' => 'Preciso logar no sistema antes de obter as mídias'));
    }

    protected function midiasPeloImovel($id = NULL )
    {
        return $this->imovel_custom_model->videos($id);
    }

    protected function obter_valores_e_finalidades($id)
    {
        $finalides = new stdClass();
        $finalides->aluguel = NULL;
        $finalides->venda = NULL;
        $finalides->temporada = NULL;

        foreach ($this->imovel_custom_model->obter_valores_e_finalidades($id) as $imovel)
        {
            if($imovel->finalidade == 1)
                $finalides->venda = $imovel->valor_site;
            elseif($imovel->finalidade == 2)
                $finalides->aluguel = $imovel->valor_site;
            else
                $finalides->temporada = $imovel->valor_site;
        }

        var_dump($finalides);

    }

}