<?php
require_once MODULESPATH . 'simples/core/Base_Controller.php';
require_once MODULESPATH . 'simples/helpers/cliente_helper.php';
require_once MODULESPATH . 'simples/libraries/EmailSimples.php';

/**
 * @property CI_Session $session
 * @property Clientes_Model $clientes_model
 * @property Corretores_Model $corretores_model
 * @property Proposta_Model $proposta_model
 * @property Contato_Model $contato_model
 * @property Contato_Imovel_Interesse_Model $contato_imovel_interesse_model
 */
class Email extends Base_Controller
{
    /* @var EmailSimples */
    protected $emailSimples;

    public function __construct()
    {
        parent::__construct();

        $dominiosSimples = [
            'http://www.simplesimob.com.br',
            'https://www.simplesimob.com.br',
        ];

        if (in_array($_SERVER['HTTP_ORIGIN'], $dominiosSimples)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        }

        $this->load->model('simples/clientes_model');
        $this->load->model('simples/imovel_model');
        $this->load->model('simples/proposta_model');
        $this->load->model('simples/corretores_model');
        $this->load->model('simples/notificacao_model');

        $this->emailSimples = new EmailSimples();
    }

    public function proposta_visualizar()
    {
        $cliente = $this->clientes_model->pelo_email($_GET['cliente_email']);

        if(is_null($cliente))
        {
            echo json_encode(['status' => FALSE, 'msg' => 'Cliente não encontrado pelo email informado.']);
            die;
        }

        $mensagem = isset($_GET['assunto']) ? $_GET['assunto'] : $_GET['mensagem'];

        $data['proposta'] = $this->emailSimples->corpoPropostaParaCliente(NULL, explode(',', $_GET['cod_imoveis']), $mensagem, $cliente->nome, $_GET['id_corretor']);

        $this->load->view('simples/proposta/visualizar', $data);
    }

    public function proposta_enviar()
    {
        $cliente  = $this->clientes_model->pelo_email($_POST['cliente_email']);
        $corretor = $this->corretores_model->obter_informacoes_basicas($_POST['id_corretor']);

        if(is_null($cliente))
        {
            echo json_encode(['status' => FALSE, 'msg' => 'Cliente não encontrado pelo email informado.']);
            die;
        }
        else if(is_null($corretor) || $corretor->bloqueado == 1)
        {
            echo json_encode(['status' => FALSE, 'msg' => 'Usuário da imobiliária não foi encontrado ou está bloqueado.']);
            die;
        }

        $mensagem = isset($_POST['assunto']) ? $_POST['assunto'] : $_POST['mensagem'];

        $proposta                   = new PropostaDomain();
        $proposta->id 				= NULL;
        $proposta->assunto 			= $mensagem;
        $proposta->id_cliente       = $cliente->id;
        $proposta->id_corretor      = $corretor->id_corretor;
        $proposta->cliente_nome 	= $cliente->nome;
        $proposta->cliente_email 	= $cliente->email;
        $proposta->cod_imoveis 	    = is_array($_POST['cod_imoveis']) ? $_POST['cod_imoveis'] : explode(',', $_POST['cod_imoveis']);
        $proposta->data 		    = date("Y-m-d H:i:s");

        $resposta['status'] = FALSE;

        if(count($proposta->cod_imoveis) == 0 || $this->imovel_model->pelos_codigos($proposta->cod_imoveis) != null)
        {
            $proposta->id = $this->proposta_model->novo($proposta, $proposta->cod_imoveis);

            if($proposta->id > 0)
            {
                $email                  = new EmailSimplesDomain($cliente->email, 'Proposta para você!', $proposta->id_corretor);
                $email->corpo           = $this->emailSimples->corpoPropostaParaCliente($proposta->id, $proposta->cod_imoveis, $mensagem, $cliente->nome, $proposta->id_corretor);
                $email->copiaRemetente  = (bool)$_POST['copia_remetente'];

                if($this->emailSimples->enviar($email))
                    $resposta['status'] = TRUE;
                else
                    $resposta['msg'] = 'Não foi possível enviar o email para o cliente.';
            }
            else
                $resposta['msg'] = 'Não foi possível registrar a proposta enviada. Por favor comunique a administração do sistema.';
        }
        else
            $resposta['msg'] = 'Imóveis informados não foram encontrado. Por favor verifique.';

        echo json_encode($resposta);
    }

    public function enviar_resposta()
    {
        $usuario = $this->corretores_model->obter_informacoes_basicas($_POST['id_corretor']);

        if(is_null($usuario) || $usuario->bloqueado == 1)
        {
            echo json_encode(['status' => FALSE, 'msg' => 'Usuário não encontrado ou bloqueado.']);
            die;
        }

        $notificacao = NULL;
        if($_POST['notificacao_tipo'] == 'normal')
        {
            $this->load->model('simples/contato_model');

            $contato = $this->contato_model->obter($_POST['id']);
        }
        else if($_POST['notificacao_tipo'] == 'interesse_em_imovel')
        {
            $this->load->model('simples/contato_imovel_interesse_model');

            $contato = $this->contato_imovel_interesse_model->obter($_POST['id']);
        }
        else
        {
            echo json_encode(array('status' => FALSE, 'msg' => 'Tipo de notificação de contato desconhecido.'));
            die;
        }

        if( ! is_null($contato))
        {
            $email = new EmailSimplesDomain($contato->email, 'Em resposta ao seu contato', $usuario->id_corretor);
            $email->corpo = $this->emailSimples->corpoRespostaContato(
                                                    $contato->nome,
                                                    $_POST['resposta'],
                                                    $usuario->id_corretor, $_POST['notificacao_tipo'] == 'normal' ? $contato->id_imovel : NULL);

            if($this->emailSimples->enviar($email))
            {
                if($_POST['notificacao_tipo'] == 'normal')
                    $status = $this->contato_model->marcar_resposta($_POST['id'], $usuario->id_corretor, $_POST['resposta']);
                else
                    $status = $this->contato_imovel_interesse_model->marcar_resposta($_POST['id'], $usuario->id_corretor, $_POST['resposta']);

                echo json_encode(array('status' => $status));
            }
            else
                echo json_encode(array('status' => FALSE, 'msg' => 'Ocorreu um erro ao enviar o email de resposta ao cliente.'));
        }
        else
            echo json_encode(array('status' => FALSE, 'msg' => 'O registro do contato não foi encontrado.'));
    }

    public function obter_rodape_padrao($id_corretor = NULL)
    {
        $arquivo = $this->emailSimples->arquivoRodape($id_corretor, FALSE);

        $this->load->helper('download');

        force_download($arquivo, NULL);
    }

    /**
     * Testa configuração cadastrada no formulário da imobiliária
     */
    public function testar()
    {
        $destinatario           = NULL;
        $id_usuario_remetente   = NULL;

        if(isset($_GET['destinatario']))
            $destinatario = $_GET['destinatario'];
        else if(isset($_POST['destinatario']))
            $destinatario = $_POST['destinatario'];

        if(isset($_GET['id_usuario_remetente']))
            $id_usuario_remetente = $_GET['id_usuario_remetente'];
        else if(isset($_POST['id_usuario_remetente']))
            $id_usuario_remetente = $_POST['id_usuario_remetente'];

        $resposta = array('status' => FALSE);

        if(is_null($destinatario) || ! filter_var($destinatario, FILTER_VALIDATE_EMAIL))
            $resposta['msg'] = 'Destinatário inválido.';
        else if(is_null($id_usuario_remetente))
            $resposta['msg'] = 'Identificador do usuário não informado.';
        else
        {
            $email = new EmailSimplesDomain($destinatario, 'Teste de E-mail SimplesImob', $id_usuario_remetente);
            $email->corpo = $this->emailSimples->corpoParaTeste('Teste de E-mail', 'Apenas um email de teste enviado pelo SimplesImob.');

            try
            {
                $resposta['status'] = $this->emailSimples->enviar($email);
                $resposta['msg']    = $this->emailSimples->ultimoErroAoTentarEnviar;
            }
            catch (Exception $ex)
            {
                $resposta = array('status' => FALSE, 'msg' => $ex->getMessage());
            }
        }

        echo json_encode($resposta);
    }
}