<?php
require_once MODULESPATH . 'simples/core/Base_Controller.php';
require_once MODULESPATH . 'simples/helpers/cliente_helper.php';
require_once MODULESPATH . 'simples/libraries/RequestMapper.php';
require_once MODULESPATH . 'simples/libraries/NotificarSistema.php';

/**
 * @property CI_Session $session
 * @property Clientes_Model $clientes_model
 * @property Corretores_Model $corretores_model
 * @property Cliente_imovel_visualizacao_log_model $cliente_imovel_visualizacao_log_model
 */
class Base_Cliente_Controller extends Base_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('simples/clientes_model');
        $this->load->model('simples/corretores_model');
    }

    public function login($foi_cadastrado = FALSE)
    {
        $resposta = array(
            'status'            => FALSE,
            'foi_cadastrado'    => $foi_cadastrado,
        );

        if(isset($_POST['email']))
        {
            $clientes = obter_registros_cliente_nas_filiais_pelo_email($_POST['email']);

            if(count($clientes) == 0) // EMAIL NÃO CADASTADO
            {
                if (isset($_POST['nome']) && is_string($_POST['nome']) && strlen($_POST['nome']) > 0)
                {
                    $cliente_id = 0;
                    if(registra_cliente($_POST, '', $cliente_id))
                    {
                        $clientes[$GLOBALS[FILIAL_CHAVE]] = obter_registros_cliente_na_filial_pelo_email($this, $_POST['email'], $GLOBALS[FILIAL_CHAVE]);
                        $resposta['foi_cadastrado'] = TRUE;
                    }
                }
                else
                    $resposta['msg'] = 'E-mail não cadastrado';
            }
            else if(isset($_POST['id_facebook']) && strlen($_POST['id_facebook']) > 0)
                foreach($clientes as $filial_chave => $cliente)
                    atualizar_facebook_cliente($cliente->id, $_POST['id_facebook'], $filial_chave);
            else if(isset($_POST['id_google']) && strlen($_POST['id_google']) > 0)
                foreach($clientes as $filial_chave => $cliente)
                    atualizar_google_cliente($cliente->id, $_POST['id_google'], $filial_chave);

            if(isset($clientes[$GLOBALS[FILIAL_CHAVE]]))
            {
                $cliente = $clientes[$GLOBALS[FILIAL_CHAVE]];

                $notificacao_tipo = NotificarSistema::TIPO_NENHUM;

                if($cliente->bloqueado || $cliente->excluido)
                {
                    $resposta['status'] = false;
                    $resposta['msg'] = $cliente->nome . ', seu cadastro parece não estar ativo. Por favor entre em contato para saber mais!';

                    $notificacao_tipo = $cliente->bloqueado ? NotificarSistema::TIPO_CLIENTE_BLOQUEADO_TENTANDO_ACESSAR_SITE : NotificarSistema::TIPO_CLIENTE_EXLUIDO_TENTANDO_ACESSAR_SITE;
                }
                else
                {
                    $cliente->ultimo_acesso  = date("Y-m-d H:i:s");
                    $this->clientes_model->registrar_ultimo_acesso($cliente->id, $cliente->ultimo_acesso);

                    $clientes[$GLOBALS[FILIAL_CHAVE]] = $cliente;

                    // REGISTRA VISUALIZAÇÃO DE IMOVEL QUANDO CLIENTE SE LOGAR
                    if(isset($_POST['visualizando_imovel']) && isset($_POST['finalidade_clicada']))
                    {
                        diminuir_uma_visualizacao_em_imovel($_POST['visualizando_imovel'], $_POST['finalidade_clicada'], $GLOBALS[FILIAL_CHAVE]);
                        registrar_visualizacao_de_cliente_ao_imovel($cliente->id, $_POST['visualizando_imovel'], $_POST['finalidade_clicada']);
                    }

                    seta_usuario_em_sessao_filiais($this, $clientes, $GLOBALS[FILIAL_CHAVE]);

                    $resposta['usuario'] = $cliente;
                    $resposta['status'] = true;

                    $notificacao_tipo = NotificarSistema::TIPO_CLIENTE_ACESSANDO_SITE;
                }

                NotificarSistema::aoTerminoDaRequisicao($GLOBALS[FILIAL_ID_SIMPLESIMOB], $notificacao_tipo, ['id_cliente' => $cliente->id]);
                NotificarSistema::finalizarRequisicaoOk(json_encode($resposta));
            }
            else
                echo json_encode($resposta);
        }
        else
            echo json_encode($resposta);
    }

    public function logout()
    {
        deslogar_usuario();

        $this->load->helper('url');
        redirect(base_url());
    }

    public function novo()
    {
        $cliente_id = 0;
        $msg_erro = '';
        $resposta['status'] = registra_cliente($_POST, '', $cliente_id, $msg_erro);

        if($resposta['status'])
        {
            $this->login(TRUE);
        }
        else
        {
            $resposta['msg'] = $msg_erro;
            echo json_encode($resposta);
        }
    }

    public function atualizar()
    {
        /** @var Clientes_Model $cliente */
        $cliente = $this->session->userdata('usuario');

        $cliente->nome 			 = $_POST['nome'];
        $cliente->telefone_1 	 = $_POST['telefone_1'];
        $cliente->cidade 		 = $_POST['cidade'];
        $cliente->uf             = $_POST['uf'];
        $cliente->atualizado_em  = date("Y-m-d H:i:s");
        $cliente->atualizado_por = $cliente->email;

        $cliente = RequestMapper::parseToObject((array)$cliente, array(), new ClienteDomain());

        echo json_encode(array('status' => $this->clientes_model->editar($cliente) > 0));
    }
}