<?php
require_once MODULESPATH . 'simples/core/Base_Controller.php';
require_once MODULESPATH . 'simples/helpers/cliente_helper.php';
require_once MODULESPATH . 'simples/libraries/RequestMapper.php';
require_once MODULESPATH . 'simples/libraries/NotificarSistema.php';
require_once MODULESPATH . 'simples/libraries/EmailSimples.php';

/**
 * @property Contato_Model $contato_model
 * @property Contato_Imovel_Interesse_Model $contato_imovel_interesse_model
 * @property Clientes_Imoveis_Log_Model $clientes_imoveis_log_model
 * @property Clientes_Model $clientes_model
 * @property Corretores_Model $corretores_model
 * @property Imoveis_Model $imoveis_model
 */
class Base_Contato_Controller extends Base_Controller
{
    /* @var EmailSimples */
    protected $emailSimples;

    public function __construct()
    {
        parent::__construct();

        $this->emailSimples = new EmailSimples();
    }

    public function index()
    {
        $this->load->view('contato');
    }

    public function novo()
    {
        $dados_contato = $this->trabalharClienteContatando();

        $resposta = array(
            'status'            => FALSE,
            'usuario_logado'    => $dados_contato['usuario_logado'],
        );

        if(isset($dados_contato['usuario_logado_dados']))
            $resposta['usuario_logado_dados'] = $dados_contato['usuario_logado_dados'];

        $this->load->model('simples/contato_model');

        $mapper = array('criado_em' => array('default_value' => date("Y-m-d H:i:s")));

        $contato = RequestMapper::parseToObject($_POST, $mapper, new ContatoDomain(), 'contato');

        $cliente = $dados_contato['cliente'];

        $contato->id_cliente = $cliente->id;
        $contato->id = $this->contato_model->novo($contato);
        if($contato->id > 0)
        {
            $resposta['status'] = TRUE;

            $emailAgradecimento = new EmailSimplesDomain($contato->email, 'Obrigado pelo contato!', $cliente->id_corretor);
            $emailAgradecimento->corpo = $this->emailSimples->corpoAgradecimentoPeloContatoCliente($contato->nome);

            if( ! $this->emailSimples->enviar($emailAgradecimento))
                $resposta['email_notificacao_cliente'] = array('status' => FALSE, 'msg' => $this->emailSimples->ultimoErroAoTentarEnviar);
            else
                $resposta['email_notificacao_cliente'] = array('status' => TRUE);

            $this->load->model('simples/corretores_model');
            $corretor = $this->corretores_model->obter_informacoes_basicas($cliente->id_corretor);

            $emailContatoCliente        = new EmailSimplesDomain($corretor->email, 'Cliente entrou em CONTATO!', $corretor->id_corretor);
            $emailContatoCliente->corpo = $this->emailSimples->corpoContatoNormalClienteParaCorretorVinculado($cliente->id, $contato);

            if( ! $this->emailSimples->enviar($emailContatoCliente))
                $resposta['email_notificacao_corretor_vinculado'] = array('status' => FALSE, 'msg' => $this->emailSimples->ultimoErroAoTentarEnviar);
            else
                $resposta['email_notificacao_corretor_vinculado'] = array('status' => TRUE);

            NotificarSistema::aoTerminoDaRequisicao($GLOBALS[FILIAL_ID_SIMPLESIMOB], NotificarSistema::TIPO_NOVO_CONTATO_NORMAL, ['contato' => $contato]);
            NotificarSistema::finalizarRequisicaoOk(json_encode($resposta));
        }
        else
            echo json_encode($resposta);
    }

    public function interesse_imovel()
    {
        $dados_contato = $this->trabalharClienteContatando();

        $resposta = array(
            'status'            => FALSE,
            'usuario_logado'    => $dados_contato['usuario_logado'],
        );

        if(isset($dados_contato['usuario_logado_dados']))
            $resposta['usuario_logado_dados'] = $dados_contato['usuario_logado_dados'];

        $cliente = $dados_contato['cliente'];

        $contato                = new stdClass();
        $contato->nome          = $_POST['nome'];
        $contato->email         = $_POST['email'];
        $contato->telefone      = $_POST['telefone'];
        $contato->id_imovel     = $_POST['cod_imovel'];
        $contato->id_cliente    = $cliente->id;
        $contato->mensagem      = $_POST['obs'];
        $contato->criado_em     = date("Y-m-d H:i:s");

        $this->load->model('simples/contato_imovel_interesse_model');
        $contato->id = $this->contato_imovel_interesse_model->inserir($contato);
        if($contato->id > 0)
        {
            $resposta['status'] = true;

            $this->load->model('simples/imovel_model');

            $emailContatoInteresse          = new EmailSimplesDomain($contato->email, 'Obrigado pelo interesse!', $cliente->id_corretor);
            $emailContatoInteresse->corpo   = $this->emailSimples->corpoAgradecimentoPeloInteresseDoClienteEmUmImovel($contato->id_imovel, $contato->nome);

            if( ! $this->emailSimples->enviar($emailContatoInteresse))
                $resposta['email_notificacao_usuario'] = array('status' => FALSE, 'msg' => $this->emailSimples->ultimoErroAoTentarEnviar);
            else
                $resposta['email_notificacao_usuario'] = array('status' => TRUE);

            $this->load->model('simples/corretores_model');
            $corretor = $this->corretores_model->obter_informacoes_basicas($cliente->id_corretor);

            $emailContatoCorretor        = new EmailSimplesDomain($corretor->email, 'Cliente INTERESSADO em um IMÓVEL!', $corretor->id_corretor);
            $emailContatoCorretor->corpo = $this->emailSimples->corpoContatoInteresseEmImovelParaCorretorVinculado($cliente->id, $contato, $contato->id_imovel);

            if( ! $this->emailSimples->enviar($emailContatoCorretor))
                $resposta['email_notificacao_corretor_vinculado'] = array('status' => FALSE, 'msg' => $this->emailSimples->ultimoErroAoTentarEnviar);
            else
                $resposta['email_notificacao_corretor_vinculado'] = array('status' => TRUE);

            NotificarSistema::aoTerminoDaRequisicao($GLOBALS[FILIAL_ID_SIMPLESIMOB], NotificarSistema::TIPO_NOVO_CONTATO_INTERESSE_EM_IMOVEL, ['contato' => $contato]);
            NotificarSistema::finalizarRequisicaoOk(json_encode($resposta));
        }
        else
            echo json_encode($resposta);
    }

    /* usuario_logado:
        0: Não logado
        1: Seta sessao (loga usuario)
        2: Novo Cadastro e logado
        3: Já logado mas e-mail informado não foi cadastrado
        4: Usuário logado
     */
    private function trabalharClienteContatando()
    {
        $resposta = array('usuario_logado' => 0);

        $this->load->model('simples/clientes_model');
        $cliente = $this->clientes_model->pelo_email($_POST['email']);

        if(is_null($cliente))
        {
            if( ! $this->session->has_userdata('usuario'))
            {
                logar_cliente_enviou_contato_se_necessario_registrar($_POST['email'], $_POST['nome'], $_POST['telefone'], $GLOBALS[FILIAL_CHAVE]);
                $resposta['usuario_logado']         = 2; //Novo cadastro e logado
                $resposta['usuario_logado_dados']   = $this->session->userdata('usuario');
                $cliente                            = $this->session->userdata('usuario');
            }
            else
            {
                $resposta['usuario_logado'] = 3; //Já logado mas e-mail informado não foi cadastrado
                $cliente                    = new ClienteDomain();
                $cliente->email             = $_POST['email'];
                $cliente->nome              = $_POST['nome'];
                $cliente->telefone_1        = $_POST['telefone'];
                $id_cliente                 = 0;
                if(registra_cliente((array)$cliente, '', $id_cliente))
                    $cliente->id            = $id_cliente;
                else
                    throw new Exception('Ocorreu um erro ao registrar seus dados. Por favor tente mais tarde.');
            }
        }
        else
        {
            if( ! $this->session->has_userdata('usuario'))
            {
                seta_usuario_em_sessao_filiais($this, obter_registros_cliente_nas_filiais_pelo_email($cliente->email), $GLOBALS[FILIAL_CHAVE]);
                $resposta['usuario_logado']         = 1; //Seta sessao (loga usuario)
                $resposta['usuario_logado_dados']   = $this->session->userdata('usuario');
            }
            else if($cliente->id_corretor == $this->session->userdata('usuario')->id_corretor)
                $resposta['usuario_logado'] = 4; //Usuário logado
            else
                $resposta['usuario_logado'] = 3; //Já logado mas e-mail informado não foi cadastrado
        }

        $resposta['cliente'] = $cliente;

        return $resposta;
    }

    public function enviar_interacao_formulario()
    {
        if($_POST['cadastrar_cliente_automaticamente'] == 1)
        {
            $this->load->model('simples/clientes_model');
            $cliente = $this->clientes_model->pelo_email($_POST['email']);
            $cliente_id = 0;
            if(is_null($cliente))
                $resposta['cliente_novo_registrado'] = registra_cliente($_POST, 'contato', $cliente_id);
            else
                $cliente_id = $cliente->id;
        }

        $mensagem = 'Usuário que preencheu o formulário pela interação programada do site, dados abaixo:<br><br>';

        $mensagem .= '<b>Nome:</b> ' . $_POST['nome'] . '<br>';
        $mensagem .= '<b>E-mail:</b> ' . $_POST['email'] . '<br>';
        $mensagem .= '<b>Telefone:</b> ' . $_POST['telefone_1'] . '<br>';

        if(isset($_POST['cidade']))
            $mensagem .= '<b>Cidade:</b> ' . $_POST['cidade'] . '/' .  $_POST['uf'] . '<br>';

        $mensagem .= '<b>Observação:</b> ' . $_POST['obs'];

        $email = new EmailSimplesDomain(SiteFilial::dadosImobiliaria()->contato_email, $_POST['email_assunto'], SiteFilial::configs()->id_corretor_padrao);
        $email->corpo = $this->emailSimples->obterCorpoHtmlUsuarioSimples(new EmailSimplesCorpoUsuarioSistemaDomain('Formulário de interação do site', $_POST['email_assunto'], $mensagem));

        $resposta['status'] = $this->emailSimples->enviar($email);

        if($resposta['status'])
        {
            NotificarSistema::aoTerminoDaRequisicao($GLOBALS[FILIAL_ID_SIMPLESIMOB], NotificarSistema::TIPO_NOVO_CONTATO_INTERESSE_EM_IMOVEL, ['id_cliente' => $cliente_id]);
            NotificarSistema::finalizarRequisicaoOk(json_encode($resposta));
        }
        else
            echo json_encode($resposta);
    }
}
