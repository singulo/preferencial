<?php
require_once MODULESPATH . 'simples/core/Base_Controller.php';

/**
 * @property Cidades_Model $cidades_model
 */
class Base_Cidade_Controller extends Base_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('simples/cidades_model');
    }

    public function obter_cidade_pela_uf()
    {
        $data['cidades'] = $this->cidades_model->obter_cidades_por_uf($_POST['uf']);
        return $data;
    }
}
