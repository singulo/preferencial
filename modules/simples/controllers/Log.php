<?php

class Log extends MX_Controller
{
    protected $pasta_logs;
    protected $tamanho_max_arquivo_log_para_enviar_conteudo = 5242880; //5MB

    public function __construct()
    {
        parent::__construct();
        $this->pasta_logs = APPPATH . "/logs/";
    }

	public function todos()
    {
        $this->load->helper('directory');
        echo json_encode(['logs' => directory_map($this->pasta_logs, FALSE, TRUE)]);
    }

    public function arquivo()
    {
        if(isset($_GET['arquivo']))
        {
            $arquivo = $this->pasta_logs . "/" . $_GET['arquivo'];
            if (file_exists($arquivo))
            {
                $arquivo_tamanho = filesize($arquivo);
                if(isset($_GET['baixar']) && $_GET['baixar'] == 1)
                {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . basename($arquivo).'"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . $arquivo_tamanho);
                    readfile($arquivo);
                    exit;
                }
                else if($this->tamanho_max_arquivo_log_para_enviar_conteudo > $arquivo_tamanho)
                    echo json_encode(['status' => TRUE, 'conteudo' => file_get_contents($arquivo)]);
                else
                    echo json_encode(['status' => FALSE, 'msg' => 'Arquivo muito grande, por favor faça o download']);
            }
            else
                echo json_encode(['status' => FALSE, 'msg' => 'Arquivo não existe']);
        }
        else
            echo json_encode(['status' => FALSE, 'msg' => 'Arquivo não informado']);
    }
}