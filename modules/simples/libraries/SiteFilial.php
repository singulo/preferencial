<?php
/**
 * Biblioteca disponivel para obter os recursos configurados
 * da filial selecionada
 */
class SiteFilial
{
    const SITE_DADOS_IMOBILIARIA_CARREGADOS                             = 'dados_imobiliaria_carregados';
    const SITE_FILIAL_VARIAVEIS_GLOBAIS_BANNERS_CARREGADOS              = 'filial_banners_carregados';
    const SITE_FILIAL_VARIAVEIS_GLOBAIS_IMOVEIS_TIPOS_CARREGADOS        = 'filial_tipos_carregados';
    const SITE_FILIAL_VARIAVEIS_GLOBAIS_IMOVEL_CIDADES_E_BAIRROS_CARREGADOS    = 'filial_cidades_e_bairros_carregados';
    const SITE_FILIAL_VARIAVEIS_GLOBAIS_CONDOMINIO_CIDADES_E_BAIRROS_CARREGADOS    = 'filial_cidades_e_bairros_condominios_carregados';
    const SITE_FILIAL_VARIAVEIS_GLOBAIS_CONFIGS_IMOBILIARIA             = 'filial_configs_imobiliaria';
    const SITE_FILIAL_VARIAVEIS_GLOBAIS_FINALIDADES_ATIVAS_CARREGADOS   = 'filial_finalidades_ativas_carregadas';

    public static function banners()
    {
        return self::carregar(
            self::SITE_FILIAL_VARIAVEIS_GLOBAIS_BANNERS_CARREGADOS,
            FILIAL_SITE_BANNER_ARQUIVO_INTERNO,
            array());
    }

    public static function imovelTipos()
    {
        return self::carregar(
            self::SITE_FILIAL_VARIAVEIS_GLOBAIS_IMOVEIS_TIPOS_CARREGADOS,
            FILIAL_SITE_IMOVEIS_TIPOS_ARQUIVO_INTERNO,
            array(),
            function($tipos){
                $_tipos = array();
                foreach($tipos as $tipo){
                    $_tipos[$tipo->id] = $tipo;
                }
                return $_tipos;
            });
    }

    /**
     * @desc Retorna o tipo extenso se retornarObjeto for FALSO, se não retornar o objeto de tipo
     */
    public static function imovelTipo($tipo, $retornarObjeto = FALSE)
    {
        $tipos = self::imovelTipos();

        if(isset($tipos[$tipo]))
            return $retornarObjeto ? $tipos[$tipo] : $tipos[$tipo]->tipo;
        else
            return $retornarObjeto ? (object)['id' => 0, 'tipo' => 'Imóvel'] : 'Imóvel';
    }

    public static function finalidadesAtivas()
    {
        if( ! isset($GLOBALS[self::SITE_FILIAL_VARIAVEIS_GLOBAIS_FINALIDADES_ATIVAS_CARREGADOS]))
            $GLOBALS[self::SITE_FILIAL_VARIAVEIS_GLOBAIS_FINALIDADES_ATIVAS_CARREGADOS] = Finalidades::ativas();

        return $GLOBALS[self::SITE_FILIAL_VARIAVEIS_GLOBAIS_FINALIDADES_ATIVAS_CARREGADOS];
    }

    public static function finalidadesEstaoAtivas()
    {
        return count(self::finalidadesAtivas()) > 1;
    }

    public static function cidadesEBairros()
    {
        return self::carregar(
            self::SITE_FILIAL_VARIAVEIS_GLOBAIS_IMOVEL_CIDADES_E_BAIRROS_CARREGADOS,
            FILIAL_SITE_IMOVEIS_CIDADES_E_BAIRROS_ARQUIVO_INTERNO,
            array());
    }


    public static function cidadesEBairrosCondominios()
    {
        return self::carregar(
            self::SITE_FILIAL_VARIAVEIS_GLOBAIS_CONDOMINIO_CIDADES_E_BAIRROS_CARREGADOS,
            FILIAL_SITE_CONDOMINIOS_CIDADES_E_BAIRROS_ARQUIVO_INTERNO,
            array());
    }

    /**
     * @return ConfigsGeraisSite
     */
    public static function configs()
    {
        if(isset($GLOBALS[self::SITE_FILIAL_VARIAVEIS_GLOBAIS_CONFIGS_IMOBILIARIA]))
            return $GLOBALS[self::SITE_FILIAL_VARIAVEIS_GLOBAIS_CONFIGS_IMOBILIARIA];

        $padrao = new ConfigsGeraisSite();

        $configs =  self::carregar(
                        self::SITE_FILIAL_VARIAVEIS_GLOBAIS_CONFIGS_IMOBILIARIA,
                        FILIAL_SITE_CONFIGS_GERAIS_ARQUIVO_INTERNO,
                        $padrao);

        foreach($padrao as $config => $valorPadrao)
        {
            if(isset($configs->$config))
                $padrao->$config = $configs->$config;
        }

        if(is_object($padrao->numero_maximo_valores_no_filtro))
            $padrao->numero_maximo_valores_no_filtro = array(
                '1' => (array)$padrao->numero_maximo_valores_no_filtro->venda,
                '2' => (array)$padrao->numero_maximo_valores_no_filtro->aluguel,
                '3' => (array)$padrao->numero_maximo_valores_no_filtro->temporada,
            );

        $GLOBALS[self::SITE_FILIAL_VARIAVEIS_GLOBAIS_CONFIGS_IMOBILIARIA] = $padrao;

        return $GLOBALS[self::SITE_FILIAL_VARIAVEIS_GLOBAIS_CONFIGS_IMOBILIARIA];
    }

    /**
     * @return DadosImobiliaria
     */
    public static function dadosImobiliaria()
    {
        return self::carregar(
                        self::SITE_DADOS_IMOBILIARIA_CARREGADOS,
                        FILIAL_SITE_DADOS_IMOBILIARIA_ARQUIVO_INTERNO,
                        array());
    }

    protected static function carregar($variavelGlobal, $variavelFilialArquivo, $valorPadrao = NULL, $manipularValor = NULL)
    {
        if( ! isset($GLOBALS[$variavelGlobal]))
        {
            if( ! isset($GLOBALS[$variavelFilialArquivo]))
                throw new Exception('Obrigatória a configuração para "' . $variavelFilialArquivo . '" no arquivo da filial para utilizar essa funcionalidade');

            $GLOBALS[$variavelGlobal] = file_exists($GLOBALS[$variavelFilialArquivo]) ?
                json_decode(file_get_contents($GLOBALS[$variavelFilialArquivo])) : $valorPadrao;

            if(is_callable($manipularValor))
                $GLOBALS[$variavelGlobal] = $manipularValor($GLOBALS[$variavelGlobal]);
        }

        return $GLOBALS[$variavelGlobal];
    }
}

abstract class DadosImobiliaria
{
    public $creci;
    public $estado;
    public $cidade;
    public $endereco;
    public $latitude_longitude;

    public $contato_email;
    public $telefones;

    public $link_pagina_facebook;
    public $link_pagina_instagram;
    public $link_perfil_twitter;
    public $link_canal_youtube;
    public $numeros_whatsapp;
}

class ConfigsGeraisSite
{
    /* CLIENTE */
    public $id_corretor_padrao                                      = 0;
    public $id_origem_cadastro_site                                 = NULL;
    public $apenas_usuario_cadastrado_visualiza_as_midias_do_imovel = TRUE;
    public $formulario_cadastro_telefone_obrigatorio                = FALSE;
    public $formulario_contato_interesse_telefone_obrigatorio       = FALSE;

    /* FILTRO DE PESQUISA */
    public $numero_maximo_dormitorios_no_filtro = 4;
    public $numero_maximo_suites_no_filtro      = 4;
    public $numero_maximo_vagas_no_filtro       = 4;
    public $numero_maximo_banheiros_no_filtro   = 4;
    public $numero_maximo_valores_no_filtro     = array(
                                                    '1' => array( //FINALIDADE VENDA
                                                        'min' => 0,
                                                        'max' => 3000000,
                                                        'intervalo' => 50000,
                                                        'similares_porcentagem' => 15),
                                                    '2' => array( //FINALIDADE ALUGUEL
                                                        'min' => 0,
                                                        'max' => 3500,
                                                        'intervalo' => 250,
                                                        'similares_porcentagem' => 10),
                                                    '3' => array( //FINALIDADE TEMPORADA
                                                        'min' => 0,
                                                        'max' => 2500,
                                                        'intervalo' => 200,
                                                        'similares_porcentagem' => 10)
                                                );

    public $filtro_cidade_e_bairros                    = TRUE;

    /* FINALIDADES */
    public $finalidade_venda_ativo      = TRUE;
    public $finalidade_aluguel_ativo    = TRUE;
    public $finalidade_temporada_ativo  = TRUE;

    /* CODIGO REFERENCIA */
    public $usar_codigo_referencia_imovel  = FALSE;
    public $ao_usar_codigo_referencia_imovel_pesquisar_pelos_2_campos = FALSE;

    /* EMAIL */
    public $email_html_cor_principal                        = '#03758b';
    public $email_html_cor_letreiro_finalidade_venda        = '#03758b';
    public $email_html_cor_letreiro_finalidade_aluguel      = '#ef9a00';
    public $email_html_cor_letreiro_finalidade_temporada    = '#ce0000';
    public $email_html_cor_corpo_fundo                      = '#01687b1f';

    /* SISTEMA */
    public $log_pagina_nao_encontrada                = FALSE;
}