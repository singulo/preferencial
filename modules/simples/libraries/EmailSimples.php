<?php
require_once MODULESPATH . 'simples/libraries/constants/NotificacaoTipos.php';

class EmailSimples
{
    protected $CI;

    const TIPO_CLIENTE_CADASTADO    = 'cliente_cadastrado';
    const TIPO_CONTATO_NORMAL       = 'contato_normal';
    const TIPO_INTERESSE_EM_IMOVEL  = 'contato_interesse_em_imovel';

    public $ultimoErroAoTentarEnviar = '';

    public function __construct()
    {
        $this->CI = get_instance();
        $this->CI->load->model('simples/configuracao_model');
    }

    public function corpoBoasVindasParaCliente($nome_destinatario = '')
    {
        $assunto = 'Seja muito bem-vindo';

        $mensagem = 'O seu cadastro já está ativo, faça o seu login apenas informando o seu email.';

        $questionamentos = array(
            array(
                'pergunta' => 'Imóveis/Mídias disponíveis?',
                'resposta' => 'Agora você pode visualizar todos os imóveis e mídias do nosso site',
            ),
            array(
                'pergunta' => 'Gostou de um imóvel ou quer alguma informação?',
                'resposta' => 'Apenas clique no botão <i>"<b>Gostaria de mais informações</b>"</i> que está na página de detalhes do imóvel, digite a sua observação se houver e clique em <i><b>ENVIAR</b></i>',
            ),
            array(
                'pergunta' => 'Quer seu imóvel no nosso site?',
                'resposta' => 'Basta entrar em contato pelo site ou por nossos telefones, você pode encontrar todos eles aqui: <a href="' . base_url('contato') . '" target="_blank">' . base_url('contato') . '</a>'
            )
        );

        $mensagem .= '<br><br><ul style="padding-left: 18px; font-size: 13px;">';
        foreach ($questionamentos as $questionamento)
        {
            $mensagem .= '<li><b style="font-size: 14px;">' . $questionamento['pergunta'] . '</b><br>' . $questionamento['resposta'] . '<br><br></li>';
        }
        $mensagem .= '</ul>';

        return $this->obterCorpoHtmlUsuarioSite(new EmailSimplesCorpoSiteDomain($assunto, $mensagem, $nome_destinatario));
    }

    public function corpoPropostaParaCliente($id_proposta, $imoveis, $mensagem, $nome_destinatario = '', $id_corretor = NULL)
    {
        $assunto = 'Imóveis para você';

        $corpo              = new EmailSimplesCorpoSiteDomain($assunto, $mensagem, $nome_destinatario);
        $corpo->imoveis     = $imoveis;
        $corpo->idCorretor  = $id_corretor;
        $corpo->idProposta  = $id_proposta;

        return $this->obterCorpoHtmlUsuarioSite($corpo);
    }

    protected function apenasDadosDosCampos($dados, array $camposPossiveis)
    {
        foreach((array)$dados as $campo => $valor)
        {
            if( ! in_array($campo, $camposPossiveis))
                unset($dados->$campo);
        }
        return $dados;
    }

    protected function corpoClienteParaCorretorVinculado($tipo, $id_cliente, $dados_contato, $imoveis)
    {
        $pagina_titulo = 'Cliente contato';
        $assunto = '';
        $mensagem = '';
        $notificacao_tipo = NULL;

        switch($tipo)
        {
            case self::TIPO_CLIENTE_CADASTADO:
                $pagina_titulo      = 'Cliente se cadastrou pelo site';
                $assunto            = 'Cliente se cadastrou pelo site';
                $mensagem           = ', acaba de se <b>cadastrar</b> pelo site. Segue dados:';
                $notificacao_tipo   = NotificacaoTipos::ClienteVinculado;
            break;
            case self::TIPO_CONTATO_NORMAL:
                $pagina_titulo      = 'Cliente lhe contatou pelo site';
                $assunto            = 'Cliente lhe contatou pelo site';
                $mensagem           = ', entrou em contato pelo site. Segue dados:';
                $notificacao_tipo   = NotificacaoTipos::Normal;
            break;
            case self::TIPO_INTERESSE_EM_IMOVEL:
                $pagina_titulo      = 'Cliente interessado em um imóvel';
                $assunto            = 'Cliente interessado em um imóvel';
                $mensagem           = ', entrou em contato pelo site, solicitando mais informações sobre o imóvel abaixo. Segue os dados:';
                $notificacao_tipo   = NotificacaoTipos::InteresseEmImovel;
            break;
        }

        $valor_estilo                   = 'margin-top: 5px; margin-bottom: 10px; font-size: 13px; text-align: justify; padding: 5px 10px; background-color: #f7f7f7; border: 1px solid #efefef;';
        $valor_estilo_nao_informado     = 'color: #bbbbbb; font-style: italic;';
        $link_sistema_perfil_cliente    = SISTEMA_SIMPLES_LINK_ADMIN . 'cliente/perfil?id=' . $id_cliente;

        $mensagem = '<p style="margin-top: 0px; margin-bottom: 0px; color: #444;"><a href="' . $link_sistema_perfil_cliente . '" target="_blank"><b>' . $dados_contato->nome . '</b></a>' . $mensagem . '</p><br>';

        foreach((array)$dados_contato as $chave => $valor)
        {
            $_valor = $valor;
            $_valor_estilo = $valor_estilo;
            if(is_null($valor) || empty($valor))
            {
                $_valor = '-- Não informado --';
                $_valor_estilo .= $valor_estilo_nao_informado;
            }

            $mensagem .= '<b style="color: #555;"><small>' . str_replace('_', ' ', ucwords($chave)) . '</small></b><p style="' . $_valor_estilo . '">' . $_valor . '</p>';
        }

        $btns_sistema = array(
            array(
                'texto' => 'Perfil',
                'link'  => $link_sistema_perfil_cliente,
                'style' => 'background-color: #04677d;'
            )
        );

        if( ! is_null($notificacao_tipo))
        {
            $link_sistema_notificacao = SISTEMA_SIMPLES_LINK_ADMIN . 'notificacao?id=' . $dados_contato->id . '&tipo=' . $notificacao_tipo;
            $btns_sistema[] = array(
                'texto' => 'Notificação',
                'link'  => $link_sistema_notificacao,
                'style' => 'background-color: #0b7384;'
            );
        }

        $corpo                  = new EmailSimplesCorpoUsuarioSistemaDomain($pagina_titulo, $assunto, $mensagem);
        $corpo->imoveis         = $imoveis;
        $corpo->btnsSistema     = $btns_sistema;

        return $this->obterCorpoHtmlUsuarioSimples($corpo);
    }

    public function corpoClienteCadastradoParaCorretorVinculado($id_cliente, $dados_contato)
    {
        $dados_contato = $this->apenasDadosDosCampos($dados_contato, ['id', 'nome', 'email', 'email_alteranativo', 'telefone_1', 'uf', 'cidade']);

        return $this->corpoClienteParaCorretorVinculado(self::TIPO_CLIENTE_CADASTADO, $id_cliente, $dados_contato, array());
    }

    public function corpoContatoNormalClienteParaCorretorVinculado($id_cliente, $dados_contato)
    {
        return $this->corpoClienteParaCorretorVinculado(self::TIPO_CONTATO_NORMAL, $id_cliente, $dados_contato, array());
    }

    public function corpoContatoInteresseEmImovelParaCorretorVinculado($id_cliente, $dados_contato, $imoveis)
    {
        return $this->corpoClienteParaCorretorVinculado(self::TIPO_INTERESSE_EM_IMOVEL, $id_cliente, $dados_contato, $imoveis);
    }

    public function corpoParaTeste($assunto, $mensagem)
    {
        return $this->obterCorpoHtmlUsuarioSite(new EmailSimplesCorpoSiteDomain($assunto, $mensagem, ''));
    }

    public function corpoAgradecimentoPeloContatoCliente($nome_destinatario = '')
    {
        $assunto = 'Retorno do seu contato';
        $mensagem = 'Obrigado pelo seu contato.<br>Vamos lhe responder assim que possível, por favor aguarde.';

        return $this->obterCorpoHtmlUsuarioSite(new EmailSimplesCorpoSiteDomain($assunto, $mensagem, $nome_destinatario));
    }

    public function corpoAgradecimentoPeloInteresseDoClienteEmUmImovel($id_imovel, $nome_destinatario = '', $id_corretor = NULL)
    {
        $assunto = 'Retorno do seu pedido de informação';
        $mensagem = 'Obrigado pelo seu contato.<br>Vamos lhe responder assim que possível com mais informações sobre o imóvel abaixo, por favor aguarde.';

        $corpo              = new EmailSimplesCorpoSiteDomain($assunto, $mensagem, $nome_destinatario);
        $corpo->idCorretor  = $id_corretor;
        $corpo->imoveis     = $id_imovel;

        return $this->obterCorpoHtmlUsuarioSite($corpo);
    }

    public function corpoRespostaContato($nome_destinatario, $resposta, $id_corretor = NULL, $id_imovel = NULL)
    {
        $assunto = 'Retorno do seu contato';

        $corpo              = new EmailSimplesCorpoSiteDomain($assunto, $resposta, $nome_destinatario);
        $corpo->imovel      = $id_imovel;
        $corpo->idCorretor  = $id_corretor;

        return $this->obterCorpoHtmlUsuarioSite($corpo);
    }

    public function obterCorpoHtmlUsuarioSite(EmailSimplesCorpoSiteDomain $corpo)
    {
        $htmls = $this->CI->configuracao_model->email->estruturasHtml();

        $imoveis = array();

        if( ! is_array($corpo->imoveis))
            $corpo->imoveis = array($corpo->imoveis);

        if(count($corpo->imoveis) > 0)
        {
            $this->CI->load->model('simples/imovel_model');

            $imoveis = $this->CI->imovel_model->pelos_codigos($corpo->imoveis);
        }

        $textosMarcacoes = array(
            '$imobiliaria_nome$'        => $GLOBALS[FILIAL_NOME],
            '$logo_imobiliaria_url$'    => $GLOBALS[CONFIG_IMOBILIARIA_LOGO],
            '$site_link$'               => base_url(),
            '$site_link_contato$'       => base_url('contato'),
            '$saudar_destinatario$'     => saudacao() . (strlen($corpo->nomeDestinatario) > 0 ? ', ' . explode(' ', $corpo->nomeDestinatario)[0] : ''),
            '$ano_atual$'               => date('Y'),
            '$redes_sociais$'           => $this->montarElementosRedeSocial(),
            '$assunto$'                 => $corpo->assunto,
            '$mensagem$'                => $corpo->mensagem,
            '$imoveis$'                 => (count($imoveis) > 0 ? $this->montarElementosImoveis($htmls->imoveis, $imoveis, $corpo->idProposta) : ''),
            '$assinatura_url$'          => $this->arquivoRodape($corpo->idCorretor)
        );

        return $this->trocaCoresHtml($this->trocarTextosMarcacao($textosMarcacoes, $htmls->estrutura));
    }
    
    public function obterCorpoHtmlUsuarioSimples(EmailSimplesCorpoUsuarioSistemaDomain $corpo)
    {
        $htmls = $this->CI->configuracao_model->email->estruturasHtml();

        $imoveis = array();

        if( ! is_array($corpo->imoveis))
            $corpo->imoveis = array($corpo->imoveis);

        if(count($corpo->imoveis) > 0)
        {
            $this->CI->load->model('simples/imovel_model');

            $imoveis = $this->CI->imovel_model->pelos_codigos($corpo->imoveis);
        }

        $textosMarcacoes = array(
            '$sistema_logo_url$'            => SISTEMA_SIMPLES_LOGO,
            '$sistema_site_link$'           => SISTEMA_SIMPLES_LINK,
            '$sistema_site_link_contato$'   => SISTEMA_SIMPLES_LINK,
            '$pagina_titulo$'               => $corpo->paginaTitulo . ' | SimplesImob',
            '$saudar_destinatario$'         => saudacao(),
            '$ano_atual$'                   => date('Y'),
            '$assunto$'                     => $corpo->assunto,
            '$mensagem$'                    => $corpo->mensagem,
            '$imoveis$'                     => (count($imoveis) > 0 ? $this->montarElementosImoveis($htmls->imoveis, $imoveis) : ''),
            '$btns_sistema$'                => (count($corpo->btnsSistema) > 0 ? $this->linhaBtnsSistema($corpo->btnsSistema) : ''),
        );

        return $this->trocaCoresHtml($this->trocarTextosMarcacao($textosMarcacoes, $htmls->estrutura_simples));
    }

    protected function trocarTextosMarcacao(array $textos, $html)
    {
        foreach ($textos as $marcacao => $valor)
            $html = str_replace($marcacao, $valor, $html);

        return $html;
    }

    protected function linhaBtnsSistema(array $links)
    {
        $html = '';
        $quantidade = count($links);

        $simples_img_icon_estilo = 'z-index: 1; float: left; position: relative; padding: 0px; background-color: #316d7f; border-radius: 50%; margin-top: -5px; margin-left: -1px; height: 45px; border-right: 0px solid #2e6a7c; box-shadow: 2px 0px 15px -4px rgba(0,0,0,0.87); -webkit-box-shadow: 2px 0px 15px -4px rgba(0,0,0,0.87); -moz-box-shadow: 2px 0px 15px -4px rgba(0,0,0,0.87);';
        $simples_btns_link_sistema = 'padding: 10px 0px; color: #fff; text-decoration: none; width: ' . (450/$quantidade) . 'px; display: block; float: left; text-align: center; z-index: 0; letter-spacing: 0.5px;';

        for ($x = 0; $x < $quantidade; $x++)
        {
            $html .= '<a href="' . $links[$x]['link'] . '" style="' . $simples_btns_link_sistema . ($quantidade == $x+1 ? 'border-top-right-radius: 5px; border-bottom-right-radius: 5px;' : '' ) . (isset($links[$x]['style']) ? $links[$x]['style'] : '') . '" target="_blank">' . $links[$x]['texto'] . '</a>';
        }

        return '<tr><td style="text-align: center; border-bottom: 1px solid #f6f6f6; margin-top: 30px; background-color: #fbfbfb; padding: 20px 70px 35px;"><p style="margin-top: 15px; font-size: 12px; margin-bottom: 10px; font-weight: 600; color: #6f6f6f; text-align: left;">Veja no sistema:</p><img src="' . SISTEMA_SIMPLES_LOGO_CIRCULAR . '" style="' . $simples_img_icon_estilo . '"><p style="margin-top: -42px; margin-bottom: 5px; margin-left: 10px; float: left; font-size: 15px;">' . $html . '</p><small style="float: left; margin-top: 5px; color: #9c9c9c; font-style: italic; font-size: 11px; text-align: left; width: 100%;">Botões links para auxiliar o usuário a verificação das informações deste e-email.</small></td></tr>';
    }

    protected function trocaCoresHtml($html)
    {
        $html = str_replace('$cor_corpo_fundo$',    SiteFilial::configs()->email_html_cor_corpo_fundo, $html);
        $html = str_replace('$cor_principal$',      SiteFilial::configs()->email_html_cor_principal, $html);

        return $html;
    }

    protected function montarElementosImoveis($html, $imoveis, $id_proposta = NULL)
    {
        $_html = [];
        foreach ($imoveis as $imovel)
        {
            $html_imovel = $html;

            $imovel_link = imovel_url($imovel, FALSE);

            if( ! is_null($id_proposta))
                $imovel_link .= (strpos($imovel_link, '?') !== FALSE ? '&' : '?') . 'id_proposta=' . $id_proposta;

            if($imovel->id_empresa_agenciadora > 0)
            {
                $descricao = array();

                if($imovel->dormitorios > 0)
                {
                    $str = "Com $imovel->dormitorios ". texto_para_plural_se_necessario($imovel->dormitorios,'dormitório');
                    array_push($descricao, $str);
                }
                if($imovel->suites > 0)
                {
                    $str = "Sendo $imovel->suites". texto_para_plural_se_necessario($imovel->dormitorios,'suíte');
                    array_push($descricao, $str);
                }
                if($imovel->suites > 0)
                {
                    $str = "$imovel->banheiros". texto_para_plural_se_necessario($imovel->dormitorios,'banheiro');
                    array_push($descricao, $str);
                }
                if($imovel->area_util > 0)
                {
                    $str = $imovel->area_util. 'm²';
                    array_push($descricao, $str);
                }

                $html_imovel = str_replace('$imovel_link$', base_url("condominios?$imovel->id_condominio&unidade=$imovel->id_unidade_agenciadora")  , $html_imovel);
                $html_imovel = str_replace('$imovel_src_img$',$imovel->foto, $html_imovel);
                $html_imovel = str_replace('$imovel_descricao$', join(', ', $descricao) , $html_imovel);

            }
            else
            {
                $html_imovel = str_replace('$imovel_link$',         $imovel_link, $html_imovel);
                $html_imovel = str_replace('$imovel_src_img$',      $GLOBALS[FILIAL_FOTOS_IMOVEIS_CAMINHO_EXTERNO] . $imovel->foto, $html_imovel);
                $html_imovel = str_replace('$imovel_descricao$',    (strlen($imovel->descricao) > 275 ? substr($imovel->descricao, 0, 272) . '...' : $imovel->descricao), $html_imovel);
            }


            $html_imovel = str_replace('$imovel_tipo$',         SiteFilial::imovelTipo($imovel->id_tipo), $html_imovel);
            $html_imovel = str_replace('$imovel_codigo$',       imovel_filial_id($imovel), $html_imovel);
            $html_imovel = str_replace('$imovel_valor$',        $this->montarValoresElementoParaImovel($imovel), $html_imovel);

            $_html[] = $html_imovel;
        }

        return implode('<tr><td style="border-bottom: 2px solid #fff;"></td></tr>', $_html);
    }

    protected function montarValoresElementoParaImovel($imovel)
    {
        $elemento = [];

        $span_finalidade_estilo = 'border-radius: 15px; padding: 4px 8px; font-size: 9px; color: #fff;';

        $valor_estilo = 'font-size: 14px; word-spacing: -2px;';
        $cifrao_estilo = 'font-size: 9px;';
        $centavos_estilo = 'font-size: 11px; font-weight: 200;';

        $valores = array(
            'VENDA'     => array('propriedade_finalidade' => 'finalidade_venda', 'propriedade_valor' => 'valor', 'span_finalidade_cor' => SiteFilial::configs()->email_html_cor_letreiro_finalidade_venda),
            'ALUGUEL'   => array('propriedade_finalidade' => 'finalidade_aluguel', 'propriedade_valor' => 'valor_aluguel_mensal', 'span_finalidade_cor' => SiteFilial::configs()->email_html_cor_letreiro_finalidade_aluguel),
            'TEMPORADA' => array('propriedade_finalidade' => 'finalidade_temporada', 'propriedade_valor' => 'valor_aluguel_diario', 'span_finalidade_cor' => SiteFilial::configs()->email_html_cor_letreiro_finalidade_temporada),
        );

        foreach($valores as $finalidade => $valor)
        {
            $propriedade_finalidade = $valor['propriedade_finalidade'];
            $propriedade_valor      = $valor['propriedade_valor'];

            if($imovel->$propriedade_finalidade == 1)
                $elemento[] = '<div style="float: left; width: 33.3333333%;"><span style="'. $span_finalidade_estilo . ' background-color:' . $valor['span_finalidade_cor'] . '">' . $finalidade . '</span><br><br><b style="' . $valor_estilo . '">' . str_replace(',00', '<small style="' . $centavos_estilo . '">,00</small>', formatar_valor($imovel->$propriedade_valor, '<small style="' . $cifrao_estilo . '">R$</small> ')) . '</b></div>';
        }

        return implode('', $elemento);
    }

    protected function montarElementosRedeSocial()
    {
        $elementos = array();

        if(strlen(SiteFilial::dadosImobiliaria()->link_pagina_facebook) > 0)
            $elementos[] = '<a href="' . SiteFilial::dadosImobiliaria()->link_pagina_facebook . '" target="_blank" style="text-decoration: none;"><img src="' . base_url('modules/simples/assets/email/imagens/icon-facebook.png') . '"/></a>';
        if(strlen(SiteFilial::dadosImobiliaria()->link_pagina_instagram) > 0)
            $elementos[] = '<a href="' . SiteFilial::dadosImobiliaria()->link_pagina_instagram . '" target="_blank" style="text-decoration: none;"><img src="' . base_url('modules/simples/assets/email/imagens/icon-instagram.png') . '"/></a>';
        if(count((array)SiteFilial::dadosImobiliaria()->numeros_whatsapp) > 0)
        {
            $numeros = (array)SiteFilial::dadosImobiliaria()->numeros_whatsapp;
            $elementos[] = '<a href="https://api.whatsapp.com/send?phone=' . apenas_numeros_na_string(array_values($numeros)[0]) . '" target="_blank" style="text-decoration: none;"><img src="' . base_url('modules/simples/assets/email/imagens/icon-whatsapp.png') . '"/></a>';
        }

        return implode('&nbsp;&nbsp;&nbsp;', $elementos);
    }

    public function arquivoRodape($id_corretor = NULL, $externo = TRUE)
    {
        $assinatura = '';

        if( ! is_null($id_corretor))
            if($externo)
                $assinatura =  $GLOBALS[FILIAL_FOTOS_CORRETORES_ASSINATURA_CAMINHO_EXTERNO] . $id_corretor . '.jpg';
            else
                $assinatura =  $GLOBALS[FILIAL_FOTOS_CORRETORES_ASSINATURA_CAMINHO_INTERNO] . $id_corretor . '.jpg';

        if(isset($GLOBALS[FILIAL_FOTOS_CORRETORES_ASSINATURA_CAMINHO_INTERNO]) && file_exists($GLOBALS[FILIAL_FOTOS_CORRETORES_ASSINATURA_CAMINHO_INTERNO] . $id_corretor . '.jpg'))
            return $assinatura;
        else
        {
            if($externo)
                return isset($GLOBALS[CONFIG_EMAIL_ASSINATURA_RODAPE_EXTERNO]) ? $GLOBALS[CONFIG_EMAIL_ASSINATURA_RODAPE_EXTERNO] : base_url('modules/simples/assets/email/imagens/rodape.jpg');
            else
                return isset($GLOBALS[CONFIG_EMAIL_ASSINATURA_RODAPE_INTERNO]) && file_exists($GLOBALS[CONFIG_EMAIL_ASSINATURA_RODAPE_INTERNO]) ? $GLOBALS[CONFIG_EMAIL_ASSINATURA_RODAPE_INTERNO] : MODULESPATH . 'simples/assets/email/imagens/rodape.jpg';
        }
    }

    public function enviar(EmailSimplesDomain $email)
    {
        $usuario = $this->obterCorretorRemetente($email->idCorretorRemetente);

        if(is_null($usuario))
            throw new Exception('Usuário para o envio de email não encontrado.');

        $this->CI->load->library('email');

        if(count($email->config) == 0)
        {
            $this->CI->load->model('simples/configuracao_model');

            $smtpConfig = $this->CI->configuracao_model->email->smtp();

            if( ! is_null($smtpConfig))
                $this->CI->email->initialize((array)$smtpConfig);
        }
        else
            $this->CI->email->initialize($email->config);

//        $this->CI->email->from($usuario->email, explode(' ', $usuario->nome)[0] . ' - ' . $GLOBALS[FILIAL_NOME]);
        $this->CI->email->from($usuario->email, utf8_encode(explode(' ', $usuario->nome)[0] . ' - ' . $GLOBALS[FILIAL_NOME]));

        if($email->remetentesCC)
            $this->CI->email->cc($email->para);
        else
            $this->CI->email->to($email->para);

        $emailsBCC = array();

        if(isset($GLOBALS[CONFIG_EMAIL_GERENTE_COPIA_OCULTA]) && strlen($GLOBALS[CONFIG_EMAIL_GERENTE_COPIA_OCULTA]) > 0)
            $emailsBCC[] = $GLOBALS[CONFIG_EMAIL_GERENTE_COPIA_OCULTA];

        if($email->copiaRemetente)
            $emailsBCC[] = $usuario->email;

        if(count($emailsBCC) > 0)
            $this->CI->email->bcc($usuario->email);

        $this->CI->email->subject($email->assunto);
        $this->CI->email->message($email->corpo);

        if($this->CI->email->send())
        {
            $this->ultimoErroAoTentarEnviar = '';
            return TRUE;
        }
        else
        {
            $this->ultimoErroAoTentarEnviar = $this->CI->email->print_debugger();
            return FALSE;
        }
    }

    protected function obterCorretorRemetente($id)
    {
        $this->CI->load->model('simples/corretores_model');

        return $this->CI->corretores_model->obter_informacoes_basicas($id);
    }
}

class EmailSimplesDomain
{
    public $para;
    public $remetentesCC    = TRUE;

    public $idCorretorRemetente;
    public $copiaRemetente  = FALSE;

    public $assunto;
    public $corpo = '';

    public $config = array();

    public function __construct($para, $assunto, $idCorretorRemetente, array $config = array())
    {
        $this->para                 = $para;
        $this->assunto              = $assunto;
        $this->idCorretorRemetente  = $idCorretorRemetente;
        $this->config               = $config;
    }
}

class EmailSimplesCorpoSiteDomain
{
    public $assunto;
    public $mensagem;
    public $nomeDestinatario;
    public $imoveis     = array();
    public $idCorretor  = NULL;
    public $idProposta  = NULL;

    public function __construct($assunto, $mensagem, $nomeDestinatario)
    {
        $this->assunto          = $assunto;
        $this->mensagem         = $mensagem;
        $this->nomeDestinatario = $nomeDestinatario;
    }
}

class EmailSimplesCorpoUsuarioSistemaDomain
{
    public $paginaTitulo;
    public $assunto;
    public $mensagem;
    public $imoveis = array();
    public $btnsSistema = array();

    public function __construct($paginaTitulo, $assunto, $mensagem)
    {
        $this->paginaTitulo = $paginaTitulo;
        $this->assunto      = $assunto;
        $this->mensagem     = $mensagem;
    }
}