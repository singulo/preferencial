<?php

class NotificarSistema
{
    const TIPO_NENHUM                                   = 0;
    const TIPO_NOVO_CLIENTE_CADASTRADO                  = 1;
    const TIPO_NOVO_CONTATO_NORMAL                      = 2;
    const TIPO_NOVO_CONTATO_INTERESSE_EM_IMOVEL         = 3;
    const TIPO_CLIENTE_ACESSANDO_SITE                   = 4;
    const TIPO_CLIENTE_BLOQUEADO_TENTANDO_ACESSAR_SITE  = 5;
    const TIPO_CLIENTE_EXLUIDO_TENTANDO_ACESSAR_SITE    = 6;
    const TIPO_CLIENTE_OLHANDO_IMOVEL                   = 7;
    const TIPO_CLIENTE_LEU_PROPOSTA                     = 8;
    const TIPO_CLIENTE_VINCULADO_A_OUTRO_USUARIO        = 9;
    const TIPO_CLIENTE_INTERAGIL_EM_FORMULARIO          = 10;

    const ERRO_EM_OPERACAO_NO_SITE                      = 500;

    public static function finalizarRequisicaoOk($text = null, $contentType = 'Content-type: application/json; charset=UTF-8')
    {
        // check if fastcgi_finish_request is callable
        if (is_callable('fastcgi_finish_request')) {
            if ($text !== null) {
                echo $text;
            }
            /*
             * http://stackoverflow.com/a/38918192
             * This works in Nginx but the next approach not
             */
            session_write_close();
            fastcgi_finish_request();

            return;
        }

        ignore_user_abort(true);

        $use_ob_start = strpos('application/json', $contentType) == -1;

        @ob_start();

        if ($text !== null) {
            echo $text;
        }

        if($use_ob_start)
        {
            // Disable compression (in case content length is compressed).
            header($contentType);
            $serverProtocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
            header($serverProtocol . ' 200 OK');
            header('Content-Encoding: none');
            header('Content-Length: ' . ob_get_length());
        }

        @ob_flush();
        @ob_end_flush();

        flush();
    }

    public static function aoTerminoDaRequisicao($idFilialSimples, $tipo, $dados = NULL)
    {
        register_shutdown_function('NotificarSistema::notificar', $idFilialSimples, $tipo, $dados);
    }

    public static function notificar($idFilialSimples, $tipo, $dados = NULL, $aguardarSegundos = 1)
    {
        $_dados['id_filial_simples'] = $idFilialSimples;
        $_dados['tipo'] = $tipo;
        $_dados['dados'] = ! is_null($dados) ? is_string($dados) ? $dados : json_encode($dados) : NULL;
        $_dados['hora_inicio_execucao'] = date('Y-m-d H:i:s');

        if($aguardarSegundos > 0)
            sleep($aguardarSegundos);

        $_dados['hora_execucao'] = date('Y-m-d H:i:s');

        $url = SISTEMA_SIMPLES_LINK_ADMIN . 'notificar';

        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, $url);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $_dados);

        $result  = curl_exec($cURL);
        curl_close($cURL);

        return @json_decode($result);
    }
}