<?php

class Finalidades
{
    const Venda         = 1;
    const Aluguel       = 2;
    const Temporada     = 3;

    public static function toString($val){
        $tmp = new ReflectionClass(get_called_class());
        $a = $tmp->getConstants();
        $b = array_flip($a);

        return ucfirst($b[$val]);
    }

    public static function getConstants()
    {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }

    public static function ativas()
    {
        $finalidades = [];

        if(SiteFilial::configs()->finalidade_venda_ativo === TRUE)
            $finalidades['Venda'] = Finalidades::Venda;
        if(SiteFilial::configs()->finalidade_aluguel_ativo === TRUE)
            $finalidades['Aluguel'] = Finalidades::Aluguel;
        if(SiteFilial::configs()->finalidade_temporada_ativo === TRUE)
            $finalidades['Temporada'] = Finalidades::Temporada;

        return $finalidades;
    }
}