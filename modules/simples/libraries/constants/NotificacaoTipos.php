<?php

class NotificacaoTipos
{
    const ClienteVinculado              = 'cliente_vinculado';
    const RegistroChave                 = 'registro_chave';
    const PropostaLida                  = 'proposta_lida';
    const Normal                        = 'normal';
    const InteresseEmImovel             = 'interesse_em_imovel';
    const SolicitacaoAtualizarImovel    = 'atualizar_imovel';
    const ImovelAtualizado              = 'imovel_atualizado';

    public static function toString($val){
        $tmp = new ReflectionClass(get_called_class());
        $a = $tmp->getConstants();
        $b = array_flip($a);

        return ucfirst($b[$val]);
    }

    public static function getConstants()
    {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
}