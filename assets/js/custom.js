if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) )
    $('.selectpicker').selectpicker('mobile');
else
    $('.selectpicker').selectpicker();

$('.telefone').mask('(99)9999-9999?9');

$('#menu-filtro select[name="valores"]').on('change', function(){

    var valores = $('#menu-filtro select[name="valores"]').val();

    if(valores == null)
    {
        $('#menu-filtro input[name="preco-min"]').val('');
        $('#menu-filtro input[name="preco-max"]').val('');
    }
    else
    {
        if(valores[0] != undefined)
            $('#menu-filtro input[name="preco-min"]').val(valores[0]);

        if(valores[1] != undefined)
            $('#menu-filtro input[name="preco-max"]').val(valores[1]);
    }
});

$('select.selectpicker[name="cidade"]').on('change', function(){
    var form = $(this).closest('form');

    carrega_bairros_cidade(form);

    var selectCondominio = form.find('select[name="id_condominio"]');

    //HABILITA APENAS OS CONDOMINIOS DA CIDADE SELECIONADA
    if($(this).val() != '')
    {
        $(selectCondominio).find('option').attr('disabled', true);
        $(selectCondominio).find('option[data-subtext="' + $(this).val() + '"]').attr('disabled', false);
    }
    else
    {
        $(selectCondominio).find('option').attr('disabled', false);
    }

    $(selectCondominio).selectpicker('refresh');
});

function carrega_bairros_cidade(form, seleciona)
{
    var selectBairro = $(form).find('select[name="bairro"]');
    var selectCidade = $(form).find('select[name="cidade"]');
    var cidade = $(selectCidade).val();

    console.log(cidade == '');

    selectBairro.html('');

    if(cidade == '')
    {
        selectBairro.attr('disabled', true);
    }
    else
    {
        selectBairro.attr('disabled', false);

        $.each(cidades_bairros[cidade], function(index, bairro){
            selectBairro.append('<option value="'+ bairro + '">' + bairro + '</option>');
        });
    }

    if(seleciona != undefined)
        selectBairro.selectpicker('val', seleciona);

    selectBairro.selectpicker('refresh');
}

$('#menu-filtro input[name="id"]').on('keyup', function(e) {
    if(e.keyCode === 13)
    {
        $('#menu-filtro').submit();
    }
});

function obter_cidades_pela_uf(uf, simpleRequest)
{
    ajaxPost(
        uf,
        $('#base_url').val() + 'cidade/obter_cidade_pela_uf',
        simpleRequest
    );
}

$("#modal-dados").on('show.bs.modal', function () {
    if($('#form-dados select[name=uf]').val() != undefined && $('#form-dados select[name=uf]').val() != '')
        obter_cidades($('#form-dados'), $('#form-dados select[name=cidade]').data('cidade-default'));
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}