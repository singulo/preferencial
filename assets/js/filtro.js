$(document).ready(function(){
    // VALOR SLIDER
    $('form.form-filtro .slider-filtro').slider({
        formatter: function(value) {
            if(value[1] == 3000000)
                return exibir_valor_imovel(value[0], '', '0,00') + ' : ' + exibir_valor_imovel(value[1], '', '0,00') + '+';
            else
            {
                return exibir_valor_imovel(value[0] == undefined ? 0 : value[0], '', '0,00') + ' : ' + exibir_valor_imovel(value[1] == undefined ? 0 : value[1], '', '0,00');
            }
        }
    });

    $("form.form-filtro .slider-filtro").on("slide", function(slideEvt) {
        $('form.form-filtro input[name="filtro-preco-min"]').val(slideEvt.value[0]);
        $('form.form-filtro input[name="filtro-preco-max"]').val(slideEvt.value[1]);
    });
});