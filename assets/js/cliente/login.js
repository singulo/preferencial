on_cliente_entrar.successCallback.push(function(usuario){
    $(".nav li a[href='#modal-login']").text(usuario.nome);
    $(".nav li a[href='#modal-login']").attr('href', '#modal-dados');

    $(".nav li a:contains('HISTÓRICO')").show();

    $('#form-dados').dejsonify(usuario);

    var creci = '';

    if(usuario.corretor.nivel != 4)
        creci = 'creci: ' + usuario.corretor.creci;

    $('#modal-dados img.corretor-foto').attr('src', $('#filial_fotos_corretores').val() + usuario.corretor.id_corretor + '.jpg');
    $('#modal-dados .corretor-nome').text(usuario.corretor.nome);
    $('#modal-dados .corretor-creci').text(creci);
    $('#modal-dados .corretor-email').html('email: <a href="mailto:' + usuario.corretor.email + '" target="_top">' + usuario.corretor.email + '</a>');
});

$(document).ready(function() {

    $("#form-login").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        }
    });
});

function cliente_login()
{
    if(!$("#form-login").valid()) return;

    var $btn = $('.btn-login').button('loading');

    cliente_verifica_email(
        $("#form-login input[name='email']").val(),
        function(usuario){//successCallback
            alertify.success('Olá ' + usuario.nome);
            //$('#modal-login').modal('hide');
            setTimeout(function(){
                location.reload();
            }, 700);
        },
        function(data){//failureCallback
            if(data.msg != undefined) // BLOQUEADO
            {
                //FECHA MODAL
                $('#modal-login').modal('hide');
                alertify.error(data.msg);
            }
            else
            {
                $("#form-cadastrar input[name='email']").val($("#form-login input[name='email']").val());

                $('#modal-login').modal('hide');
                $('#modal-cadastrar').modal('show');
            }

        },
        function(){//errorCallback
            alertify.error('Ocorreu um erro ao efetuar o login. Por favor tente mais tarde!');
        },
        function(){//completeCallback
            $btn.button('reset');
        });
}

function cliente_logout()
{
    alertify
        .okBtn("Sim")
        .cancelBtn("Não")
        .confirm("Deseja realmente sair?",
        function () {
            window.location.href = $('#base_url').val() + 'cliente/logout';
    });
}
