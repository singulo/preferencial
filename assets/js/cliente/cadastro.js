$(document).ready(function() {

    $("#form-cadastrar").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            nome: {
                required: true,
                minlength: 5
            }
        }
    });
});

function cliente_cadastrar()
{
    if(!$("#form-cadastrar").valid()) return;

    var $btn = $('.btn-login').button('loading');

    var cliente = $('#form-cadastrar').jsonify();

    cliente_registrar(
        cliente,
            {
                successCallback: function(data){
                    $('#modal-cadastrar').modal('hide');

                    //SETA INPUT DE EMAIL NO FORM DE LOGIN PARA EFETUAR O LOGIN APOS REGISTRO DO CLIENTE

                    if(data.cliente != undefined)
                    {
                        $("#form-login input[name='email']").val(data.cliente.email);
                    }
                    else
                        $("#form-login input[name='email']").val(cliente.email);

                    cliente_login();
                },
                failureCallback: function(data){
                    alertify.error('Ocorreu um erro ao cadastrar. Por favor tente mais tarde!');
                },
                errorCallback: function(){
                    alertify.error('Ocorreu um erro ao cadastrar. Por favor tente mais tarde!');
                },
                completeCallback: function(){
                    $btn.button('reset');
                }
        });
}