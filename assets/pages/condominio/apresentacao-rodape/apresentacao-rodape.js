$(document).ready(function(){
    $('.owl-condominios-apresentacao-rodape').owlCarousel({
        autoplay: 1500,
        items: 4,
        loop: true,
        margin: 0,
        //pagination: false
        navegation: false,
        responsive : {
            0:{
                items: 1,
            },
            768:{
                items: 1,
            },
            1000: {
                items: 4,
            }
        }
    });
});