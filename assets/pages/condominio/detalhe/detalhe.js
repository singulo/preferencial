(function() {

    [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

})();

$(document).ready(function(){
    $('#imagens-principais-carousel').owlCarousel({
        center: true,
        autoplay: true,
        autoWidth: true,
        items: 1,
        loop: true,
        margin: 0,
        responsive: {
            1200: {
                items: 1.47
            }
        }
    });

    /*$('#imagens-imovel-carousel').owlCarousel({
     items: 4,
     });*/

    $(".owl-carousel-navigation .next").click(function(){
        $('#imagens-principais-carousel').trigger('next.owl.carousel');
    });
    $(".owl-carousel-navigation .prev").click(function(){
        $('#imagens-principais-carousel').trigger('prev.owl.carousel');
    });

    $('#section-fotos a').fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
    });

    $('#section-plantas a').fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
    });
});