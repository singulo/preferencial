var imovelVisualizacao = $('#imovel-visualizacao').clone();

if(typeof filtro !== 'undefined')
{
    $("#form-pesquisar input[name='id']").val(filtro.id);
    $("#form-pesquisar select.selectpicker[name='id_tipo']").selectpicker('val', filtro.id_tipo);
    $("#form-pesquisar select.selectpicker[name='cidade']").selectpicker('val', filtro.cidade).trigger('change');
    $("#form-pesquisar select.selectpicker[name='id_condominio']").selectpicker('val', filtro.id_condominio);
    $("#form-pesquisar select.selectpicker[name='bairro']").selectpicker('val', filtro.bairro);
    $("#form-pesquisar select.selectpicker[name='dormitorios']").selectpicker('val', filtro.dormitorios);
    $("#form-pesquisar select.selectpicker[name='finalidade']").selectpicker('val', filtro.finalidade);
    $("#form-pesquisar select.selectpicker[name='preco_min']").selectpicker('val', filtro.preco_min);
    $("#form-pesquisar select.selectpicker[name='preco_max']").selectpicker('val', filtro.preco_max);
}

var maxResultado = 12;
$(document).ready(function(){


    if(window.location.pathname == '/imovel/pesquisar' || window.location.pathname == '/novo/imovel/pesquisar')
    {

        var pag = getParameterByName('pagina', location.href);

        pesquisar(pag, pag != null);

    }


    //pesquisar(0, maxResultado);

    $('#ordenar_por').on('change', function(){
        pesquisar(0);
    });
});

$('#form-pesquisar input[name="id"]').on('keyup', function(e) {
    if(e.keyCode === 13)
        pesquisar(0);
});

function pesquisar(pagina, forcar_total)
{
    $('#msg-resultados-encontrados').html('Buscando <strong>imóveis</strong>, aguarde ...');

    $('#imoveis-encontrados').html('');

    filtro = {};

    filtro.pagina = pagina;

    if(forcar_total)
        filtro.forcar_total = 1;

    filtro.limite = maxResultado;
    filtro.ordenacao = $('#ordenar_por').val();


    window.history.pushState('', document.title, $('#base_url').val() + 'imovel/pesquisar?' + $.param($('#form-pesquisar').jsonify()) + '&' + $.param(filtro));
    filtro.filtro = $('#form-pesquisar').jsonify();

    ajaxPost(
        filtro,
        $('#base_url').val() + 'imovel/buscar',
        {
            successCallback: function(data){
                $.each(data.imoveis, function (key, imovel) {
                    $('#imoveis-encontrados').append(montar_imovel(imovel));
                });

                if(data.total > 0)
                {
                    $('#msg-resultados-encontrados').html('<strong>Encontramos ' + data.total + '</strong> <br> <small>ofertas em nosso site.</small>');
                    //$('#pagination').pagination('updateItems', data.total);
                    atualizar_paginacao(parseInt(filtro.pagina) + 1, data.total);

                }
                else
                    $('#msg-resultados-encontrados').html('Nada foi encontrado.');
            },
            failureCallback: function(data){
                alertify.error('Ocorreu um erro ao obter os imóveis.')
            },
            errorCallback: function(jqXhr, textStatus, errorThrown){
                alertify.error('Ocorreu um erro ao obter os imóveis. Tente novamente mais tarde.')
            },
            completeCallback: function(){}
        }
    );
}

function montar_imovel(imovel)
{
    novaDiv = imovelVisualizacao.clone();

    novaDiv.find('img.imovel-img').attr('src', $('#filial_fotos_imoveis').val() + imovel.foto);

    novaDiv.find('.imovel-url').attr('href', imovel_url(imovel));

    novaDiv.find('.tipo').text(imoveis_tipos[imovel.id_tipo].tipo);
    novaDiv.find('.finalidade').text(finalidades[imovel.finalidade]);
    novaDiv.find('.descricao').text(imovel.descricao);

    novaDiv.find('.imovel-valor').html(exibir_valor_imovel(imovel, 'R$'));

    novaDiv.show();

    return novaDiv;
}

//$(function() {
//    $('#pagination').pagination({
//        items: 0,
//        itemsOnPage: maxResultado,
//        cssStyle: 'light-theme',
//        nextText: 'Próximo <i class="fa fa-angle-right"></i>',
//        prevText: '<i class="fa fa-angle-left"></i> Anterior',
//        onPageClick : function(pageNumber){
//            pesquisar((pageNumber-1), maxResultado);
//        }
//    });
//});

function atualizar_paginacao(paginaAtual, items)
{
    $('#pagination').pagination({
        items: items,
        itemsOnPage: maxResultado,
        cssStyle: 'light-theme',
        nextText: '',
        prevText: '',
        currentPage: paginaAtual,
        onPageClick : function(pageNumber){
            pesquisar((pageNumber-1), maxResultado);
        }
    });
}