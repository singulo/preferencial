(function() {

    [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

})();

$(document).ready(function(){
    var owl = $('#imagens-principais-carousel').owlCarousel({
        center: true,
        autoplay: true,
        autoWidth: true,
        response: true,
        items: 1,
        loop: true,
        margin: 0,
        responsive:{
            768:{
                items: 1
            },
            1200:{
                items:1.47
            }
        }
    });

    $(".owl-carousel-navigation .next").click(function(){
        $('#imagens-principais-carousel').trigger('next.owl.carousel');
    });
    $(".owl-carousel-navigation .prev").click(function(){
        $('#imagens-principais-carousel').trigger('prev.owl.carousel');
    });

    $('#section-fotos a').fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
    });

    $('#section-plantas a').fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
    });


    /*owl.on('changed.owl.carousel', function(event) {
        if(event.page.index == event.page.count -1 && JSON.parse($('#usuario').val()) == null)
        {
            $('#modal-mais-info-login').modal('show');
        }
    });*/

});