$('.owl-destaques').owlCarousel({
    autoplay: true,
    items: 3,
    loop: false,
    margin: 25,
    //pagination: true
    responsive : {
        0:{
            items: 1,
        },
        768:{
            items: 2,
        },
        1000: {
            items: 3,
        }
    }
});

$('.banner-container').owlCarousel({
    autoplay: true,
    items: 1,
    loop: $('.banner-container div.item').length > 1
});

if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    $('.menu-superior').addClass('menu-fixo');
    $('.menu-imovel').addClass('menu-fixo');
}